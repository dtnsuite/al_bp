# Makefile for compiling Unified API

LIB=static
#LIB=dynamic I should add LIB_BP
UNIFIED_API_LIB_NAME_BASE=libunified_api
IMPL_OBJS=
CC=gcc
#It temporarily includes uD3TN C API source files destined to be moved to uD3TN  
UD3TN_C_API_SRC=./src/bp/ud3tn/ud3tn_c_api/*.c

DEBUG=0
ifeq ($(DEBUG),0)
DEBUG_FLAG= -O2
else
DEBUG_FLAG=-g -fno-inline -O0
endif

CFLAGS= $(DEBUG_FLAG) -Wall -fPIC -Werror

IBRDTN_VERSION=1.0.1
ION_ZCO=-DNEW_ZCO #obsolete
BP=bpv7
DIRS_BP_IMPL= -I./src/bp/ibr/ -I./src/bp/dtn2/ -I./src/bp/ion_$(BP)/ -I./src/bp/ud3tn/ -I./src/bp/ud3tn/ud3tn_c_api/ -I./src/bp/unibo-bp/
DIRS_NEW_AL= -I./src -I./src/al/bundle  -I./src/al/types -I./src/al/utilities/debug_print -I./src/al/utilities/bundle_print -I./src/list -I./src/al_bp

LIBS=
UNIFIED_API_LIB_NAME=$(UNIFIED_API_LIB_NAME_BASE)

HAVE_IMPL := 0

ifneq ($(strip $(DTN2_DIR)),)
$(info Making for DTN2)
HAVE_IMPL := 1
UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME)_vDTN2
INC := -I$(DTN2_DIR) -I$(DTN2_DIR)/applib/ -I$(DTN2_DIR)/oasys/include -I/usr/include/tirpc
OPT := -DDTN2_IMPLEMENTATION
ifeq ($(DTNME_OLD),1)
OPT := $(OPT) -DSECS
endif
endif

ifneq ($(strip $(ION_DIR)),)
$(info Making for ION)
HAVE_IMPL := 1
UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME)_vION
INC := $(INC) -I$(ION_DIR) -I$(ION_DIR)/$(BP)/include -I$(ION_DIR)/$(BP)/library -I$(ION_DIR)/ici/include
OPT := $(OPT) -DION_IMPLEMENTATION $(ION_ZCO)
endif

ifneq ($(strip $(IBRDTN_DIR)),)
# IBRDTN
$(info Making for IBRDTN)
HAVE_IMPL := 1
UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME)_vIBRDTN
INC := $(INC) -I$(IBRDTN_DIR)/ibrcommon-$(IBRDTN_VERSION) -I$(IBRDTN_DIR)/ibrdtn-$(IBRDTN_VERSION)
OPT := $(OPT) -DIBRDTN_IMPLEMENTATION
endif

ifneq ($(strip $(UD3TN_DIR)),)
# UD3TN
$(info Making for uD3TN)
HAVE_IMPL := 1
UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME)_vUD3TN
INC := $(INC) -I$(UD3TN_DIR)/include -I$(UD3TN_DIR)/generated/aap2 -I$(UD3TN_DIR)/include/cla/posix/ -I$(UD3TN_DIR)/include/ud3tn -I$(UD3TN_DIR)/external/nanopb
OPT := $(OPT) -DUD3TN_IMPLEMENTATION
endif

ifeq ($(UNIBO_BP),1)
# Unibo-BP
$(info Making for Unibo-BP)
HAVE_IMPL := 1
UNIFIED_API_LIB_NAME := $(UNIFIED_API_LIB_NAME)_vUNIBOBP
OPT := $(OPT) -DUNIBOBP_IMPLEMENTATION
endif

ifeq ($(HAVE_IMPL), 0)
# Missing all implementations
all: help
else
# Ok
all: lib
endif

OPT := $(OPT) $(CFLAGS)

ifeq ($(strip $(LIB)),static)
LIB_CC=ar crs $(UNIFIED_API_LIB_NAME).a
else
LIB_CC=$(CC) -shared -Wl,-soname,$(UNIFIED_API_LIB_NAME).so -o $(UNIFIED_API_LIB_NAME).so
endif

INSTALLED=$(wildcard /usr/lib/$(UNIFIED_API_LIB_NAME_BASE)*)

lib: objs
	$(LIB_CC) *.o $(IMPL_OBJS)

install:
	cp $(UNIFIED_API_LIB_NAME)* /usr/lib/

uninstall:
	@if test `echo $(INSTALLED) | wc -w` -eq 1 -a -f "$(INSTALLED)"; then rm -rf $(INSTALLED); else if test -n "$(INSTALLED)"; then echo "MORE THAN 1 FILE, DELETE THEM MANUALLY: $(INSTALLED)"; else echo "NOT INSTALLED"; fi fi

objs:
	$(CC) $(DIRS_NEW_AL) $(DIRS_BP_IMPL) $(INC) $(OPT) -c src/bp/unibo-bp/*.c src/bp/dtn2/*.c src/bp/ion_$(BP)/*.c src/bp/ud3tn/*.c $(UD3TN_C_API_SRC) src/al/bundle/*.c src/al/bundle/options/*.c src/al/socket/*.c  src/al_bp/*.c src/al/utilities/debug_print/*.c src/al/utilities/bundle_print/*.c  src/list/*.c
	g++ $(DIRS_NEW_AL) $(DIRS_BP_IMPL) $(INC) $(OPT) -Wno-deprecated -c src/bp/ibr/*.cpp

help:
	@echo "Usage:"
	@echo
	@echo "For Unibo-BP:            make UNIBO_BP=1"
	@echo "For DTN2 (or DTNME):     make DTN2_DIR=<dtn2_dir or dtnme_dir>"
	@echo "For ION:                 make ION_DIR=<ion_dir>"
	@echo "For IBRDTN:              make IBRDTN_DIR=<ibrdtn_dir>"
	@echo "For UD3TN:               make UD3TN_DIR=<ud3tn_dir>"
	@echo
	@echo "The implementation directories (DTN2_DIR, ION_DIR, etc.) can be combined to build a multiplatform executable"
	@echo
	@echo "To compile with debug symbols add DEBUG=1"
	@echo "To compile for BPv6 in ION add BP=bpv6"
	@echo "To compile for DTN2 and DTNME<1.2.0 versions add DTNME_OLD=1"

clean:
	@rm -rf *.o *.so *.a
  
