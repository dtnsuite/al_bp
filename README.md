# Unified API library

The Unified API library aims to offer prospective programmers an “abstracted” interface compatible with most major BP
implementations, such as Unibo_BP, DTN2/DTNME, ION, IBR-DTN and µD3TN (AAP2).

It is part of the DTNsuite, which consists of several DTN applications that make use of the Unified API (ex Abstraction
Layer), plus the Unified API itself.

As well as making applications intrinsically multiplatform, the Unified API library also facilitates the development of
new applications by presenting an interface resembling the familiar UDP socket, and by moving some of the most
challenging BP aspects from the application to the library.

# Copyright

Copyright (c) 2022, University of Bologna.

# License

Unified API is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, ei>
Unified-API is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR>
You should have received a copy of the GNU General Public License along with Unified-API. If not,
see <http://www.gnu.org/licenses/>.
As an exception, the files in the ud3tn_api_c, designed and developed to be moved in the ud3tn official package, are
released under the Apche 2.0 licence. See RAEDME.txt in the mentioned directory.

## Download, compilation and optional installation (see the guide)

Create a dtnsuite directory in your file system (if not already present)

```bash
$ mkdir dtnsuite
$ cd dtnsuite
``` 

Then clone the Unified API repository from your dtnsuite directory:

```bash
$ git clone https://gitlab.com/dtnsuite/unified_api.git
$ cd unified_api
```

(Change the branch if needed) Compile

- To compile only for DTN2/DTNME implementation
    ```bash
    $ make DTN2_DIR=<dtn2_dir>
    ```
- To compile only for ION
    ```bash
    $ make ION_DIR=<ion_dir>
    ```
- To compile only for IBR-DTN
    ```bash
    $ make IBRDTN_DIR=<ibrdtn_dir>
    ```
- To compile only for µD3TN
    ```bash
    $ make UD3TN_DIR=<ud3tn_dir>
    ```
- To compile only for Unibo-BP
    ```bash
    $ make UNIBO_BP=1
    ```
- The above can be concatenated to compile the Unified API for multiple implementations, for example:
  To compile for DTN2, ION and Unibo_BP
    ```bash
    $ make DTN2_DIR=<dtn2_dir> ION_DIR=<ion_dir> UNIBO_BP=1
    ```

Then you may install the Unified API library (optional; you need it only if you compile DTNsuite applications with
dynamic libraries, by overriding DTNsuite application Makefile default)

```bash
$ sudo make install
```

- For further help on compilation and options
    ```bash
    $ make
    ```
    
## Guide

Please refer to the Unified API .pdf guide for:

- release notes
- further instructions about the building the DTNsuite package

Further information:
A. Bisacchi, C. Caini, S. Lanzoni, “Design and Implementation of a Bundle Protocol Unified API”, in Proc. of ASMS 2022,
Graz, Austria, Sept. 2022, pp. 1-6. <https://doi.org/10.1109/ASMS/SPSC55670.2022.9914734>

## Credits (main authors of the present version)

- Carlo Caini (Academic supervisor, carlo.caini@unibo.it)
- Andrea Bisacchi (co-supervisor of v4 and of Unified API)
- Silvia Lanzoni (main author of Unified API)
- Lorenzo Persampieri (Unibo-BP support)
- Luca Fantini (complete decoupling of "al" layer from BP implementations)
- Fabio Colonna (ud3tn support)
- Beatrice Barbieri (ud3tn support)
