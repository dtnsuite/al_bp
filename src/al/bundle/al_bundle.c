/** \file al_bundle.c
 *
 *  \brief  This file contains all the functions related to the bundle object.
 *
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Davide Pallotti, davide.pallotti@studio.unibo.it
 *  \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *  \authors Silvia Lanzoni, silvia.lanzoni5@studio.unibo.it
 *
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *  01/01/16 | D. Pallotti	   |  Implementation contribution.
 *  01/01/20 | A. Bisacchi	   |  Implementation contribution.
 *  01/10/21 | S. Lanzoni      |  Implementation refactoring and documentation.
 *  11/08/23 | L. Fantini      |  Added al_bundle_unset_payload()
 *
 *
 */




#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <limits.h>
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>

#include "unified_api_system_libraries.h"

#include "al_bundle.h"
#include "al_bp.h"

al_bp_error_t set_payload(al_types_bundle_payload* payload,
                                 al_types_bundle_payload_location location,
                                 char* val, int len);

/******************************************************************************
 *
 * \par Function Name:
 *      al_bundle_create
 *
 * \brief  It initializes a bundle object.
 *
 * \return al_error
 *
 * \param[in] bundle_object		The bundle that will be initialized
 *
 *
 *****************************************************************************/

al_error al_bundle_create(al_types_bundle_object *bundle_object) {
	if (bundle_object == NULL) {
		printf("Error in al_bundle_create: the bundle object is null. \n");
		return AL_ERROR;
	}
	// spec
	bundle_object->spec = (al_types_bundle_spec*) malloc(
			sizeof(al_types_bundle_spec));
	memset(bundle_object->spec, 0, sizeof(al_types_bundle_spec));

	//spec->extensions.extension_blocks
	bundle_object->spec->extensions.extension_blocks = (al_types_extension_block*) malloc(sizeof(al_types_extension_block));
	memset(bundle_object->spec->extensions.extension_blocks,0, sizeof(al_types_extension_block));

	// payload
	bundle_object->payload = (al_types_bundle_payload*) malloc(
			sizeof(al_types_bundle_payload));
	memset(bundle_object->payload, 0, sizeof(al_types_bundle_payload));

	return AL_SUCCESS;
}

 /******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_free
  *
  * \brief  It deallocates the memory allocated with al_bundle_create(). If the payload is still attached, it frees it.
  *
  * \return al_error
  *
  * \param[in] bundle_object		The bundle object
  *
  *
  *****************************************************************************/

al_error al_bundle_free(al_types_bundle_object *bundle_object) {
	if (bundle_object == NULL) {
		printf("Error in al_bundle_free: the bundle object is null. \n");
		return AL_ERROR;
	}
	// payload->buf->buf_val or payload->filename.filename_val
	if(bundle_object->payload->filename.filename_val != NULL || bundle_object->payload->buf.buf_val != NULL) {
		al_bp_free_payload(bundle_object->payload);
	}
	// payload
	if (bundle_object->payload != NULL) {
		free(bundle_object->payload);
		bundle_object->payload = NULL;
	}
	
	// spec->extensions.extension_blocks
	al_bp_free_extensions(bundle_object->spec);

	// spec
	if (bundle_object->spec != NULL) {
		free(bundle_object->spec);
		bundle_object->spec = NULL;
	}
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_payload_location
  *
  * \brief  It returns the bundle payload location.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  location			The bundle payload location
  *
  *
  *****************************************************************************/

al_error al_bundle_get_payload_location(
		al_types_bundle_object bundle_object,
		al_types_bundle_payload_location *location) {
	if (bundle_object.payload == NULL){
		printf("Error in al_bundle_create: the bundle object is null. \n");
		return AL_ERROR;
	}

	*location = bundle_object.payload->location;

	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_payload_location
  *
  * \brief  It sets the bundle payload location.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  location			The bundle payload location
  *
  *
  *****************************************************************************/

al_error al_bundle_set_payload_location(
		al_types_bundle_object *bundle_object,
		al_types_bundle_payload_location location) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_payload_location: the bundle object is null. \n");
		return AL_ERROR;
	}
	if (bundle_object->payload == NULL){
		printf("Error in al_bundle_set_payload_location: the bundle payload is null. \n");
		return AL_ERROR;
	}

	bundle_object->payload->location = location;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_payload_size
  *
  * \brief  It returns the bundle payload size.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  size				The bundle payload size
  *
  * \Notes: If the payload location is a file gets the size of the file instead.
  *
  *****************************************************************************/

al_error al_bundle_get_payload_size(al_types_bundle_object bundle_object,
		uint32_t *size) {
	if (bundle_object.payload == NULL){
		printf("Error in al_bundle_get_payload_size: the bundle payload is null. \n");
		return AL_ERROR;
	}

	if (bundle_object.payload->location == BP_PAYLOAD_MEM) {
		if (bundle_object.payload->buf.buf_val != NULL) {
			*size = bundle_object.payload->buf.buf_len;
			return AL_SUCCESS;
		} else { // buffer is null
			printf("Error in al_bundle_get_payload_size: the buffer is null. \n");
			return AL_ERROR;
		}
	} else if (bundle_object.payload->location == BP_PAYLOAD_FILE
			|| bundle_object.payload->location == BP_PAYLOAD_TEMP_FILE) {
		if (bundle_object.payload->filename.filename_val == NULL){ // filename is null {
			printf("Error in al_bundle_get_payload_size: the filename is null. \n");
			return AL_ERROR;
	}
		struct stat st;
		memset(&st, 0, sizeof(st));
		if (stat(bundle_object.payload->filename.filename_val, &st) < 0) {
			printf("Error in al_bundle_get_payload_size: internal error in checking bundle payload file. \n");
			return AL_ERROR;
		}
		*size = st.st_size;
		return AL_SUCCESS;

	} else { // wrong payload location
		printf("Error in al_bundle_get_payload_size: wrong payload location \n");
		return AL_ERROR;
	}
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_payload_file
  *
  * \brief  It gets the filename of the payload.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  filename			The bundle payload filename
  * \param[in]  filename_len 		The filename length
  *
  *
  *****************************************************************************/

al_error al_bundle_get_payload_file(al_types_bundle_object bundle_object,
		char **filename, uint32_t *filename_len) {
	if (bundle_object.payload->location == BP_PAYLOAD_FILE
			|| bundle_object.payload->location == BP_PAYLOAD_TEMP_FILE) {
		if (bundle_object.payload->filename.filename_val == NULL) // filename is null
		{
			printf("Error in al_bundle_get_payload_file: filename is null\n");
			return AL_ERROR;
		}
		if (bundle_object.payload->filename.filename_len <= 0) {// filename size error
			printf("Error in al_bundle_get_payload_file: filename size error \n");
			return AL_ERROR;
		}
		*filename_len = bundle_object.payload->filename.filename_len;
		(*filename) = bundle_object.payload->filename.filename_val;
		return AL_SUCCESS;
	} else{
		// bundle location is not file
		printf("Error in al_bundle_get_payload_file: bundle location is not a file\n");
		return AL_ERROR;
	}
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_payload_mem
  *
  * \brief  It obtains the pointer related to the payload buffer
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  buf			        Pointer at the payload buffer
  * \param[in] 	buf_len				Payload buffer length
  *
  *
  *****************************************************************************/


al_error al_bundle_get_payload_mem(al_types_bundle_object bundle_object,
		char **buf, uint32_t *buf_len) {
	if (bundle_object.payload->location == BP_PAYLOAD_MEM) {
		if (bundle_object.payload->buf.buf_val != NULL) {
			*buf_len = bundle_object.payload->buf.buf_len;
			(*buf) = bundle_object.payload->buf.buf_val;
			return AL_SUCCESS;
		} else { // buffer is null
			printf("Error in al_bundle_get_payload_mem: buffer is null \n");
			return AL_ERROR;
		}
	} else
		// bundle location is not memory
		return BP_EINVAL;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_payload_file
  *
  * \brief  It sets the payload from a file given as input.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  filename			The bundle payload filename
  * \param[in]  filename_len 		The filename length
  *
  *****************************************************************************/


al_error al_bundle_set_payload_file(al_types_bundle_object *bundle_object,
		char *filename, uint32_t filename_len) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_payload_file: bundle object is null\n");
		return AL_ERROR;
	}
	if (filename == NULL){
		printf("Error in al_bundle_set_payload_file: filename is null\n");
		return AL_ERROR;
	}
	if (filename_len <= 0){
		printf("Error in al_bundle_set_payload_file: filename length is <0 \n");
		return AL_ERROR;
	}
	al_bp_error_t err;

	if (bundle_object->payload == NULL) {
		printf("Warning: al_bundle_set_payload_file running with payload == NULL\n");
		al_types_bundle_payload *bundle_payload = (al_types_bundle_payload *) malloc(sizeof(al_types_bundle_payload));
		memset(bundle_payload, 0, sizeof(*bundle_payload));
		err = set_payload(bundle_payload, BP_PAYLOAD_FILE, filename,
				filename_len);
		bundle_object->payload = bundle_payload;
		if (err != BP_SUCCESS){
			printf("Error in al_bundle_set_payload_file: %s\n",
				al_bp_strerror(err));
			return AL_ERROR;
		}
		else
			return AL_SUCCESS;
	} else // payload not null
	{
		memset(bundle_object->payload, 0, sizeof(al_types_bundle_payload));
		err = set_payload(bundle_object->payload, BP_PAYLOAD_FILE,
				filename, filename_len);
		if (err != BP_SUCCESS){
			printf("Error in al_bundle_set_payload_file: %s\n",
				al_bp_strerror(err));
			return AL_ERROR;
		}
			else
				return AL_SUCCESS;
	}
	return AL_SUCCESS;
}

//CCaini experimental
al_error al_bundle_set_payload(al_types_bundle_object *bundle_object,
		char *buffer_or_filename, uint32_t length, al_types_bundle_payload_location location) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_payload: bundle object is null\n");
		return AL_ERROR;
	}
	if (buffer_or_filename == NULL){
		printf("Error in al_bundle_set_payload: buffer_or_filename is null\n");
		return AL_ERROR;
	}
	if (length <= 0){
		printf("Error in al_bundle_set_payload: length is <0 \n");
		return AL_ERROR;
	}

	if (bundle_object->payload == NULL) {
		printf("Warning: al_bundle_set_payload was running with payload == NULL\n");
		bundle_object->payload = (al_types_bundle_payload *) malloc(sizeof(al_types_bundle_payload));
	}

	memset(bundle_object->payload, 0, sizeof(al_types_bundle_payload));

	if (location == BP_PAYLOAD_MEM) {
		bundle_object->payload->location=location;
		bundle_object->payload->buf.buf_len = length;
		bundle_object->payload->buf.buf_val = buffer_or_filename;//both are pointers

	} else if (location == BP_PAYLOAD_FILE || location == BP_PAYLOAD_TEMP_FILE) {
		bundle_object->payload->location=location;
		bundle_object->payload->filename.filename_len = length;
		bundle_object->payload->filename.filename_val = buffer_or_filename; //both are pointers

	} else {
		printf("Error in al_bundle_set_payload: location is wrong \n");
		return AL_ERROR;
	}
return AL_SUCCESS;
}


/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_payload_mem
  *
  * \brief  It sets the payload from a memory buffer.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  buf			        Pointer at the payload buffer
  * \param[in] 	buf_len				Payload buffer length
  *
  *****************************************************************************/

al_error al_bundle_set_payload_mem(al_types_bundle_object *bundle_object,
		char *buf, uint32_t buf_len) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_payload_mem: bundle object is null\n");
		return AL_ERROR;
	}
	if (buf_len < 0){
		printf("Error in al_bundle_set_payload_mem: the buffer length is <0\n");
		return AL_ERROR;
	}

	al_bp_error_t err;

	if (bundle_object->payload == NULL) {
//CCaini Ma non poteva allocare direttamente la memoria a bundle_object->payload
//e poi lavorare con quello esattamente come nel caso non NULL?
		//alloc memory for the bundle_payload pointer
		al_types_bundle_payload *bundle_payload = (al_types_bundle_payload *) malloc(sizeof(al_types_bundle_payload));
		//set to 0 &bundle_payload (memory and file parts)
		memset(bundle_payload, 0, sizeof(*bundle_payload));
		//set the &bundle_payload structure (memory part only)
		err = set_payload(bundle_payload, BP_PAYLOAD_MEM, buf, buf_len);
        //copy the pointer
		bundle_object->payload = bundle_payload;//both are pointers
		//bundle_payload sparirà all'uscita della funzione, per cui non lo dealloco, credo.
		if (err != BP_SUCCESS){
					printf("Error in al_bundle_set_payload_mem: %s\n",
						al_bp_strerror(err));
					return AL_ERROR;
				}
				else
					return AL_SUCCESS;
	} else //bundle_object->payload not null
	{
		//set to 0 the memory pointed by bundle_object_payload
		memset(bundle_object->payload, 0, sizeof(al_types_bundle_payload));
		//set the payload structure (memory part only)
		err = set_payload(bundle_object->payload, BP_PAYLOAD_MEM, buf,
				buf_len);
		if (err != BP_SUCCESS){
					printf("Error in al_bundle_set_payload_mem: %s\n",
						al_bp_strerror(err));
					return AL_ERROR;
				}
				else
					return AL_SUCCESS;
	}
	return err;

}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_unset_payload
  *
  * \brief  It unsets (detach) the payload. The dual operation of al_bundle_set_payload_*
  * CCaini in both cases memory is neither allocated nor deallocated
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  *
  *****************************************************************************/
al_error al_bundle_unset_payload(al_types_bundle_object *bundle_object) {
	if(bundle_object == NULL) {
		printf("Error in al_bundle_unset_payload: bundle object is null\n");
		return BP_EINVAL;
	}
	if(bundle_object->payload == NULL) {
		printf("Error in al_bundle_unset_payload: bundle object payload is null\n");
		return BP_EINVAL;
	}
	memset(bundle_object->payload, 0, sizeof(al_types_bundle_payload));
	return BP_SUCCESS;
}

/******************************************************************************
  * ex al_bp_set_payload; the new version is independent of BP implementations
  * It is an internal function used by both al_bundle_set_payload_mem/file
 *****************************************************************************/

al_bp_error_t set_payload(al_types_bundle_payload* payload, //nonnull
                                 al_types_bundle_payload_location location,
                                 char* val, int len)
{
	memset(payload, 0, sizeof(*payload));

	payload->location = location;

	if (location == BP_PAYLOAD_MEM) {
		payload->buf.buf_len = len;
		payload->buf.buf_val = val;//both are pointers

	} else if (location == BP_PAYLOAD_FILE || location == BP_PAYLOAD_TEMP_FILE) {
		payload->filename.filename_len = len;
		payload->filename.filename_val = val; //both are pointers

	} else
		return BP_EINVAL;

	return BP_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_source
  *
  * \brief  It obtains the bundle source EID.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  source				Source EID
  *
  *****************************************************************************/

al_error al_bundle_get_source(al_types_bundle_object bundle_object,
		al_types_endpoint_id *source) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_source: bundle object is null\n");
			return AL_ERROR;
	}
	*source = bundle_object.spec->source;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_source
  *
  * \brief  It sets the bundle source EID.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  source				Source EID
  *
  *****************************************************************************/

al_error al_bundle_set_source(al_types_bundle_object *bundle_object,
		al_types_endpoint_id source) {
	if (bundle_object == NULL){
			printf("Error in al_bundle_set_source: bundle object is null\n");
				return AL_ERROR;
		}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_source: bundle object spec is null\n");
			return AL_ERROR;
	}
	al_bp_copy_eid(&(bundle_object->spec->source), &source);
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_dest
  *
  * \brief  It gets the destination EID.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  destination				Destination EID
  *
  *****************************************************************************/

al_error al_bundle_get_dest(al_types_bundle_object bundle_object,
		al_types_endpoint_id *dest) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_dest: bundle object spec is null\n");
			return AL_ERROR;
	}
	*dest = bundle_object.spec->destination;
	return AL_SUCCESS;

}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_dest
  *
  * \brief  It sets the bundle destination EID.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  destination				Destination EID
  *
  *****************************************************************************/

al_error al_bundle_set_dest(al_types_bundle_object *bundle_object,
		al_types_endpoint_id dest) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_dest: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_dest: bundle object spec is null\n");
			return AL_ERROR;
	}
	al_bp_copy_eid(&(bundle_object->spec->destination), &dest);
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_report_to
  *
  * \brief  The function gets the bundle report-to EID.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  report_to			Report_to EID.
  *
  *****************************************************************************/

al_error al_bundle_get_report_to(al_types_bundle_object bundle_object,
		al_types_endpoint_id *report_to) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_report_to: bundle object spec is null\n");
			return AL_ERROR;
	}
	*report_to = bundle_object.spec->report_to;
	return AL_SUCCESS;

}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_report_to
  *
  * \brief  The function sets the bundle report-to EID.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object			The bundle object
  * \param[in]  report_to				Report_to EID to be set.
  *
  *****************************************************************************/

al_error al_bundle_set_report_to(al_types_bundle_object *bundle_object,
		al_types_endpoint_id report_to) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_report_to: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_report_to: bundle object spec is null\n");
			return AL_ERROR;
	}
	al_bp_copy_eid(&(bundle_object->spec->report_to), &report_to);
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_unreliable
  *
  * \brief  It sets the "unreliable" (ecos) bundle flag .
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  unreliable			The flag value to be set
  *
  *****************************************************************************/

al_error al_bundle_set_unreliable(al_types_bundle_object *bundle_object,
		boolean_t unreliable) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_unreliable: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_unreliable: bundle object spec is null\n");
			return AL_ERROR;
	}
	bundle_object->spec->ecos.unreliable = unreliable;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_unreliable
  *
  * \brief  It gets the "unreliable" (ecos) bundle flag .
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  unreliable			The flag value
  *
  *****************************************************************************/


al_error al_bundle_get_unreliable(al_types_bundle_object bundle_object,
		boolean_t *unreliable) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_unreliable: bundle object spec is null\n");
			return AL_ERROR;
	}
	*unreliable = bundle_object.spec->ecos.unreliable;
	return AL_SUCCESS;
}


/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_reliable
  *
  * \brief  It sets the "reliable" (ecos) bundle flag .
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  reliable			The flag value to be set
  *
  *****************************************************************************/

al_error al_bundle_set_reliable(al_types_bundle_object *bundle_object,
		boolean_t reliable) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_reliable: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_reliable: bundle object spec is null\n");
			return AL_ERROR;
	}
	bundle_object->spec->ecos.reliable = reliable;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_reliable
  *
  * \brief  It gets the "reliable" (ecos) bundle flag .
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  reliable			The flag value
  *
  *****************************************************************************/


al_error al_bundle_get_reliable(al_types_bundle_object bundle_object,
		boolean_t *reliable) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_reliable: bundle object spec is null\n");
			return AL_ERROR;
	}
	*reliable = bundle_object.spec->ecos.reliable;
	return AL_SUCCESS;
}


/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_critical
  *
  * \brief  It sets the critical (ecos) bundle flag.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  critical			The flag value to be set
  *
  *****************************************************************************/

al_error al_bundle_set_critical(al_types_bundle_object *bundle_object,
		boolean_t critical) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_critical: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_critical: bundle object spec is null\n");
			return AL_ERROR;
	}
	bundle_object->spec->ecos.critical= critical;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_critical
  *
  * \brief  It gets the critical (ecos) bundle flag.
  *
  * \return al_error
  *
  * \param[in] 	 bundle_object		The bundle object
  * \param[in]   critical			The flag value
  *
  *****************************************************************************/


al_error al_bundle_get_critical(al_types_bundle_object bundle_object,
		boolean_t *critical) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_critical: bundle object spec is null\n");
			return AL_ERROR;
	}
	*critical = bundle_object.spec->ecos.critical;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_flow_label
  *
  * \brief  It sets the bundle flow label (ecos).
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  flow_label			The bundle flow label to be set
  *
  *****************************************************************************/

al_error al_bundle_set_flow_label(al_types_bundle_object *bundle_object,
		uint32_t flow_label) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_flow_label: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_flow_label: bundle object spec is null\n");
			return AL_ERROR;
	}
	bundle_object->spec->ecos.flow_label = flow_label;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_flow_label
  *
  * \brief  It gets the bundle flow label (ecos).
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  flow_label			The bundle flow label
  *
  *****************************************************************************/

al_error al_bundle_get_flow_label(al_types_bundle_object bundle_object,
		uint32_t *flow_label) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_flow_label: bundle object spec is null\n");
			return AL_ERROR;
	}
	*flow_label = bundle_object.spec->ecos.flow_label;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_lifetime
  *
  * \brief  It gets the bundle lifetime.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  exp			        The bundle lifetime
  *
  *****************************************************************************/

al_error al_bundle_get_lifetime(al_types_bundle_object bundle_object,
		al_types_timeval *exp) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_expiration: bundle object spec is null\n");
			return AL_ERROR;
	}
	*exp = bundle_object.spec->lifetime;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_lifetime
  *
  * \brief  It sets the bundle lifetime.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  exp			        The bundle lifetime to be set
  *
  *****************************************************************************/

al_error al_bundle_set_lifetime(al_types_bundle_object *bundle_object,
		al_types_timeval exp) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_expiration: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_expiration: bundle object spec is null\n");
			return AL_ERROR;
	};
	bundle_object->spec->lifetime = exp;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_creation_timestamp
  *
  * \brief  It gets the bundle creation timestamp.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  ts		            The bundle creation timestamp
  *
  *****************************************************************************/

al_error al_bundle_get_creation_timestamp(
		al_types_bundle_object bundle_object, al_types_creation_timestamp *ts) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_creation_timestamp: bundle object spec is null\n");
			return AL_ERROR;
	}
	*ts = bundle_object.spec->creation_ts;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_creation_timestamp
  *
  * \brief  It sets the bundle creation timestamp.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  ts		            The bundle creation timestamp to be set
  *
  *****************************************************************************/


al_error al_bundle_set_creation_timestamp(
		al_types_bundle_object *bundle_object, al_types_creation_timestamp ts) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_creation_timestamp: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_creation_timestamp: bundle object spec is null\n");
			return AL_ERROR;
	}
	bundle_object->spec->creation_ts = ts;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_control_flags
  *
  * \brief  It gets the delivery options.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  bundle_proc_ctrl_flags               The bundle delivery options
  *
  *****************************************************************************/

al_error al_bundle_get_control_flags(al_types_bundle_object bundle_object,
		al_types_bundle_processing_control_flags *bundle_proc_ctrl_flags) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_control_flags: bundle object spec is null\n");
			return AL_ERROR;
	}
	*bundle_proc_ctrl_flags = bundle_object.spec->bundle_proc_ctrl_flags;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_control_flags
  *
  * \brief  It sets the delivery options.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  bundle_proc_ctrl_flags               The bundle delivery options
  *
  *****************************************************************************/

al_error al_bundle_set_control_flags(al_types_bundle_object *bundle_object,
		al_types_bundle_processing_control_flags bundle_proc_ctrl_flags) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_control_flags: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_control_flags: bundle object spec is null\n");
			return AL_ERROR;
	}
	bundle_object->spec->bundle_proc_ctrl_flags = bundle_proc_ctrl_flags;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_status_report
  *
  * \brief  It gets the status report information from the received bundle
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The administrative bundle received
  * \param[in]  status_report       The status report contained in it
  *
  *****************************************************************************/

al_error al_bundle_get_status_report(al_types_bundle_object bundle_object,
		al_types_bundle_status_report **status_report) {
	if (bundle_object.payload->status_report==NULL) {
		return AL_ERROR;
	}
	else {
		*status_report = bundle_object.payload->status_report;
		return AL_SUCCESS;
	}

}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_cardinal_priority
  *
  * \brief  It gets the bundle cardinal priority.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  priority			The bundle cardinal priority
  *
  *****************************************************************************/

al_error al_bundle_get_cardinal_priority(al_types_bundle_object bundle_object,
		al_types_bundle_priority_enum *priority) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_priority: bundle object spec is null\n");
			return AL_ERROR;
	}
	*priority = bundle_object.spec->cardinal;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_cardinal_priority
  *
  * \brief  It sets the bundle ordinal priority.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  priority			The bundle cardinal priority
  *
  *****************************************************************************/

al_error al_bundle_set_cardinal_priority(al_types_bundle_object *bundle_object,
		al_types_bundle_priority_enum priority) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_priority: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_priority: bundle object spec is null\n");
			return AL_ERROR;
	}
	bundle_object->spec->cardinal = priority;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_ordinal_priority
  *
  * \brief  It gets the bundle ordinal priority.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  priority			The bundle priority
  *
  *****************************************************************************/

al_error al_bundle_get_ordinal_priority(al_types_bundle_object bundle_object,
		uint32_t priority) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_get_priority: bundle object spec is null\n");
			return AL_ERROR;
	}
	priority = bundle_object.spec->ecos.ordinal;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_ordinal_priority
  *
  * \brief  It sets the bundle ordinal priority.
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  priority			The bundle ordinal priority
  *
  *****************************************************************************/

al_error al_bundle_set_ordinal_priority(al_types_bundle_object *bundle_object,
		uint32_t priority) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_priority: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_priority: bundle object spec is null\n");
			return AL_ERROR;
	}
	bundle_object->spec->ecos.ordinal = priority;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_irf_trace
  *
  * \brief  It sets the inter-regional-forwarding trace report request flag
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  irf_trace			The inter-regional-forwarding trace report request flag (new ION versions)
  *
  *****************************************************************************/

al_error al_bundle_set_irf_trace(al_types_bundle_object *bundle_object,
		unsigned char irf_trace) {
	if (bundle_object == NULL){
		printf("Error in al_bundle_set_irf_trace: bundle object is null\n");
			return AL_ERROR;
	}
	if (bundle_object->spec == NULL){
		printf("Error in al_bundle_set_irf_trace: bundle object spec is null\n");
			return AL_ERROR;
	}
	bundle_object->spec->irf_trace = irf_trace;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_get_irf_trace
  *
  * \brief  It gets the inter-regional-forwarding trace report request flag
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  irf_trace			The inter-regional-forwarding trace report request flag (new ION versions)
  *
  *****************************************************************************/

al_error al_bundle_get_irf_trace(al_types_bundle_object bundle_object,
		unsigned char irf_trace) {
	if (bundle_object.spec == NULL){
		printf("Error in al_bundle_set_irf_trace: bundle object spec is null\n");
			return AL_ERROR;
	}
	irf_trace = bundle_object.spec->irf_trace;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_bp_version
  *
  * \brief  It sets the bp_version value
  *
  * \return al_error
  *
  * \param[in] 	bundle_object		The bundle object
  * \param[in]  bp_version			The bp_version value
  *
  *****************************************************************************/

al_error al_bundle_set_bp_version(al_types_bundle_object *bundle_object, uint8_t bp_version){

	if (bundle_object == NULL){
			printf("Error in al_bundle_set_irf_trace: bundle object is null\n");
				return AL_ERROR;
		}
		if (bundle_object->spec == NULL){
			printf("Error in al_bundle_set_irf_trace: bundle object spec is null\n");
				return AL_ERROR;
		}

	bundle_object->spec->bp_version = bp_version;
	return AL_SUCCESS;
}

/******************************************************************************
  *
  * \par Function Name:
  *      al_bundle_set_extension_block
  *
  * \brief  It sets the extension block structure
  *
  * \return al_error
  *
  * \param[in] 	bundle_object			The bundle object
  * \param[in]  metadata_type			The metadata type
  * \param[in]  metadata_string 		The metadata string
  *
  *****************************************************************************/


al_error al_bundle_set_extension_block(al_types_bundle_object *bundle_object , char * metadata_type, char * metadata_string){
	if (bundle_object == NULL){
			printf("Error in al_bundle_set_extension_block: bundle object is null\n");
				return AL_ERROR;
		}
		if (bundle_object->spec == NULL){
			printf("Error in al_bundle_set_extension_block: bundle object spec is null\n");
				return AL_ERROR;
		}
		//if not setted by the user
		if(strlen(metadata_type)==0 ){
			bundle_object->spec->extensions.extension_number=0;
			return AL_SUCCESS;
		}
		//default
		bundle_object->spec->extensions.extension_number=1; //only 1 extension_block
		bundle_object->spec->extensions.extension_blocks->block_processing_control_flags=0;
		bundle_object->spec->extensions.extension_blocks->block_type_code=METADATA_EXTENSION_BLOCK;
		bundle_object->spec->extensions.extension_blocks->crc_type=0;
		//concatenate metafata_type and metadata (string)in one string, the two fields separated by "/"
		char block_type_data [1000];//CCaini to be fixed
		sprintf(block_type_data,"%s/%s", metadata_type, metadata_string);

		bundle_object->spec->extensions.extension_blocks->block_data.block_type_specific_data_len = strlen(block_type_data) + 1; //compute the length
		//allocate the memory to the pointer
		bundle_object->spec->extensions.extension_blocks->block_data.block_type_specific_data = malloc(sizeof(char) * bundle_object->spec->extensions.extension_blocks->block_data.block_type_specific_data_len);
		//copy the string in the allocated memory
		strcpy(bundle_object->spec->extensions.extension_blocks->block_data.block_type_specific_data, block_type_data);
	return AL_SUCCESS;
}



