/** \file al_bundle_options.c
 *
 *  \brief  This file contains the parsing of the options.
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *  \authors Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *  \authors Federico Domenicali, federico.domenicali@studio.unibo.it
 *  \authors Silvia Lanzoni, silvia.lanzoni5@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | F. Domenicali   |  Initial Implementation.
 *  01/01/20 | A. Zappacosta   |  Implementation contribution.
 *  01/01/20 | A. Bisacchi	   |  Implementation contribution.
 *  01/01/22 | S. Lanzoni	   |  Implementation contribution.
 *  01/10/23 | L. Fantini      |  Dependencies on BP implementations eliminated
 *
 */
#include "unified_api_system_libraries.h"
#include "al_bundle_options.h"
#include "al_bundle.h"
#include "al_bp.h"

#define min(x, y) (((x) < (y)) ? (x) : (y))

#define lookup(option_index, compatible) \
if((error = al_bp_lookup_option_compatibility(option_index, compatible)) != BP_SUCCESS) {\
	printf("Error in al_bp_lookup_option_compatibility: %s\n", al_bp_strerror(error));\
	return AL_ERROR;\
}

#define handle_incompatibility(info, option_index, buff, size) \
if((error = al_bp_lookup_implementation_supporting_option(option_index, buff, size) != BP_SUCCESS)) {\
	printf("Error in al_bp_lookup_implementation_supporting_option: %s\n", al_bp_strerror(error));\
	return AL_ERROR;\
}\
printf("Parser error, the %s --%s option is available only in %s \n", info, long_options[option_index].name, buff);

static int find_index_from_short_option(const struct option *long_options, int val);
/**
 * \brief Enum which assign an int value to the bundle options in order to use the getopt function
 */

typedef enum {
	BSR_RCV,
	BSR_CST,
	BSR_FWD,
	BSR_DLV,
	BSR_DEL,
	APP,

	ORDINAL,
	UNRELIABLE,
	RELIABLE,
	CRITICAL,
	FLOW,
	MB_TYPE,
	MB_STRING,
	IRF,

	FORCE_EID,
	IPN_LOCAL,
	OPEN_WITH_IP,
	DEBUG,
} parser_options_val;

/**
 * \brief Function which assign a default value to the bundle options
 * The bp options fields are set following the order of the bp options structure fields
 * \par Notes: This function is private
 *
 */

void al_bundle_options_set_default(bundle_options_t *bundle_options) {
	bundle_options->bp_version = 0;
	bundle_options->lifetime = 60;
	bundle_options->cardinal = BP_PRIORITY_NORMAL;
	bundle_options->disable_fragmentation = FALSE;
	bundle_options->custody_transfer = FALSE;
	bundle_options->reception_reports = FALSE;
	bundle_options->custody_reports = FALSE;
	bundle_options->forwarding_reports = FALSE;
	bundle_options->delivery_reports = FALSE; //CCaini it should be false and set to true in dtnperf client only
	bundle_options->deletion_reports = FALSE;
	bundle_options->ack_requested_by_application = FALSE;

	//ecos
	bundle_options->ecos.ecos_enabled = TRUE;
	bundle_options->ecos.ordinal = 0;
	bundle_options->ecos.unreliable = FALSE;
	bundle_options->ecos.reliable = FALSE;
	bundle_options->ecos.critical = FALSE;
	bundle_options->ecos.flow_label = 0;
	//metadata
	bundle_options->metadata_type="";
	bundle_options->metadata_string="";
	//bundle_options->irf_trace=0; WORK IN  PROGRESS
}

/**
 * \brief Function which assign a default value to the dtn suite options
 * \par Notes: This function is private
 */

void al_bundle_options_set_default_dtn_suite(dtn_suite_options_t *dtn_suite_options) {
	dtn_suite_options->eid_format_forced = 'N';
	dtn_suite_options->ipn_local = 0;
	dtn_suite_options->debug_level=0;
	strcpy(dtn_suite_options->ip, "none");
	dtn_suite_options->port=0;
}

/******************************************************************************
 *
 * \par Function Name:
 *      al_bundle_options_parse
 *
 * \brief  Function to parse bundle and DTNsuite options passed on the command line.
 * Unrecognized options are saved in the "residual_options" structure to be later processed
 * by the application-specific parser.
 *
 * \return al_error
 *
 * \param[in]	argc					   	Number of arguments (separated by a space) passed on the command line (plus 1 for the application)
 * \param[in]	argv			          	The array of arguments passed on the command line
 * \param[in]	bundle_options	         	The structure where the bundle options are saved
 * \param[in]	dtn_suite_options			The structure where the dtnsuite options are saved
 * \param[in]	residual_options			The structure where the unrecognized options are saved
 *
 *
 * \par Notes: The options followed by a string (like the bundle destination EID) must contain a space between the option and the argument.
 *
 *
 *****************************************************************************/

al_error al_bundle_options_parse(int argc, char **argv,
		bundle_options_t *bundle_options,
		dtn_suite_options_t *dtn_suite_options,
		application_options_t* residual_options)
{
	al_bp_error_t error;
	//checks parameters
//CCaini perche' non anche le dtnsuite options?
	if (argv == NULL || bundle_options == NULL || residual_options == NULL)
		return AL_ERROR;

	//set default parameters
	al_bundle_options_set_default(bundle_options);
	al_bundle_options_set_default_dtn_suite(dtn_suite_options);

	residual_options->app_opts = malloc(sizeof(char *) * argc);
//The first argument "argv[0]" always contains the name of the program
//argc is the dimension of argv; as a mimimum is 1 (no options at all)
	residual_options->app_opts[0]=argv[0];//to mimic argv
	residual_options->app_opts_size = 1; //to mimic argc
	int app_index=1; //to start copying in residual_options[1], to mimic argv
//we deliberatley do not set residual_options[0], which
//would be skipped by any getopt_long() in application parsing function

	int option_read; 	   //UPDATE: actual result of the getopt_long
	int done=0;
	int old_optind=0;
	opterr = 0;
	optopt=0;
	//ip
	char ip_and_port[200];
	char * ip_temp;
	char ip [100];
	int port;
	//metadata
	boolean_t type_set = FALSE;

	char impl_names_buffer[256];
	boolean_t compatible = FALSE;

	/*This structure contains all long options and: 1) their translations into short options
	 *(if they have ones)) or 2) corresponding values*/
	static struct option long_options[] = {
			//bundle_options rcv, fwd, dlv, del
			{ "bp_version", required_argument, 0, 'V' },
			{ "lifetime", required_argument, 0, 'l' },
			{ "priority", required_argument, 0, 'p' },
			{ "do-not-fragment",no_argument, 0, 'N' },
			{ "custody-transfer", no_argument, 0,'C' 	},//BPv6 only
			{ "bsr-rcv", no_argument, 0, BSR_RCV },
			{ "bsr-cst", no_argument, 0, BSR_CST },//BPv6 only
			{ "bsr-fwd",no_argument, 0, BSR_FWD },
			{ "bsr-dlv", no_argument, 0, BSR_DLV },
			{ "bsr-del", no_argument, 0, BSR_DEL },
			{ "ack-req-by-app", no_argument, 0, APP },
			{ "ordinal",required_argument, 0, ORDINAL },
			{ "unreliable",no_argument, 0, UNRELIABLE },
			{ "reliable", no_argument, 0, RELIABLE },
			{ "critical", no_argument, 0, CRITICAL },
			{ "flow", required_argument, 0, FLOW },
			{ "mb-type",required_argument, 0, MB_TYPE },
			{ "mb-string",required_argument, 0, MB_STRING },
			{ "irf-trace",required_argument, 0, IRF },//not implemented yet
			//dtn_suite_options
			{ "force-eid", required_argument, 0, FORCE_EID },
			{ "ipn-local", required_argument, 0, IPN_LOCAL },
			{ "open-with-ip", required_argument, 0, OPEN_WITH_IP},
			{ "debug", required_argument, 0, DEBUG},

			{ 0, 0, 0, 0 }// The last element of the array has to be filled with zeros.
	};
	int option_index = 0;

	/* the third parameter must contain all short options (followed by a colon if
	 * they require an argument). The leading ":" returns ":" if a required argumen is missing*/

	int ipn_local_missing_flag=0;//required only in DTN2 if forced to IPN
	while (!done) {
		option_index = -1; // reset value, to distinguish long and short options
			// the first ':' of the optstring is for setting the return value of getopt_long()
			// to ':' instead of '?' to indicate a missing option argument.
		option_read = getopt_long(argc, argv, ":V:l:p:NC", long_options,
				&option_index);

		switch (option_read) {
		case 'V':
			if(option_index == -1) // matched by the short option
				option_index = find_index_from_short_option(long_options, option_read);
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->bp_version = atoi(optarg);
				break;
			} else {
				handle_incompatibility("-V", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case 'l':
			if(option_index == -1) // matched by the short option
							option_index = find_index_from_short_option(long_options, option_read);
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->lifetime = atoi(optarg);
				break;
			} else {
				handle_incompatibility("-l", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case 'p':
			if(option_index == -1) // matched by the short option
							option_index = find_index_from_short_option(long_options, option_read);
			lookup(option_index, &compatible);
			if(compatible) {
				if (!strcasecmp(optarg, "bulk")) {
					bundle_options->cardinal = BP_PRIORITY_BULK;
				} else if (!strcasecmp(optarg, "normal")) {
					bundle_options->cardinal = BP_PRIORITY_NORMAL;
				} else if (!strcasecmp(optarg, "expedited")) {
					bundle_options->cardinal = BP_PRIORITY_EXPEDITED;
				} else {
					printf("Parser error, wrong priority value \n");
					printf("%s\n", al_bundle_options_get_bundle_help());
					return AL_ERROR;
				}
				break;
			} else {
				handle_incompatibility("-p", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case 'N':
			if(option_index == -1) // matched by the short option
							option_index = find_index_from_short_option(long_options, option_read);
			option_index=1;
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->disable_fragmentation = TRUE;
				break;
			} else {
				handle_incompatibility("-N", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case 'C':
			if(option_index == -1) // matched by the short option
							option_index = find_index_from_short_option(long_options, option_read);
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->custody_transfer = 1;
				break;
			} else {
				handle_incompatibility("-C", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case BSR_RCV:
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->reception_reports = TRUE;
				break;
			} else {
				handle_incompatibility("-r", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case BSR_CST:
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->custody_reports = 1;
				break;
			} else {
				handle_incompatibility("-c", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case BSR_FWD:
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->forwarding_reports = TRUE;
				break;
			} else {
				handle_incompatibility("-f", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case BSR_DLV:
					lookup(option_index, &compatible);
					if(compatible) {
						bundle_options->delivery_reports = TRUE;
						break;
					} else {
						handle_incompatibility("", option_index, impl_names_buffer, sizeof(impl_names_buffer));
						printf("%s\n", al_bundle_options_get_bundle_help());
						return AL_ERROR;
					}
		case BSR_DEL:
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->deletion_reports = TRUE;
				break;
			} else {
				handle_incompatibility("", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case APP:
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->ack_requested_by_application = TRUE;
				break;
			} else {
				handle_incompatibility("", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case ORDINAL:
			lookup(option_index, &compatible);
			if(compatible) {
				if (bundle_options->cardinal != BP_PRIORITY_EXPEDITED) {
					printf("Parser error, ordinal priority option requires that the cardinal priority be expedited \n");
					printf("%s\n", al_bundle_options_get_bundle_help());
					return AL_ERROR;
				}
				if (atoi(optarg) > 254) {
					printf("Parser error, the ordinal priority must be < 254 \n");
					printf("%s\n", al_bundle_options_get_bundle_help());
					return AL_ERROR;
				} else {
					bundle_options->ecos.ordinal = atoi(optarg);
					bundle_options->ecos.ecos_enabled = TRUE;
				}
				break;
			} else {
				handle_incompatibility("", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case UNRELIABLE:
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->ecos.unreliable = TRUE;
				bundle_options->ecos.ecos_enabled = TRUE;
				break;
			} else {
				handle_incompatibility("ECOS", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}

		case RELIABLE:
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->ecos.reliable = TRUE;
				bundle_options->ecos.ecos_enabled = TRUE;
				break;
			} else {
				handle_incompatibility("ECOS", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}


		case CRITICAL:
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->ecos.critical = TRUE;
				bundle_options->ecos.ecos_enabled = TRUE;
				break;
			} else {
				handle_incompatibility("ECOS", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case FLOW:
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->ecos.flow_label = atoi(optarg);
				bundle_options->ecos.ecos_enabled = TRUE;
				break;
			} else {
				handle_incompatibility("ECOS", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case MB_TYPE:
			lookup(option_index, &compatible);
			if(compatible) {
				bundle_options->metadata_type = optarg;
				type_set=TRUE;
				break;
			} else {
				handle_incompatibility("Metadata Type", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case MB_STRING:
			lookup(option_index, &compatible);
			if(compatible) {
				if(type_set == FALSE) {
					printf("Parser error, metadata type must be set before metadata string \n");
					printf("%s\n", al_bundle_options_get_bundle_help());
					return AL_ERROR;
				}
				bundle_options->metadata_string = optarg;
				break;
			} else {
				handle_incompatibility("Metadata String", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}
		case IRF:
			lookup(option_index, &compatible);
			if(compatible) {
				//temporary solution
				bundle_options->irf_trace = (unsigned char) atoi(optarg);
				break;
			} else {
				handle_incompatibility("IRF (Inter regional Routing)", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_bundle_help());
				return AL_ERROR;
			}

//dtn_suite_options
		case FORCE_EID:
			lookup(option_index, &compatible);
			if(compatible) {
				if (!strcmp(optarg, "DTN")) {
					dtn_suite_options->eid_format_forced = 'D';
				} else if (!strcmp(optarg, "IPN")) {
					dtn_suite_options->eid_format_forced = 'I';
					int ipn_local_option_index = find_index_from_short_option(long_options,  IPN_LOCAL);
					if(ipn_local_option_index < 0) {
						printf("Parser error, couldn't find ipn_local option\n");
						return AL_ERROR;
					}
					boolean_t need_ipn_local_option = FALSE;
					lookup(ipn_local_option_index, &need_ipn_local_option);
					if(need_ipn_local_option) {
						ipn_local_missing_flag = 1;
					}
				} else {
					printf("Parser error, wrong value for the force eid option \n");
					printf("%s\n", al_bundle_options_get_dtnsuite_help());
					return AL_ERROR;
				}
				break;
			} else {
				handle_incompatibility("", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_dtnsuite_help());
				return AL_ERROR;
			}
		case IPN_LOCAL:
				lookup(option_index, &compatible);
				if(compatible) {
					if (!ipn_local_missing_flag) {
						printf("Parser error, ipn_local is set before force-eid \n");
						printf("%s\n", al_bundle_options_get_dtnsuite_help());
						return AL_ERROR;
					}
					if (atoi(optarg) <= 0) {
						printf("Parser error, the ipn_local value must be >0 \n");
						printf("%s\n", al_bundle_options_get_dtnsuite_help());
						return AL_ERROR;
					} else {
						dtn_suite_options->ipn_local = atoi(optarg);
					}
					ipn_local_missing_flag=0;
					break;
				} else {
					handle_incompatibility("", option_index, impl_names_buffer, sizeof(impl_names_buffer));
					printf("%s\n", al_bundle_options_get_dtnsuite_help());
					return AL_ERROR;
				}
		case OPEN_WITH_IP: //ip:port CCaini controllare per DTNME
			lookup(option_index, &compatible);
			if(compatible) {
				strcpy(ip_and_port, optarg);
				ip_temp = strtok(ip_and_port,":");
				strcpy(ip,ip_temp);
				port = atoi(strtok(NULL,"\0"));
				//check port
				if (port <1024 || port > 65535){
					printf("\nThe port number is not valid\n");
							printf("%s\n", al_bundle_options_get_dtnsuite_help());
					return AL_ERROR;
				}
				strcpy(dtn_suite_options->ip,ip);
				dtn_suite_options->port = port;
				//printf("\nthe ip stored is  %s\n",dtn_suite_options->ip);
				break;
			} else {
				handle_incompatibility("", option_index, impl_names_buffer, sizeof(impl_names_buffer));
				printf("%s\n", al_bundle_options_get_dtnsuite_help());
				return AL_ERROR;
			}
		case DEBUG:
			dtn_suite_options->debug_level = atoi(optarg);
		    //al_utilities_debug_print_debugger_init(debugLevel, false, "DTNperf_log");
			break;
		case ':':
				// optind is the index of the *next* element in argv[].
				printf("Missing arg for %s\n", argv[optind-1]);
	      		return AL_ERROR;
		case '?':
			//if optind = oldoptind -> do not save
			if (optind != old_optind) {
				if (argv[optind-1][0]== '-' && (app_index == 0 || argv[optind - 1] != residual_options->app_opts[app_index- 1])) {
					residual_options->app_opts[app_index] = argv[optind - 1];
					residual_options->app_opts_size++;
					app_index++;
					//After option insertion check if the argument must be saved
					if (optind < argc) {
						if (argv[optind][0] != '-') {
							residual_options->app_opts[app_index] =argv[optind];
							residual_options->app_opts_size++;
							app_index++;
						}
					}
				}
				old_optind = optind;
			}
			break;
		case  -1:
			done = -1;
			if (ipn_local_missing_flag) {
					printf("Parser error, ipn_local is missing \n");
					printf("%s\n", al_bundle_options_get_dtnsuite_help());
					return AL_ERROR;}
		default:
			break;
		}

	}//while

	return AL_SUCCESS;
}

/**
 * \brief Function to free the result structure
 */

void al_bundle_options_free(application_options_t result_options) {
	free(result_options.app_opts);
}

/**
 * \brief Function which returns the help of the dtnsuite options
 */

char * al_bundle_options_get_dtnsuite_help(void){
    return "\nDTNsuite options:\n"
    		"     --force-eid <[DTN|IPN]> Force the EID registration scheme.\n"
			"     --ipn-local <num>        Set the ipn local number (DTNME only)\n"
    		"     --open-with-ip <ip:port> Open the connection using the ip and the port specified."
    		"     --debug[=level]          Debug messages [1-2]; if level is not indicated level = 2.\n";
}

/**
 * \brief Function which returns the help of the bundle options
 */

char * al_bundle_options_get_bundle_help(void){
    return		"\nbundle options:\n"
    		" -V, --bp_version <num>       Bundle Protocol Version (6, 7, or 0 = DTNME default) (DTNME Only)\n"
       		" -l, --lifetime <time>        Bundle lifetime (always in s). Default is 60 s.\n"
       		" -p, --priority <val>         Bundle  priority [bulk|normal|expedited]. Default is normal.\n"
    		" -N, --do-not-fragment        Disable bundle fragmentation (ION & DTNME only).\n"
			" -C, --custody-transfer       Request of custody transfer option.\n"
       		"     --bsr-cst				   Request of \"custody acceptance\" status reports.\n"
    		"     --bsr-rcv                Request of \"received\" status reports.\n"
			"     --bsr-fwd                Request of \"forwarded\" status reports.\n"
			"     --bsr-del                Request of \"deleted\" status reports.\n"
    		"     --bsr-dlv                Request of \"delivered\" status reports.\n"
    		"     --ack-req-by-app         Request of ack by the application. (IBR only) \n"
    		"     --ordinal <num>          ECOS \"ordinal priority\" [0-254]. Default: 0 (ION & DTNME & Unibo-BP).\n"
    		"     --unreliable             Set ECOS \"unreliable flag\" to True. Default: False (ION & DTNME & Unibo-BP).\n"
    		"     --reliable               Set ECOS \"reliable flag\" to True. Default: False (DTNME & Unibo-BP).\n"
    		"     --critical               Set ECOS \"critical flag\" to True. Default: False (ION & DTNME & Unibo-BP).\n"
    		"     --flow <num>             ECOS \"flow\" number. Default: 0 (ION & DTNME & Unibo-BP).\n"
			"     --mb-type <type>         Include metadata block and specify type(ION & DTNME & Unibo-BP).\n"
			"     --mb-string <string>     Extension/metadata block content (ION & DTNME & Unibo-BP). \n"
    		"     --irf-trace <char>       Extension to inter-regional routing (ION exp.vers. only).\n"
    		"Warning: in UD3TN, all bundle options will be ignored; they can only be set once, at BPA start";
}

/**
 * \brief Function which sets the bundle options given in input to the bundle structure
 */

al_error al_bundle_options_set(al_types_bundle_object *bundle,
		bundle_options_t bundle_options) {
	al_error bundle_err;
	al_types_bundle_processing_control_flags dopts = BP_DOPTS_NONE;

	// BP version (DTNME only)
	bundle->spec->bp_version = bundle_options.bp_version;

	// Do not fragment
	if (bundle_options.disable_fragmentation)
		dopts |= BP_DOPTS_DO_NOT_FRAGMENT;

	// Custody request
	if (bundle_options.custody_transfer
			&& (bundle_options.ecos.critical == FALSE))
		dopts |= BP_DOPTS_CUSTODY;

	// Receive bundle status report
	if (bundle_options.reception_reports)
		dopts |= BP_DOPTS_RECEPTION_BSR;

	// Custody bundle status report
	if (bundle_options.custody_reports)
		dopts |= BP_DOPTS_CUSTODY_BSR;

	// Forward bundle status report
	if (bundle_options.forwarding_reports)
		dopts |= BP_DOPTS_FORWARD_BSR;

	// Delivery bundle status report
	if (bundle_options.delivery_reports)
		dopts |= BP_DOPTS_DELIVERY_BSR;

	// Deleted bundle status report
	if (bundle_options.deletion_reports)
		dopts |= BP_DOPTS_DELETION_BSR;

	// Ack requested by application
	if (bundle_options.ack_requested_by_application)
		dopts |= BP_DOPTS_ACK_REQUESTED_BY_APPLICATION;

	//  Set options
	bundle_err = al_bundle_set_control_flags(bundle, dopts);
	if (bundle_err != AL_SUCCESS) {
		return AL_ERROR;
	}

	// Bundle lifetime
	bundle_err = al_bundle_set_lifetime(bundle, bundle_options.lifetime);
	if (bundle_err != AL_SUCCESS) {
		return AL_ERROR;
	}

	// Bundle cardinal priority
	bundle_err = al_bundle_set_cardinal_priority(bundle,
			bundle_options.cardinal);
	if (bundle_err != AL_SUCCESS) {
		return AL_ERROR;
	}

	// ECOS enabled (DTNME only) At present, Oct. 2021, it does not work in both BP versions
	bundle->spec->ecos.ecos_enabled = bundle_options.ecos.ecos_enabled;

	// Bundle ordinal priority
	bundle_err = al_bundle_set_ordinal_priority(bundle,
			bundle_options.ecos.ordinal);
	if (bundle_err != AL_SUCCESS) {
		return AL_ERROR;
	}

	// Bundle unreliable
	bundle_err = al_bundle_set_unreliable(bundle,
			bundle_options.ecos.unreliable);
	if (bundle_err != AL_SUCCESS) {
		return AL_ERROR;
	}

	// Bundle reliable
	bundle_err = al_bundle_set_reliable(bundle,
			bundle_options.ecos.reliable);
	if (bundle_err != AL_SUCCESS) {
		return AL_ERROR;
	}
	// Bundle critical
	bundle_err = al_bundle_set_critical(bundle, bundle_options.ecos.critical);
	if (bundle_err != AL_SUCCESS) {
		return AL_ERROR;
	}

	// Bundle flow label
	bundle_err = al_bundle_set_flow_label(bundle,
			bundle_options.ecos.flow_label);
	if (bundle_err != AL_SUCCESS) {
		return AL_ERROR;
	}

	//metadata
	bundle_err = al_bundle_set_extension_block(bundle, bundle_options.metadata_type, bundle_options.metadata_string);
	if (bundle_err != AL_SUCCESS) {
			return AL_ERROR;
		}

	//inter regional routing
	bundle_err = al_bundle_set_irf_trace(bundle, bundle_options.irf_trace);
	if (bundle_err != AL_SUCCESS) {
		return AL_ERROR;
	}

	return AL_SUCCESS;
}

static int find_index_from_short_option(const struct option *long_options, int val) {
	int result = -1;
	for (int i = 0; long_options[i].name != NULL && result == -1; i++)
	{
		if(long_options[i].val == val) {
			result = i;
		}
	}
	if(result == -1)
		printf("Error in find_index_from_short_option: the option %c (%d) was not found in long_options[]!\n", val, val);
	return result;
}
