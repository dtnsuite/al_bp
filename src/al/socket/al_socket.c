/** \file al_socket.c
 *
 *  \brief  This file contains all the al_socket functions.
 *
 *  \details These functions have to be called directly by the DTN applications;
 *  they are designed to implement a socket like interface to the abstract bundle protocol.
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify				<br>
 *     it under the terms of the GNU General Public License as published by			<br>
 *     the Free Software Foundation, either version 3 of the License, or			<br>
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,					<br>
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of				<br>
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the				<br>
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *  \authors Silvia Lanzoni, silvia.lanzoni5@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/20 | A. Bisacchi	   |  Initial Implementation.
 *  01/10/21 | S. Lanzoni      |  Implementation refactoring and documentation.
 *
 */

#include "unified_api_system_libraries.h"
//#include <semaphore.h>
//#include <libgen.h>
//#include <sched.h>

#include "al_socket.h"
#include "al_bundle.h"
#include "al_utilities_bundle_print.h"
#include "al_bp.h"
#include "list.h"


/**
 * \brief Structure that groups the elements that characterize a registration.
 */

typedef struct {
	/**
	 * \brief Registration state.
	 */
	al_types_reg_info reginfo;
	/**
	 * \brief Local enpdoint identifier.
	 */
	al_types_endpoint_id local_eid;
	/**
	 * \brief Handle object, used in al_bp layer.
	 */
	al_types_handle handle;
	/**
	 * \brief Mutex for half-duplex send/receive (i.e. not at the same time).
	 */
	pthread_mutex_t mutex_half_duplex;
	/**
	 * \brief Registration cookie used in al_bp layer.
	 */
	al_types_reg_id regid;
	/**
	 * \brief Error of layer al_bp.
	 */
	al_bp_error_t error;
	/**
	 * \brief Registration descriptor.
	 */
	al_socket_registration_descriptor reg_des;

} al_socket_registration_t;

/**
 * \brief List used to store the registration descriptors.
 */
static List registration_list = empty_list;

/**
 * \brief Tells if the list is initialized or not.
 */

static boolean_t initialized = FALSE;

/**
 * \brief Defines the active implementation.
 */

//static al_types_implementation bp_implementation = BP_NONE; // Active BP implementation (global to all socket functions)

// Private functions
static void remove_registration_from_list(
		al_socket_registration_descriptor reg_des);
static void insert_registration_in_list(al_socket_registration_t registration);
static al_socket_registration_t* get_registration_from_reg_des(
		al_socket_registration_descriptor reg_des);
static int compare_registration_and_reg_des(void *data1, size_t data1_size,
		void *data2, size_t data2_size);

/******************************************************************************
 *
 * \par Function Name:
 *      al_socket_register
 *
 * \brief  This function registers a new connection of the application to the BP.
 *
 * \return al_error
 *
 * \param[in]	registration_descriptor    	Identifier of the registration
 * \param[in]	dtn_demux_string           	DTN demux token (string)
 * \param[in]	ipn_service_number         	Service number to be used in case of an ipn scheme registration
 * \param[in]	force_eid_scheme			The "force_eid" (N|D|I) is used to specify if the default format <br>
 *  of the registration must be overridden ("N" no, i.e. use the default, 'D' for "dtn", 'I' for "ipn")
 * \param[in]	ipn_node_number_for_DTN2    Ipn node identifier.
 * \param[in]	ip 							(Optional) Ip address used to open the connection
 * \param[in]	port						(Optional) Port used to open the connection
 *
 *
 * \par Notes:
 *            It returns, in addition to the possible error, the registration_descriptor	<br>
 *            (an integer inspired to file descriptor in sockets, but not passed by			<br>
 *            the OS) to be given in input to the al_socket functions that work on a specific registration. <br>
 *
 *****************************************************************************/

al_error al_socket_register(
		al_socket_registration_descriptor *registration_descriptor,
		char *dtn_demux_string, int ipn_service_number, char force_eid_scheme,
		int ipn_node_number_for_DTN2, char *ip, int port) {
	al_socket_registration_t registration;
	al_bp_error_t result;
	*registration_descriptor = 0;
	static al_socket_registration_descriptor max_registration_descriptor = 1;
	al_types_scheme URI_scheme;

	// The "force_eid" (N|D|I) is used to specify if the default format of the registration must
	// be overridden ("N" no, i.e. use the default, 'D' for "dtn", 'I' for "ipn").

	if (force_eid_scheme == 'I') {
		URI_scheme = IPN_SCHEME;
	} else if (force_eid_scheme == 'D') {
		URI_scheme = DTN_SCHEME;
	} else if (force_eid_scheme == 'N') {
		result = al_bp_get_default_scheme_running_bpi(&URI_scheme);
		if(result != BP_SUCCESS) {
			printf("Error in al_socket_register: unable to get default URI scheme due to %s\n", al_bp_strerror(result));
			return AL_ERROR;
		}
	} else {
		printf("Error in al_socket_register: The force_eid given in input is not correct \n");
		return AL_ERROR;
	}

	//Checks other input parameters
	if (URI_scheme == DTN_SCHEME && dtn_demux_string == NULL) //dtn
	{
		printf("Error in al_socket_register: DTN parameters given in input are not correct \n");
		return AL_ERROR;
	}
	if (URI_scheme == IPN_SCHEME && ipn_service_number < 0) //ipn
	{
		printf("Error in al_socket_register: IPN parameters given in input are not correct \n");
		return AL_ERROR;
	}
	if (ipn_node_number_for_DTN2 < 0)
	{
		printf("Error in al_socket_register: ipn_node_number is < 0 \n");
		return AL_ERROR;
	}


	memset(&registration, 0, sizeof(al_socket_registration_t)); // set all to 0

	registration.reg_des = max_registration_descriptor;

	//open
	if(strcmp(ip, "none")){ // open with ip
		//printf("calling al_bp_open_with_ip with ip %s  and port %d\n", ip,port);
		registration.error = al_bp_open_with_ip(ip, port, &(registration.handle));
	}
	else{ //open with unix socket
		registration.error = al_bp_open(&(registration.handle));
		//TBD
		//registration.error = al_bp_open(sockpath, &(registration.handle));
	}

	if (registration.error != BP_SUCCESS){
		printf("Error in al_socket_register: al_bp_open failed due to %s\n", al_bp_strerror(registration.error));
		return AL_ERROR;
	}

	//get local eid
	registration.error = al_bp_build_local_eid(
								(registration).handle, &((registration).local_eid), 
								ipn_node_number_for_DTN2, ipn_service_number,
								dtn_demux_string, 
								URI_scheme);
	if(registration.error != BP_SUCCESS) {
		printf("Error in al_socket_build_local_eid: %s\n", al_bp_strerror(registration.error));
		return AL_ERROR;
	}

	//set reginfo
	al_bp_copy_eid(&(registration.reginfo.endpoint), &(registration.local_eid));
	registration.reginfo.flags = BP_REG_DEFER;
	registration.reginfo.regid = BP_REGID_NONE;
	registration.reginfo.expiration = 0;

	//register
	registration.error = al_bp_register(&(registration.handle),
			&(registration.reginfo), &(registration.regid), &(registration.mutex_half_duplex));

	if (registration.error != BP_SUCCESS) {
		printf("Error in al_socket_register: %s\n", al_bp_strerror(registration.error));
		return AL_ERROR;
	}

	// put registration in the list
	insert_registration_in_list(registration);

	//print registration
	printf("Registered at: ");
	//al_utilities_bundle_print_endpoint_id(registration.local_eid, "Local eid:", 0, stdout); //test
	al_utilities_bundle_print_str(registration.local_eid.uri, "Eid", 0, stdout);

	*registration_descriptor = registration.reg_des;
	max_registration_descriptor++;

	return AL_SUCCESS;
}

/******************************************************************************
 *
 * \par Function Name:
 *      al_socket_unregister
 *
 * \brief  It deletes the registration identified by the registration descriptor given in input.
 *
 * \return al_error
 *
 * \param[in]	registration_descriptor      The identifier of the registration we want to delete
 *
 *
 *****************************************************************************/

al_error al_socket_unregister(
		al_socket_registration_descriptor registration_descriptor) {
	al_error error;
	al_socket_registration_t *registration;

	registration = get_registration_from_reg_des(registration_descriptor);
	if (registration == NULL) {
		printf("Error in al_socket_unregister: Not registered \n");
		return AL_ERROR;
	}

	error = AL_SUCCESS;

    if ((registration->error = al_bp_close_and_unregister(registration->handle, registration->regid,
                             registration->local_eid, &(registration->mutex_half_duplex))) != BP_SUCCESS) {
		printf("Error in al_socket_close_and_unregister: %s\n", al_bp_strerror(registration->error));
        error = AL_ERROR;
    }

	// remove from list
	remove_registration_from_list(registration->reg_des);

	return error;
}

/******************************************************************************
 *
 * \par Function Name:
 *      al_socket_send
 *
 * \brief  It sends a bundle object.
 *
 * \return al_error
 *
 * \param[in]	registration_descriptor		Identifier of the registration.
 * \param[in]	bundle             			The bundle object that will be sent.
 * \param[in]	destination          		Endpoint id of the destination.
 * \param[in]	report_to             		Endpoint id of the report_to.
 *
 * \par Notes:
 * 	This function uses the registration_descriptor to identify the registration; then it sends a bundle object. It is a wrapper of the
 * al_bp_send, but the destination and the "report to" EIDs are passed in input. For the sake of backward compatibility the
 * destination and "report to" originally contained in the bundle object are saved and restored at the end of the function.
 *
 *
 *****************************************************************************/

al_error al_socket_send(
		al_socket_registration_descriptor registration_descriptor,
		al_types_bundle_object bundle, al_types_endpoint_id destination,
		al_types_endpoint_id report_to)
{
	al_socket_registration_t *registration;
	al_types_endpoint_id old_destination;
	al_types_endpoint_id old_report_to;
	al_types_handle handle;

	registration = get_registration_from_reg_des(registration_descriptor);
	if (registration == NULL) {
		printf("Error in al_socket_send: Not registered \n");
		return AL_ERROR;
	}

	if (strcmp(destination.uri, "dtn:none") == 0){
		printf("Warning in al_socket_send: The destination is not indicated or is dtn:none \n");
		return AL_WARNING_DESTINATION;
	}

	old_destination = bundle.spec->destination;
	// save old values
	old_report_to = bundle.spec->report_to;
	al_bundle_set_source(&bundle, registration->local_eid);
	al_bundle_set_dest(&bundle, destination);
	al_bundle_set_report_to(&bundle, report_to);

	// This should avoid the blocking in getting handle
	/*if (registration->bp_implementation == BP_DTN)
	 handle = registration->handle_send;
	 else
	 handle = registration->handle;*/

	handle = registration->handle;

#ifdef DEBUG
	debug_print_bundle_object(bundle);
	#endif

	registration->error = al_bp_send(handle, registration->regid, bundle.spec,
			bundle.payload, &(registration->mutex_half_duplex));

	// reset old destination and old report_to
	al_bundle_set_dest(&bundle, old_destination);
	al_bundle_set_report_to(&bundle, old_report_to);
	if(registration->error != BP_SUCCESS) {
		printf("Error in al_socket_send: %s\n", al_bp_strerror(registration->error));
		return AL_ERROR;
	}
	return AL_SUCCESS;
}

/******************************************************************************
 *
 * \par Function Name:
 *      al_socket_receive
 *
 * \brief  It receives a bundle object destined to the registration_descriptor given in input.
 *
 * \return al_error
 *
 * \param[in]	registration_descriptor		Identifier of the registration.
 * \param[in]	bundle             			The bundle object that will be sent.
 * \param[in]	payload_location            The payload location which can be received either in a file.
 * \param[in]	timeval            			BP timeout.
 *
 * \par Notes:
 * It is a wrapper of the al_bp_receive.
 *
 *
 *****************************************************************************/

al_error al_socket_receive(
		al_socket_registration_descriptor registration_descriptor,
		al_types_bundle_object *bundle,
		al_types_bundle_payload_location payload_location, al_types_timeout timeout_in_ms)
{

	al_socket_registration_t *registration;

	if (bundle == NULL){
		printf("Error in al_socket_receive: Input bundle is null \n");
		return AL_ERROR;
	}

	registration = get_registration_from_reg_des(registration_descriptor);
	if (registration == NULL) {
		printf("Error in al_socket_receive: Not registered \n");
		return AL_ERROR;
	}

	al_bp_free_payload(bundle->payload);
	al_bp_free_extensions(bundle->spec);
	memset(bundle->payload, 0, sizeof(al_types_bundle_payload));
	memset(bundle->spec, 0, sizeof(al_types_bundle_spec));
	// printf("%s pre al_bp_recv\n", __FUNCTION__);
	registration->error = al_bp_recv(registration->handle, bundle->spec,
			payload_location, bundle->payload, timeout_in_ms, &(registration->mutex_half_duplex));
	// printf("%s post al_bp_recv\n", __FUNCTION__);

	switch (registration->error) {
	case BP_SUCCESS:
		return AL_SUCCESS;
	case BP_ERECVINT:
		//printf("Warning in al_socket_receive: Reception is interrupted \n");
		return AL_WARNING_RECEPINTER;
	case BP_ETIMEOUT:
		//printf("Warning in al_socket_receive: Timeout \n");
		return AL_WARNING_TIMEOUT;
	default:
		printf("Error in al_socket_receive: %s\n", al_bp_strerror(registration->error));
		return AL_ERROR;
	}
}

/******************************************************************************
 *
 * \par Function Name:
 *      al_socket_init
 *
 * \brief  It initializes the socket layer.
 *
 * \return al_error
 *
 * \par Notes:
 * It also discovers the active BP implementation and saves this information in a global
 * (to this file) variable available to other al_socket functions. If it does not find
 * any active BP implementation, or if it finds multiple implementations, or
 * if the running BP implementation is not among thos for which the code was compiled
 * it returns a specific error.
 *
 *****************************************************************************/

al_error al_socket_init() {
	al_bp_error_t result;

	//Checks if the list is initialized
	if (initialized){
		printf("Error in al_socket_init: the list can't be initialized \n");
		return AL_ERROR;
	}

	//Discover the running BP implementation and performs a few checks
	if((result = al_bp_get_implementation()) != BP_SUCCESS) {
		if(result == BP_EBADBPI)//Unified API not compiled for the running BPimplementation
			printf("Error in al_socket_init: %s [%s]\n", al_bp_strerror(result), al_bp_get_implementation_name());
		else
			printf("Error in al_socket_init: %s\n", al_bp_strerror(result));
		return AL_ERROR;
	}
	
	printf("Running BP implementation: %s\n", al_bp_get_implementation_name());

	printf("UNIFIED_API version:   %s\n", get_unified_api_version());

	//List initialization
	registration_list = empty_list;
	initialized = TRUE;
	return AL_SUCCESS;
}

/******************************************************************************
 *
 * \par Function Name:
 *      al_socket_find_registration
 *
 * \brief  It checks for an existing registration on the given endpoint id.
 *
 * \return al_bp_error_t
 *
 * \param[in]	registration_descriptor     The registration descriptor corresponding to the EID passed in input.
 * \param[in]	eid							Endpoint identifier.
 *
 * \par Notes:
 * It is a wrapper of the al_bp_find_registration.
 *
 *****************************************************************************/

al_error al_socket_find_registration(
		al_socket_registration_descriptor registration_descriptor,
		al_types_endpoint_id *eid) {
	al_socket_registration_t *registration;

	if (eid == NULL) {
		printf("Error in al_socket_find_registration: The eid is null \n");
		return AL_ERROR;
	}

	registration = get_registration_from_reg_des(registration_descriptor);
	if (registration == NULL) {
		printf("Error in al_socket_find_registration: The registration is null \n");
		return AL_ERROR;
	}

	registration->error = al_bp_find_registration(registration->handle, eid,
			&(registration->regid));
	if (registration->error != BP_SUCCESS) {
		// TODO: BP_EBUSY is a success ("yes, I've found it") or an error ("no, you can't use it")?
		printf("Error in al_socket_find_registration: %s\n", al_bp_strerror(registration->error));
		return AL_ERROR;
	}
	return AL_SUCCESS;
}

/******************************************************************************
 *
 * \par Function Name:
 *      al_socket_destroy
 *
 * \brief  It destroys the registration list.
 *
 * \return void
 *
 * \par Warning: After calling this function, the programmer can no more use any al_socket functions except the al_socket_init
 *
 *****************************************************************************/

void al_socket_destroy() {
	if (!initialized) {
		printf("Error in al_socket_destroy: Not initialized \n");
		return;
	}
	initialized = FALSE;

	// dealloc all list
	list_destroy(&registration_list);
}

/******************************************************************************
 *
 * \par Function Name:
 *      al_socket_str_type_error
 *
 * \brief  It returns the string of the error passed in input.
 *
 * \return string
 *
 * \param[in]	error 	Error we want to obtain the message from.
 *
 *****************************************************************************/

char* al_socket_str_type_error(al_error error) {
	switch (error) {
	case AL_SUCCESS:
		return "success";
	case AL_WARNING_DESTINATION:
		return "destination EID is dtn:none";
	case AL_WARNING_RECEPINTER:
		return "bundle reception interrupted";
	case AL_WARNING_TIMEOUT:
		return "bundle reception timeout expired";
	case AL_ERROR:
		return "general error";
	default:
		return "unknown error";
	}
}


/******************************************************************************
 *
 * \par Function Name:
 *      al_socket_get_local eid
 *
 * \brief  It returns the local EID structure associated to the registration descriptor given in input.
 *
 * \return al_types_endpoint_id
 *
 * \param[in]	registration_descriptor		Identifier of the registration.
 *
 *****************************************************************************/

al_types_endpoint_id al_socket_get_local_eid( //*
		al_socket_registration_descriptor registration_descriptor) {
	al_socket_registration_t *registration;
	registration = get_registration_from_reg_des(registration_descriptor);
	if (registration == NULL) {
		al_types_endpoint_id none;
		al_bp_get_none_endpoint(&none);
		return none;
	}
	return registration->local_eid;
}


/***************************************************
 *               PRIVATE FUNCTIONS                 *
 ***************************************************/


/******************************************************************************
 *
 * \par Function Name:
 *      remove_registration_from_list
 *
 * \brief  It removes a registration description from the list.
 *
 * \return void
 *
 * \param[in]	registratin_descriptor 	identifier of the registration
 *
 * \par Warning: This is a private function.
 *
 *****************************************************************************/

static void remove_registration_from_list(
		al_socket_registration_descriptor registration_descriptor) {
	list_remove_data(&registration_list, &registration_descriptor,
			sizeof(registration_descriptor), &compare_registration_and_reg_des);
}

/******************************************************************************
 *
 * \par Function Name:
 *      insert_registration_from_list
 *
 * \brief  It inserts a registration description in the list.
 *
 * \return void
 *
 * \param[in]	registratin_descriptor 	identifier of the registration
 *
 * \par Warning: This is a private function.
 *
 *****************************************************************************/

static void insert_registration_in_list(al_socket_registration_t registration) {
	// insert in the list
	list_push_back(&registration_list, &registration, sizeof(registration));
}

/******************************************************************************
 *
 * \par Function Name:
 *      get_registration_from_reg_des
 *
 * \brief  It obtains the registration from the registration descriptor given as input.
 *
 * \return void
 *
 * \param[in]	registratin_descriptor 	identifier of the registration
 *
 * \par Warning: This is a private function.
 *
 *****************************************************************************/

static al_socket_registration_t* get_registration_from_reg_des(
		al_socket_registration_descriptor registration_descriptor) {
	al_socket_registration_t *registration_pointer;

	registration_pointer = (al_socket_registration_t*) list_get_pointer_data(
			registration_list, &registration_descriptor,
			sizeof(al_socket_registration_descriptor),
			&compare_registration_and_reg_des);

	return registration_pointer;
}

/******************************************************************************
 *
 * \par Function Name:
 *      insert_registration_from_list
 *
 * \brief  It compares registration and registration descriptor
 *
 * \return int
 *
 * \par Warning: This is a private function.
 *
 *****************************************************************************/

//data1 = registration
//data2 = reg_des
static int compare_registration_and_reg_des(void *data1, size_t data1_size,
		void *data2, size_t data2_size) {
	al_socket_registration_t registration;
	al_socket_registration_descriptor reg_des;

	registration.reg_des = reg_des = (al_socket_registration_descriptor) -1; // may be uninitialized

	if (data1 == NULL)
		return -1;
	if (data2 == NULL)
		return 1;

	if (data1_size == sizeof(al_socket_registration_t))
		registration = *((al_socket_registration_t*) data1);
	else if (data1_size == sizeof(al_socket_registration_descriptor))
		reg_des = *((al_socket_registration_descriptor*) data1);
	else
		return -1;

	if (data2_size == sizeof(al_socket_registration_t))
		registration = *((al_socket_registration_t*) data2);
	else if (data2_size == sizeof(al_socket_registration_descriptor))
		reg_des = *((al_socket_registration_descriptor*) data2);
	else
		return 1;

	if (reg_des == -1 || registration.reg_des == -1)
		return -1;

	return (reg_des - registration.reg_des);
}
