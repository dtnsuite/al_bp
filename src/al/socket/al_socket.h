/** \file al_socket.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *  \details These functions have to be called directly by the DTN applications;
 *  they are designed to implement a socket like interface to the abstracted bundle protocol.
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/20 | A. Bisacchi	   |  Initial Implementation.
 *
 */


#ifndef AL_SOCKET_H_
#define AL_SOCKET_H_

#include "../types/al_types.h"

/**
 * \brief This is an identifier of the registration, one for each registration.
 */

typedef unsigned int al_socket_registration_descriptor;

// This function registers a new connection of the application to the BP.
// Output:
// 		registration_descriptor: the registration descriptor (&gt;0; 0 in case of error)
// Input:
// 		dtn_demux string: dtn demux token (string)
// 		ipn_demux_number: demux token (number )to be used in case of an ipn scheme registration (&gt;0)
// It returns, in addition to the possible error, the registration_descriptor (an integer somewhat inspired to file descriptor in sockets, but not passed by
// the OS) to be given in input to “al_socket” functions that work on a specific registration.
al_error al_socket_register(al_socket_registration_descriptor* registration_descriptor, char* dtn_demux_string, int ipn_demux_number, char force_eid_scheme, int ipn_node_for_DTN2, char * open_with_ip, int port);

// It unregisters the registration identified by the registration descriptor.
al_error al_socket_unregister(al_socket_registration_descriptor registration_descriptor);

// This function uses the registration_descriptor to identify the 
// registration;
al_error al_socket_send(al_socket_registration_descriptor registration_descriptor, al_types_bundle_object bundle, al_types_endpoint_id destination, al_types_endpoint_id report_to);

// It receives a bundle object destined to the registration_descriptor 
// passed in input.
al_error al_socket_receive(al_socket_registration_descriptor registration_descriptor, al_types_bundle_object* bundle, al_types_bundle_payload_location payload_location, al_types_timeout timeout_in_ms);

// It initializes the Abstraction Layer extension "B". The "force_eid" 
// (N|D|I) is used to specify if the default format of the registration 
// must be overridden ("N" no, i.e. use the default, 'D' for "dtn", 'I' 
// for "ipn"); it also discovers the active BP implementation and saves 
// this information in a local variable available to other socket
// functions. If it does not find any active BP implementation, it 
// returns a specific error.
// ipn_node_for_DTN2: ipn node number used only in case of registration following the ipn scheme on a DTN2 machine
al_error al_socket_init();

// It destroys the registration list. After calling this function, the 
// programmer can no more use any al_socket functions except the al_socket_init.
void al_socket_destroy();

// It is a wrapper of al_bp_find_registration. It gives in output the 
// registration descriptor corresponding to the EID passed in input.
al_error al_socket_find_registration(al_socket_registration_descriptor registration_descriptor, al_types_endpoint_id* eid);

char* al_socket_str_type_error(al_error error);

// It returns the local EID structure associated to the registration 
// descriptor given in input.
al_types_endpoint_id al_socket_get_local_eid(al_socket_registration_descriptor registration_descriptor);

#endif /* AL_SOCKET_H_ */
