/** \file al_types.h
 *
 *  \brief  This file contains the definition of all types specific to the Unified API
 *
 *  \par Copyright
 *  	Copyright (c) 2016, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Davide Pallotti, davide.pallotti@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *  01/01/16 | D. Pallotti	   |  Implementation contribution.
 *  01/01/22 | S. Lanzoni	   |  Implementation contribution.
 *
 *
 */

/*
 * bp_types.h
 *
 */

#ifndef AL_TYPES_H_
#define AL_TYPES_H_

#include <stdint.h>

#include "unified_api_boolean_type.h"

//Unified_api (ex al_bp) version
#define UNIFIED_API_VERSION_STRING	"2.2.0"

#define AL_TYPES_MAX_EID_LENGTH 256

//al error structure
typedef enum al_error
{
	/**
		* \brief Successful function outcome
	*/
	AL_SUCCESS=0,
	/**
		* \brief Fail function outcome
	*/
	AL_ERROR=1,
	/**
		* \brief General warning function outcome
	*/
	AL_WARNING=2,
	/**
		* \brief Warning the destination EID is not indicated or is dtn:none (al_socket_send() specific error).
	*/
	AL_WARNING_DESTINATION=3,
	/**
		* \brief Warning, timed out, (al_socket_receive() specific error).
	*/
	AL_WARNING_TIMEOUT=4,
	/**
		* \brief Warning, reception interrupted, (al_socket_receive() specific error).
	*/
	AL_WARNING_RECEPINTER=5
} al_error;



/**
 * \brief Bundle protocol implementation
 */
typedef enum
{
    BP_DTNME = 0, //Also for DTN2
    BP_ION,
	BP_IBR,
	BP_UD3TN,
    BP_UNIBOBP,
	
	IMPLEMENTATION_COUNTER, // add your new implementation in the row above!
	BP_NONE,
	BP_MULTIPLE, //Two or more BP implementations running

} al_types_implementation;

/**
 * \brief DTN eid Scheme block_type_code
 */
typedef enum
{
	IPN_SCHEME = 0,
	DTN_SCHEME,
} al_types_scheme;

/**
 * \brief Specification of a bp endpoint id, i.e. a URI.
 *
 * Implemented as a fixed-length char buffer. Note that for efficiency reasons, this
 * fixed length is relatively small (256 bytes).
 *
 * The alternative is to use the string XDR block_type_code but then all endpoint
 * ids would require malloc / free which is more prone to leaks / bugs.
 */

typedef struct al_types_endpoint_id {
	char uri[AL_TYPES_MAX_EID_LENGTH]; //256
} al_types_endpoint_id;

/**
 * \brief The basic handle for communication with the bp router.
 */
typedef int * al_types_handle;

#define BP_INFINITE_WAIT  -1 // (uint64_t) -1 === (2^64)-1 
#define BP_NO_WAIT         0
/**
 * \brief Timeout in milliseconds.
 *
 * timeout = BP_INFINITE_WAIT: (uint64_t) -1  ==> infinite blocking
 * timeout = BP_NO_WAIT:                   0  ==> passing/non-blocking call, useful for polling
 * timeout =                       [1, 2^64]  ==> timeout in milliseconds
 */
typedef uint64_t al_types_timeout;

/**
 * \brief BP times are in ms in RFC9171. Thus timeval cannot be defined shorter than 8B.
 */

typedef uint64_t al_types_timeval;

/**
 * \brief BP (RFC9171 require two 8B integers) creation_timestamp.
 */

typedef uint64_t al_types_timeval_ms;

/**
 * \brief BP (RFC9171 require two 8B integers) creation_timestamp.
 */

typedef uint64_t al_types_timeval_s;

/**
 * \brief BP (RFC9171 require two 8B integers) creation_timestamp.
 */


typedef struct al_types_creation_timestamp {
	uint64_t time;
	uint64_t seqno;//from 32 to 64
} al_types_creation_timestamp;//al_types_creation_timestamp

typedef uint32_t al_types_reg_token;

/**
 * \brief A registration cookie.
 */

typedef uint32_t al_types_reg_id;

/**
 * \brief Registration state.
 */

typedef struct al_types_reg_info {
	al_types_endpoint_id endpoint;
	al_types_reg_id regid;
	uint32_t flags;
	uint32_t replay_flags;
	al_types_timeval expiration;
	boolean_t init_passive;
	al_types_reg_token reg_token;
	struct {
		uint32_t script_len;
		char *script_val;
	} script;
} al_types_reg_info;

/**
 * \brief Registration flags
 */

typedef enum al_types_reg_flags {
	/**
	 * \brief Drop bundle if registration not active
	 */
	BP_REG_DROP = 1,
	/**
	 * \brief Spool bundle for later retrieval
	 */
	BP_REG_DEFER = 2,
	/**
	 * \brief Exec program on bundle arrival
	 */
	BP_REG_EXEC = 3,
	/**
	 * \brief App assumes custody for the session
	 */
	BP_SESSION_CUSTODY = 4,
	/**
	 * \brief Creates a publication point
	 */
	BP_SESSION_PUBLISH = 8,
	/**
	 * \brief Creates subscription for the session
	 */
	BP_SESSION_SUBSCRIBE = 16,
	/**
	 * \brief application will acknowledge delivered bundles with bp_ack()
	 *
	 */
	BP_DELIVERY_ACKS = 32,
} al_types_reg_flags;

/**
 * \brief Value for an unspecified registration cookie (i.e. indication that
 * the daemon should allocate a new unique id).
 */

#define BP_REGID_NONE 0

/**
 * \brief Bundle processing control flags(all BPv6 and BPV7; the cardinal priority is treated apart as in BPV7 )
 */

typedef enum al_types_bundle_processing_control_flags {

	/**
	 * \brief set the do not fragment bit
	 */
	BP_DOPTS_DO_NOT_FRAGMENT = 256, //2 bit
	/**
	 * \brief custody xfer
	 */
	BP_DOPTS_CUSTODY = 1, //3 bit (BPV6)
	/**
	 * \brief destination is a singleton
	 */
	BP_DOPTS_SINGLETON_DEST = 64, //4 bit (BPV6)
	/**
	 * \brief ack requested by the application
	 * \detail only in IBR
	*/
	BP_DOPTS_ACK_REQUESTED_BY_APPLICATION = 512, //5 bit (BPV6)
	/**
	 * \brief status in time
	 * \detail In RFC but not present in any implementations
	*/
	BP_DOPTS_STATUS_TIME_IN_BSR = 1024, //6 bit (BPV7)
	/**
		* \brief per hop arrival receipt
	*/
	BP_DOPTS_RECEPTION_BSR = 4, //14 bit
	/**
	 * \brief per custodian receipt
	 */
	BP_DOPTS_CUSTODY_BSR = 16, //15 bit
	/**
	 * \brief per hop departure receipt
	 */
	BP_DOPTS_FORWARD_BSR = 8, //16 bit
	/**
	 * \brief end to end delivery (i.e. return receipt)
	 */
	BP_DOPTS_DELIVERY_BSR = 2, //17 bit
	/**
	 * \brief request deletion receipt
	 */
	BP_DOPTS_DELETION_BSR = 32, //18 bit
	/**
	 * \brief no custody, etc
	 */
	BP_DOPTS_NONE = 0,
	/**
	 * \brief destination is not a singleton
	 */
	BP_DOPTS_MULTINODE_DEST = 128,
} al_types_bundle_processing_control_flags; //bundle processing control flags

/**
 * \brief ecos bundle structure
 */

typedef struct {
	boolean_t ecos_enabled;
	uint8_t ordinal;
	boolean_t unreliable;
	boolean_t reliable;
	boolean_t critical;
	uint32_t flow_label;
} al_types_bundle_ecos_t;


/**
 * \brief Bundle cardinal specifier.
 * Only for ION implementation there is ordinal number
 * 	   ordinal [0 - AL_MAX_ORDINAL_NBR]
 */

typedef enum al_types_bundle_priority_enum {
	/**
	 * \brief lowest cardinal
	 */
	BP_PRIORITY_BULK = 0,
	/**
	 * \brief regular cardinal
	 */
	BP_PRIORITY_NORMAL = 1,
	/**
	 * \brief important
	 */
	BP_PRIORITY_EXPEDITED = 2,
	/**
	 * \brief TBD
	 */
	BP_PRIORITY_RESERVED = 3,
} al_types_bundle_priority_enum;


/**
 * Metadata type code numbers used in Metadata Blocks - see RFC 6258
 */
typedef enum {
	METADATA_TYPE_URI		        = 0x01,  ///< Metadata block carries URI
	METADATA_TYPE_EXPT_MIN			= 0xc0,  ///< Low end of experimental range
	METADATA_TYPE_EXPT_MAX			= 0xff,  ///< High end of experimental range
} al_types_metadata_type_code;

/**
 * Valid type codes for bundle blocks as in RFC9171
 */
typedef enum {
    PRIMARY_BLOCK               		= 0x000, ///< INTERNAL ONLY -- NOT IN SPEC
    BUNDLE_PAYLOAD_BLOCK                = 0x001, ///< Defined in RFC9171
    BUNDLE_AUTHENTICATION_BLOCK 		= 0x002, ///< Defined in RFC9171
    PAYLOAD_INTEGRITY_BLOCK      		= 0x003, ///< Defined in RFC9171
    PAYLOAD_CONFIDENTIALITY_BLOCK       = 0x004, ///< Defined in RFC9171
    PREVIOUS_HOP_INSERTION_BLOCK        = 0x005, ///< Defined in RFC9171
	PREVIOUS_NODE      					= 0x006, ///< Defined in RFC9171
	BUNDLE_AGE       				    = 0x007, ///< Defined in RFC9171
    METADATA_EXTENSION_BLOCK            = 0x008, ///< Defined in RFC9171
    EXTENSION_SECURITY_BLOCK    		= 0x009, ///< Defined in RFC9171
	HOP_COUNT                   		= 0x00a, ///< Defined in RFC9171
} al_types_bundle_block_type;

/**
 * Position of block processing control flags as in RFC9171.
 * The values are in hexadecimal format (e.g. 0x40 is 100 0000 so we have the bit 1 in position 6 counting from 0)
 */
typedef enum {
	// Block must be replicated in every fragment
    BLOCK_MUST_BE_REPLICATED_IN_FRAGMENTS   	= 0x001, /// bit position 0 Defined in RFC505050 and RFC9171
	//Transmit status report if block can't be processed
    TRANSMIT_STATUS_REPORT_IF_NOT_PROCESSED     = 0x002, /// bit position 1 Defined in RFC505050 and RFC9171
	//Delete bundle if block can't be processed
    DELETE_BUNDLE_IF_NOT_PROCESSED 				= 0x004, /// bit position 2 Defined in RFC505050 and RFC9171
	//Last block
    LAST_BLOCK      							= 0x008, /// bit position 3 Defined in RFC505050 and RFC9171
	//Discard block if it can't be processed
    DISCARD_BLOCK_IF_NOT_PROCESSED      	    = 0x010, /// bit position 4 Defined in RFC505050 and RFC9171
	//Block was forwarded without being processed
    BLOCK_FORWARDED_WITHOUT_PROCESSED           = 0x020, /// bit position 5 Defined in RFC505050 and RFC9171
	//Block contains an EID-reference field
	BLOCK_CONTAINS_EID_FIELD      			    = 0x040, /// bit position 6 Defined in RFC505050 and RFC9171
} al_types_block_control_flags;

/**
 * \brief Extension block as RFC 9171
 */

typedef struct al_types_extension_block {
	uint8_t block_type_code;//RFC 5050 and RFC9171 require 1B
	uint64_t block_processing_control_flags;//RFC9171 requires 8B
	uint8_t crc_type; //only bpv7; the CRC field is not present as its management is in charge of BPA only
	struct {
		uint64_t block_type_specific_data_len;
		char *block_type_specific_data;
	} block_data;
} al_types_extension_block;

//TODO: chiedere a bisacchi
/**
 * \brief Bundle specifications in the order indicated by BPV7 RFC. The delivery_regid is ours and is ignored when sending
 * bundles, but is filled in by the daemon with the registration
 * id where the bundle was received.
 */

typedef struct al_types_bundle_spec {
	al_types_reg_id delivery_regid;
	uint8_t bp_version;//RFC 5050 and RFC9171 require 1B
	al_types_bundle_processing_control_flags bundle_proc_ctrl_flags;
	al_types_bundle_priority_enum cardinal;
	al_types_endpoint_id destination;
	al_types_endpoint_id source;
	al_types_endpoint_id report_to;
	al_types_creation_timestamp creation_ts;
//	al_types_timeval_ms lifetime;//Persampieri and I think that in Unified_API it should be given always in ms.
	al_types_timeval lifetime;//To be replaced by the ms version
	al_types_bundle_ecos_t ecos;
	struct {
			uint32_t extension_number;//Number of extension (metadata) blocks
			al_types_extension_block *extension_blocks;//one (CCaini or more?) extension blocks
		} extensions;
	unsigned char irf_trace; //In order to work with ION inter-regional routing
} al_types_bundle_spec;

/**
 * \brief payload location.
 * The payload of a bundle can be sent or received either in a file,
 * in which case the payload structure contains the filename, or in
 * memory where the struct contains the block_data in-band, in the 'buf'
 * field.
 *
 * When sending a bundle, if the location specifies that the payload
 * is in a temp file, then the daemon assumes ownership of the file
 * and should have sufficient permissions to move or rename it.
 *
 * When receiving a bundle that is a status report, then the
 * status_report pointer will be non-NULL and will point to a
 * al_types_bundle_status_report structure which contains the parsed fields
 * of the status report.
 */

typedef enum al_types_bundle_payload_location {
	/**
	 * \brief payload contents in memory
	 */
	BP_PAYLOAD_FILE = 0,
	/**
	 * \brief payload contents in file
	 */
	BP_PAYLOAD_MEM = 1,
	/**
	 * \brief in file, assume ownership (send only)
	 */
	BP_PAYLOAD_TEMP_FILE = 2,
} al_types_bundle_payload_location;

/**
 * \brief  Bundle Status Report "Reason Code"
 */

typedef enum al_types_status_report_reason {
	BP_SR_REASON_NO_ADDTL_INFO = 0x00,
	BP_SR_REASON_LIFETIME_EXPIRED = 0x01,
	BP_SR_REASON_FORWARDED_UNIDIR_LINK = 0x02,
	BP_SR_REASON_TRANSMISSION_CANCELLED = 0x03,
	BP_SR_REASON_DEPLETED_STORAGE = 0x04,
	BP_SR_REASON_ENDPOINT_ID_UNINTELLIGIBLE = 0x05,
	BP_SR_REASON_NO_ROUTE_TO_DEST = 0x06,
	BP_SR_REASON_NO_TIMELY_CONTACT = 0x07,
	BP_SR_REASON_BLOCK_UNINTELLIGIBLE = 0x08,
} al_types_status_report_reason;

/**
 * \brief Position of bundle Status Report status report flags that indicate which timestamps in
 * the status report structure are valid.
 */

typedef enum al_types_status_report_flags {
	BP_STATUS_RECEIVED = 0x01,
	BP_STATUS_CUSTODY_ACCEPTED = 0x02,
	BP_STATUS_FORWARDED = 0x04,
	BP_STATUS_DELIVERED = 0x08,
	BP_STATUS_DELETED = 0x10,
} al_types_status_report_flags;

/**
 * \brief  Type definition for a unique bundle identifier.
 * Returned from bp_send after the daemon has assigned the creation_secs and creation_subsecs,
 * in which case frag_length and frag_offset are always zero, and also in
 * status report block_data in which case they may be set if the bundle is
 * fragmented.
 */

typedef struct al_types_bundle_id {
	al_types_endpoint_id source;
	al_types_creation_timestamp creation_ts; //consisting of time and seqno
	uint32_t frag_offset;
	uint32_t frag_length;
} al_types_bundle_id;

/**
 * \brief Type definition for a bundle status report.
 */

typedef struct al_types_bundle_status_report {
	al_types_bundle_id bundle_x_id;
	al_types_status_report_reason reason;
	al_types_status_report_flags flags;
	uint64_t reception_ts;
	uint64_t custody_ts;
	uint64_t forwarding_ts;
	uint64_t delivery_ts;
	uint64_t deletion_ts;
	uint64_t ack_by_app_ts;
} al_types_bundle_status_report;

/**
 * \brief Type definition for a bundle payload
 */
typedef struct al_types_bundle_payload { //al_bundle_payload_t
	al_types_bundle_payload_location location;
	struct {
		uint32_t filename_len;
		char *filename_val;
	} filename;
	struct {
		uint32_t buf_crc;
		uint32_t buf_len;
		char *buf_val;
	} buf;
	al_types_bundle_status_report *status_report;
} al_types_bundle_payload;



/****************************************************************
 *
 *             HIGHER LEVEL TYPES
 *
 ****************************************************************/

/**
 * \brief bundle_object
 */

typedef struct al_types_bundle_object { //al_types_bundle_object
	al_types_bundle_spec * spec;
	al_types_bundle_payload * payload;
} al_types_bundle_object;

#endif /* AL_TYPES_H_ */
