/** \file al_utilities_bundle_print.c
 *
 *  \brief  This file contains the implementation of debug utility functions.
 *
 *  \details Used to print the content of variables of Unified API types.
 *
  *
 *  \par Copyright
 *  	Copyright (c) 2016, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Davide Pallotti, davide.pallotti@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/16 | D. Pallotti	   |  Implementation contribution.
 *
 *
 */
 
#include "al_utilities_bundle_print.h"

const char* al_utilities_bundle_print_tabs(int tabs) {
	switch (tabs) {
		case 0: return "";
		case 1: return "    ";
		case 2: return "        ";
		case 3: return "            ";
		case 4: return "                ";
		case 5: return "                    ";
		case 6: return "                        ";
		case 7: return "                            ";
		case 8: return "                                ";
		case 9: return "                                    ";
		default: return "                                        ";
	}
}

void al_utilities_bundle_print_null(const char* Tname, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %s\n", al_utilities_bundle_print_tabs(indent), Tname, name, "(null)");
}

// template<typename T>
// void al_utilities_bundle_print_array(const T* array, size_t size, void (*al_bp_print_T)(T, const char*, size_t, FILE*), const char* Tname, 
                       // const char* name, size_t indent, FILE* stream) {
	// if (size > 0) {
		// fprintf(stream, "%s%s %s[%zu]:\n", al_utilities_bundle_print_tabs(indent), Tname, name, size);
		// for (size_t i = 0; i < size; ++i) {
			// char name[8]; sprintf(name, "[%zu]", i);
			// al_bp_print_T(array[i], name, indent+1, stream);
		// }
	// } else fprintf(stream, "%s%s %s[%zu]: (empty)\n", al_utilities_bundle_print_tabs(indent), Tname, name, size);
// }

void al_utilities_bundle_print_boolean(int boolean, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %s\n", al_utilities_bundle_print_tabs(indent), "boolean_t", name, boolean ? "TRUE" : "FALSE");
}

void al_utilities_bundle_print_char(unsigned char unsigned_char, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %c\n", al_utilities_bundle_print_tabs(indent), "unsigned char", name, unsigned_char);
}

void al_utilities_bundle_print_u32(uint32_t u32, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %u\n", al_utilities_bundle_print_tabs(indent), "uint32_t", name, u32);
}

void al_utilities_bundle_print_u64(uint64_t u64, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %lu\n", al_utilities_bundle_print_tabs(indent), "uint64_t", name, u64);
}

void al_utilities_bundle_print_u32_hex(uint32_t u32, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %#X\n", al_utilities_bundle_print_tabs(indent), "uint32_t", name, u32);
}

void al_utilities_bundle_print_str(const char* str, const char* name, size_t indent, FILE* stream) {
	if (str != NULL) fprintf(stream, "%s%s %s: \"%s\"\n", al_utilities_bundle_print_tabs(indent), "char*", name, str);
	else fprintf(stream, "%s%s %s: NULL\n", al_utilities_bundle_print_tabs(indent), "char*", name);
}

void al_utilities_bundle_print_str_len(const char* str, size_t len, const char* name, size_t indent, FILE* stream) {
	if (str != NULL) fprintf(stream, "%s%s %s: \"%.*s\"\n", al_utilities_bundle_print_tabs(indent), "char*", name, (int)len, str);
	else fprintf(stream, "%s%s %s: NULL\n", al_utilities_bundle_print_tabs(indent), "char*", name);	
}

void al_utilities_bundle_print_endpoint_id(al_types_endpoint_id endpoint_id, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent), "al_types_endpoint_id", name);
	al_utilities_bundle_print_str(endpoint_id.uri, "uri", indent+1, stream);
}

void al_utilities_bundle_print_timeval(al_types_timeval timeval, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %lu\n", al_utilities_bundle_print_tabs(indent), "al_types_timeval", name, timeval);
}

void al_utilities_bundle_print_timestamp(al_types_creation_timestamp timestamp, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent), "al_types_creation_timestamp", name);
	al_utilities_bundle_print_u64(timestamp.time, "time", indent+1, stream);
	al_utilities_bundle_print_u32(timestamp.seqno, "seqno", indent+1, stream);
}

void al_utilities_bundle_print_reg_token(al_types_reg_token reg_token, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %u\n", al_utilities_bundle_print_tabs(indent), "al_types_reg_token", name, reg_token);
}

void al_utilities_bundle_print_reg_id(al_types_reg_id reg_id, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %u\n", al_utilities_bundle_print_tabs(indent), "al_types_reg_id", name, reg_id);
}

void al_utilities_bundle_print_reg_info(al_types_reg_info reg_id, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent), "al_types_reg_info", name);
	al_utilities_bundle_print_endpoint_id(reg_id.endpoint, "endpoint", indent+1, stream);
	al_utilities_bundle_print_reg_id(reg_id.regid, "regid", indent+1, stream);
	al_utilities_bundle_print_u32_hex(reg_id.flags, "flags", indent+1, stream);
	al_utilities_bundle_print_u32_hex(reg_id.replay_flags, "reply_flags", indent+1, stream);
	al_utilities_bundle_print_timeval(reg_id.expiration, "expiration", indent+1, stream);
	al_utilities_bundle_print_boolean(reg_id.init_passive, "init_passive", indent+1, stream);
	al_utilities_bundle_print_reg_token(reg_id.reg_token, "reg_token", indent+1, stream);
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent+1), "struct", "script");
	al_utilities_bundle_print_u32(reg_id.script.script_len, "script_len", indent+2, stream);
	al_utilities_bundle_print_str(reg_id.script.script_val, "script_val", indent+2, stream);
}

void al_utilities_bundle_print_reg_flags(al_types_reg_flags reg_flags, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %#X\n", al_utilities_bundle_print_tabs(indent), "al_types_reg_flags", name, (uint32_t) reg_flags);
}

void al_utilities_bundle_print_bundle_delivery_opts(al_types_bundle_processing_control_flags bundle_delivery_opts, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %#X\n", al_utilities_bundle_print_tabs(indent), "al_types_bundle_processing_control_flags", name, (uint32_t) bundle_delivery_opts);
}

void al_utilities_bundle_print_bundle_priority_cardinal(al_types_bundle_priority_enum bundle_priority_enum, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %u\n", al_utilities_bundle_print_tabs(indent), "cardinal_priority", name, (uint32_t) bundle_priority_enum);
}

void al_utilities_bundle_print_bundle_priority_ordinal(uint32_t ordinal_priority, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %u\n", al_utilities_bundle_print_tabs(indent), "ordinal_priority", name, ordinal_priority);
}

void al_utilities_bundle_print_extension_block(al_types_extension_block extension_block, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent), "al_types_bundle_priority", name);
	al_utilities_bundle_print_u32(extension_block.block_type_code, "type", indent+1, stream);
	al_utilities_bundle_print_u32_hex(extension_block.block_processing_control_flags, "flags", indent+1, stream);
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent+1), "struct", "data");
	al_utilities_bundle_print_u32(extension_block.block_data.block_type_specific_data_len, "block_type_specific_data_len", indent+2, stream);
	al_utilities_bundle_print_str_len(extension_block.block_data.block_type_specific_data, extension_block.block_data.block_type_specific_data_len, "block_type_specific_data", indent+2, stream);
}


//the fields are printed in a logical order overriding the original structure order, for reader convenience
void al_utilities_bundle_print_bundle_spec(al_types_bundle_spec bundle_spec, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent), "al_types_bundle_spec", name);
	al_utilities_bundle_print_reg_id(bundle_spec.delivery_regid, "delivery_regid", indent+1, stream);
	al_utilities_bundle_print_u32(bundle_spec.bp_version, "bp_version", indent+1, stream);
	al_utilities_bundle_print_endpoint_id(bundle_spec.source, "source", indent+1, stream);
	al_utilities_bundle_print_endpoint_id(bundle_spec.destination, "destination", indent+1, stream);
	al_utilities_bundle_print_endpoint_id(bundle_spec.report_to, "report_to", indent+1, stream);
	al_utilities_bundle_print_bundle_delivery_opts(bundle_spec.bundle_proc_ctrl_flags, "bundle_proc_ctrl_flags", indent+1, stream);
	al_utilities_bundle_print_bundle_priority_cardinal(bundle_spec.cardinal, "cardinal", indent+1, stream);
	al_utilities_bundle_print_timestamp(bundle_spec.creation_ts, "creation_ts", indent+1, stream);
	al_utilities_bundle_print_timeval(bundle_spec.lifetime, "lifetime", indent+1, stream);
	//ecos
	al_utilities_bundle_print_boolean(bundle_spec.ecos.ecos_enabled,"ecos_enabled", indent+1, stream);
	al_utilities_bundle_print_bundle_priority_ordinal(bundle_spec.ecos.ordinal, "ordinal", indent+1, stream);
	al_utilities_bundle_print_boolean(bundle_spec.ecos.unreliable, "unreliable", indent+1, stream);
	al_utilities_bundle_print_boolean(bundle_spec.ecos.critical, "critical", indent+1, stream);
	al_utilities_bundle_print_u32(bundle_spec.ecos.flow_label, "flow_label", indent+1, stream);
	//extensions
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent+1), "struct", "blocks");
	al_utilities_bundle_print_u32(bundle_spec.extensions.extension_number, "extensions blocks_len", indent+2, stream);
	al_utilities_bundle_print_array(bundle_spec.extensions.extension_blocks, bundle_spec.extensions.extension_number, al_utilities_bundle_print_extension_block, "al_bundle_priority", "blocks_val", indent+2, stream);
	//irf
	//al_utilities_bundle_print_char(bundle_spec.irf_trace, "irf_trace", indent +1,  stream);
}

void al_utilities_bundle_print_bundle_payload_location(al_types_bundle_payload_location bundle_payload_location, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %u\n", al_utilities_bundle_print_tabs(indent), "al_types_bundle_payload_location", name, (uint32_t) bundle_payload_location);
}

void al_utilities_bundle_print_status_report_reason(al_types_status_report_reason status_report_reason, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %u\n", al_utilities_bundle_print_tabs(indent), "al_types_status_report_reason", name, (uint32_t) status_report_reason);
}

void al_utilities_bundle_print_status_report_flags(al_types_status_report_flags status_report_flags, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s: %#X\n", al_utilities_bundle_print_tabs(indent), "al_types_status_report_flags", name, (uint32_t) status_report_flags);
}

void al_utilities_bundle_print_bundle_id(al_types_bundle_id bundle_id, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent), "al_types_bundle_id", name);
	al_utilities_bundle_print_endpoint_id(bundle_id.source, "source", indent+1, stream);
	al_utilities_bundle_print_timestamp(bundle_id.creation_ts, "creation_ts", indent+1, stream);
	al_utilities_bundle_print_u32(bundle_id.frag_offset, "frag_offset", indent+1, stream);
	al_utilities_bundle_print_u32(bundle_id.frag_length, "orig_length", indent+1, stream);
}

void al_utilities_bundle_print_bundle_status_report(al_types_bundle_status_report bundle_status_report, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent), "al_types_bundle_status_report", name);
	al_utilities_bundle_print_bundle_id(bundle_status_report.bundle_x_id, "bundle_id", indent+1, stream);
	al_utilities_bundle_print_status_report_reason(bundle_status_report.reason, "reason", indent+1, stream);
	al_utilities_bundle_print_status_report_flags(bundle_status_report.flags, "flags", indent+1, stream);
    al_utilities_bundle_print_u32(bundle_status_report.reception_ts, "reception_ts", indent+1, stream);
    al_utilities_bundle_print_u32(bundle_status_report.custody_ts, "custody_ts", indent+1, stream);
    al_utilities_bundle_print_u32(bundle_status_report.forwarding_ts, "forwarding_ts", indent+1, stream);
    al_utilities_bundle_print_u32(bundle_status_report.delivery_ts, "delivery_ts", indent+1, stream);
    al_utilities_bundle_print_u32(bundle_status_report.deletion_ts, "deletion_ts", indent+1, stream);
	al_utilities_bundle_print_u32(bundle_status_report.ack_by_app_ts, "ack_by_app_ts", indent+1, stream);
}

void al_utilities_bundle_print_bundle_payload(al_types_bundle_payload bundle_payload, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent), "al_types_bundle_payload", name);
	al_utilities_bundle_print_bundle_payload_location(bundle_payload.location, "location", indent+1, stream);
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent+1), "struct", "filename");
	al_utilities_bundle_print_u32(bundle_payload.filename.filename_len, "filename_len", indent+2, stream);
	al_utilities_bundle_print_str(bundle_payload.filename.filename_val, "filename_val", indent+2, stream);
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent+1), "struct", "buf");
	al_utilities_bundle_print_u32(bundle_payload.buf.buf_crc, "buf_crc", indent+2, stream);
	al_utilities_bundle_print_u32(bundle_payload.buf.buf_len, "buf_len", indent+2, stream);
	al_utilities_bundle_print_str_len(bundle_payload.buf.buf_val, bundle_payload.buf.buf_len, "buf_val", indent+2, stream);
	if (bundle_payload.status_report != NULL)
		al_utilities_bundle_print_bundle_status_report(*bundle_payload.status_report, "*status_report", indent+1, stream);
	else al_utilities_bundle_print_null("al_types_bundle_status_report", "status_report", indent+1, stream);
}

void al_utilities_bundle_print_bundle_object(al_types_bundle_object bundle_object, const char* name, size_t indent, FILE* stream) {
	fprintf(stream, "%s%s %s:\n", al_utilities_bundle_print_tabs(indent), "al_types_bundle_object", name);
	al_utilities_bundle_print_bundle_spec(*bundle_object.spec, "*spec", indent+1, stream);
	al_utilities_bundle_print_bundle_payload(*bundle_object.payload, "*payload", indent+1, stream);
}
