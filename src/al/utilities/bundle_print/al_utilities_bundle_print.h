/** \file al_utilities_bundle_print.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *  \details Used to print the content of variables of Abstraction Layer types.
 *
 *
 *  \par Copyright
 *  	Copyright (c) 2016, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Davide Pallotti, davide.pallotti@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/16 | D. Pallotti	   |  Implementation contribution.
 *
 *
 */

#ifndef AL_UTILITIES_PRINT_H_
#define AL_UTILITIES_PRINT_H_

#include <stdio.h>
#include <stdint.h>

#include "../../types/al_types.h"

const char* al_utilities_bundle_print_tabs(int tabs);

void al_utilities_bundle_print_null(const char* Tname, const char* name, size_t indent, FILE* stream);

// template<typename T>
// void al_utilities_bundle_print_array(const T* array, size_t size, void (*al_bp_print_T)(T, const char*, size_t, FILE*), const char* Tname,
                       // const char* name, size_t indent, FILE* stream);
				  
#define al_utilities_bundle_print_array(array, size, al_bp_print_T, Tname, name, indent, stream) \
	if ((size) > 0) { \
		fprintf(stream, "%s%s %s[%zu]:\n", al_utilities_bundle_print_tabs(indent), Tname, name, (size_t) (size)); \
		size_t i; \
		for (i = 0; i < (size); ++i) { \
			char index[23]; sprintf(index, "[%zu]", i); \
			al_bp_print_T((array)[i], index, (indent)+1, stream); \
		} \
	} else fprintf(stream, "%s%s %s[%zu]: (empty)\n", al_utilities_bundle_print_tabs(indent), Tname, name, (size_t) (size));

void al_utilities_bundle_print_boolean(int boolean, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_char(unsigned char unsigned_char, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_u64(uint64_t u64, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_u32(uint32_t u32, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_u32_hex(uint32_t u32, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_str(const char* str, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_str_len(const char* str, size_t len, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_endpoint_id(al_types_endpoint_id endpoint_id, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_timeval(al_types_timeval timeval, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_timestamp(al_types_creation_timestamp timestamp, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_reg_token(al_types_reg_token reg_token, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_reg_id(al_types_reg_id reg_id, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_reg_info(al_types_reg_info reg_id, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_reg_flags(al_types_reg_flags reg_flags, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_bundle_delivery_opts(al_types_bundle_processing_control_flags bundle_delivery_opts, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_bundle_priority_cardinal(al_types_bundle_priority_enum cardinal_priority, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_bundle_priority_ordinal(uint32_t ordinal_priority, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_extension_block(al_types_extension_block extension_block, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_bundle_spec(al_types_bundle_spec bundle_spec, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_bundle_payload_location(al_types_bundle_payload_location bundle_payload_location, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_status_report_reason(al_types_status_report_reason status_report_reason, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_status_report_flags(al_types_status_report_flags status_report_flags, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_bundle_id(al_types_bundle_id bundle_id, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_bundle_status_report(al_types_bundle_status_report bundle_status_report, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_bundle_payload(al_types_bundle_payload bundle_payload, const char* name, size_t indent, FILE* stream);

void al_utilities_bundle_print_bundle_object(al_types_bundle_object bundle_object, const char* name, size_t indent, FILE* stream);


#endif /* AL_UTILITIES_PRINT_H_ */
