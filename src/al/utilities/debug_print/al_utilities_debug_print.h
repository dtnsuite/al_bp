/** \file al_utilities_debug_print.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * \authors Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 * \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 * \authors Silvia Lanzoni, silvia.lanzoni5@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/20 | M. Bertolazzi   |  Initial Implementation.
 *  01/01/20 | A. Bisacchi	   |  Implementation refactoring.
 *  15/01/22 | S. Lanzoni      |  Add documentation.
 *
 */


#ifndef AL_UTILITIES_DEBUG_PRINT_H
#define AL_UTILITIES_DEBUG_PRINT_H
#include <stdio.h>
#include <stdarg.h>

#include "../../../unified_api_boolean_type.h"

typedef enum debug_level {DEBUG_L0=0, DEBUG_L1, DEBUG_L2} threshold_level;


int al_utilities_debug_print_debugger_init(int debug, boolean_t create_log, char *log_filename);
int al_utilities_debug_print_debugger_destroy();

boolean_t al_utilities_debug_print_check_level(threshold_level required_level);

void al_utilities_debug_print(threshold_level level, const char *string, ...);

void al_utilities_debug_print_error(const char* string, ...);

FILE* al_utilities_debug_print_get_log_fp();

//void verbose_print(const char *string, ...);

#endif /* AL_UTILITIES_DEBUG_PRINT_H */
