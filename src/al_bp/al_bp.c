/** \file al_bp.c
 *
 *  \brief  This file contains all the APIs of the al_bp.
 *
 *  \details Each API consists of a switch between DTN2,ION and IBR-DTN API implementations.
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Davide Pallotti, davide.pallotti@studio.unibo.it
 *  \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *  \authors Silvia Lanzoni, silvia.lanzoni5@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *  \par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *  01/01/16 | D. Pallotti	   |  Implementation contribution.
 *  01/01/20 | A. Bisacchi	   |  Implementation contribution.
 *  01/10/21 | S. Lanzoni      |  Implementation refactoring and reorganization.
 *  01/10/23 | L. Fantini      |  Table for compatibility checks
 *
 */

#include "unified_api_system_libraries.h"
/*
 * CCaini 31 dic 2023
 * The use of "bp_implementation" is inconsistent: it is sometimes used as a global
 * variable, sometimes is passed as a parameter. Note that the al_socket_as
 * also bp_implementation as global variable, and al_bundle_options calls
 * al_bp_get_implementation (after the al_socket_init) because it now needs
 * the implementation to pass it to lower layer functions.
 */


#include "al_bp.h"

#include "bp_dtn.h"
#include "bp_ibr.h"
#include "bp_ion.h"
#include "bp_ud3tn.h"
#include "bp_unibo_bp.h"
// It's declared static in order to prevent bp_get_implementation from executing after the first call (should it be called multiple times,
//which however it should not happen in the ltates Unified API versions)

static al_types_implementation bp_implementation = BP_NONE;
const char* unified_api_version = UNIFIED_API_VERSION_STRING;

//TODO: dtn_none seems pleonastic
#define DTNNONE_STRING "dtn:none"
const char* dtn_none = DTNNONE_STRING;

#define MAX_TIMEOUT_FOR_HALF_DUPLEX_COMM (1 * 1000) // 1 sec It is the max interval betwen two subsequent calls
// in DTNME and UD3tn; this to give the sending thread a chance to tranmit after this interval

/* This the only API of this file whose name has not the prefix al_bp*/
const char * get_unified_api_version()
{
	return unified_api_version;
}

#define OPTION_TABLE_ROWS 23
#define OPTION_TABLE_COLUMNN IMPLEMENTATION_COUNTER
const static boolean_t OPTIONS_TABLE [OPTION_TABLE_ROWS][OPTION_TABLE_COLUMNN] = {
/*                            DTNME    ION      IBR      uD3TN    UNIBOBP */
/*   BUNDLE OPTIONS    */
/* bp_version          */ {   TRUE,    FALSE,   FALSE,   FALSE,    FALSE   },
/* lifetime            */ {   TRUE,    TRUE,    TRUE,    TRUE,     TRUE    },
/* priority            */ {   TRUE,    TRUE,    TRUE,    TRUE,     TRUE    },
/* do-not-fragment     */ {   TRUE,    TRUE,    TRUE,    FALSE,    TRUE    },
/* custody             */ {   TRUE,    TRUE,    TRUE,    FALSE,    TRUE    },
/* bsr-rcv             */ {   TRUE,    TRUE,    TRUE,    FALSE,    TRUE    },
/* bsr-cst             */ {   TRUE,    TRUE,    TRUE,    FALSE,    TRUE    },
/* bsr-fwd             */ {   TRUE,    TRUE,    TRUE,    FALSE,    TRUE    },
/* bsr-dlv             */ {   TRUE,    TRUE,    TRUE,    FALSE,    TRUE    },
/* bsr-del             */ {   TRUE,    TRUE,    TRUE,    FALSE,    TRUE    },
/* ack-req-by-app      */ {   FALSE,   FALSE,   TRUE,    FALSE,    TRUE    },
/* ordinal             */ {   TRUE,    TRUE,    FALSE,   FALSE,    TRUE    },
/* unreliable          */ {   TRUE,    TRUE,    FALSE,   FALSE,    TRUE    },
/* reliable            */ {   TRUE,    FALSE,   FALSE,   FALSE,    TRUE    },
/* critical            */ {   TRUE,    TRUE,    FALSE,   FALSE,    TRUE    },
/* flow                */ {   TRUE,    TRUE,    FALSE,   FALSE,    TRUE    },
/* mb-type             */ {   TRUE,    TRUE,    FALSE,   FALSE,    TRUE   },
/* mb-string           */ {   TRUE,    TRUE,    FALSE,   FALSE,    TRUE   },
/* irf-trace           */ {   FALSE,   TRUE,    FALSE,   FALSE,    FALSE   },

/*   DTNSUITE OPTIONS  */
/* force-eid           */ {   TRUE,    TRUE,    TRUE,    TRUE,    TRUE    },
/* ipn-local           */ {   TRUE,    FALSE,   FALSE,   FALSE,   FALSE   },
/* open-with-ip        */ {   TRUE,    FALSE,   FALSE,   TRUE,    FALSE   },
/* debug               */ {   TRUE,    TRUE,    TRUE,    TRUE,    TRUE    }
};


// The implementation names in order matching al_types_implementation.
static char const *implementation_name[IMPLEMENTATION_COUNTER] = {
	[BP_DTNME]   = "DTN2",
	[BP_ION]     = "ION",
	[BP_IBR]     = "IBRDTN",
	[BP_UD3TN]   = "uD3TN",
	[BP_UNIBOBP] = "Unibo-BP"
};

/**
 *  \brief Return the name of the underlying implementation of bundle protocol
 */
char const * al_bp_get_implementation_name(void) {
	if(bp_implementation == BP_NONE) {
		return "none";
	} else if (bp_implementation == BP_MULTIPLE) {
		return "multiple";
	}
	return implementation_name[bp_implementation];
}

static char const * al_bp_get_implementation_name_by_impl(al_types_implementation impl_idx) {
	if(impl_idx == BP_NONE) {
		return "none";
	} else if (impl_idx == BP_MULTIPLE) {
		return "multiple";
	}
	return implementation_name[impl_idx];
}

/**
 *  \brief If the current implementation supports the option fills result with TRUE, FALSE otherwise.
 * 
 * The option_index is the index in the array of option (returned by the getopt_long).
 */
al_bp_error_t al_bp_lookup_option_compatibility(int option_index, boolean_t *result) {
	if(option_index < 0 || option_index > OPTION_TABLE_ROWS) {
		return BP_EINVAL;
	}
	if(result == NULL) {
		return BP_ENULLPNTR;
	}
	(*result) = OPTIONS_TABLE[option_index][bp_implementation];
	return BP_SUCCESS;
}

/**
 *  \brief Build a string with the implementation names that support the requested feature.
 * 
 * The string is built in the buf.
 * The option_index is the index in the array of option (given by the getopt_long).
 */
al_bp_error_t al_bp_lookup_implementation_supporting_option(int option_index, char *buf, int buf_size) {
	if(buf == NULL) {
		return BP_ENULLPNTR;
	}
	int used = 0;
	char const *impl_name;
	memset(buf, 0, buf_size);

	if(option_index < 0 || option_index > OPTION_TABLE_ROWS) {
		return BP_EINVAL;
	}
	for (int impl_index = 0; impl_index < IMPLEMENTATION_COUNTER; impl_index++)
	{
		if(OPTIONS_TABLE[option_index][impl_index]) {
			impl_name = al_bp_get_implementation_name_by_impl(impl_index);
			strncat(buf, impl_name, buf_size - used);
			used += strlen(impl_name);

			strcat(buf, ", ");
			used += strlen(", ");
		}
	}
	if(used > 2) {
		buf[used - 2] = '\0'; // remove last separator ", "
	}
	return BP_SUCCESS;
}

/**
 *  \brief Check for a single running BP implementation.
 */
al_bp_error_t al_bp_implementation_checks() {
	if(bp_implementation == BP_NONE)
		return BP_ENOBPI;

	if(bp_implementation == BP_MULTIPLE)
		return BP_EMULTIBPI;
	
	return BP_SUCCESS;
}
/**
 *  \brief Check if the Unified API was compiled compatibly with the running BP implementation.
 */
al_bp_error_t al_bp_compatibility_checks() {
	switch (bp_implementation)
	{
	case BP_DTNME:
	#ifndef DTN2_IMPLEMENTATION
		return BP_EBADBPI;
	#endif
		break;

	case BP_ION:
	#ifndef ION_IMPLEMENTATION
		return BP_EBADBPI;
	#endif
		break;

	case BP_IBR:
	#ifndef IBR_IMPLEMENTATION
		return BP_EBADBPI;
	#endif
		break;

	case BP_UD3TN:
	#ifndef UD3TN_IMPLEMENTATION
		return BP_EBADBPI;
	#endif
		break;

    case BP_UNIBOBP:
    #ifndef UNIBOBP_IMPLEMENTATION
        return BP_EBADBPI;
    #endif
		break;

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
	
	return BP_SUCCESS;
}

/**
 *  \brief Find the underlying implementation of bundle protocol
 *  It also does single implementation and compatibility checks.
 * 	It fills p_impl and return BP_SUCCESS, otherwise the correct error.
 */
al_bp_error_t al_bp_get_implementation()
{
	al_bp_error_t error;
    if (bp_implementation == BP_NONE)
    {
    	/*OS Warning: the following strings are valid only for Linux/Unix OS. */
        if (bp_ibr_is_running())
            bp_implementation = BP_IBR;

        if (bp_unibo_bp_is_running()) {
        	if (bp_implementation == BP_NONE)
        		bp_implementation = BP_UNIBOBP;
        	else
        		bp_implementation = BP_MULTIPLE;
        }

        if (bp_dtn_is_running()) {
        	if (bp_implementation == BP_NONE)
        		bp_implementation = BP_DTNME;
        	else
        		bp_implementation = BP_MULTIPLE;
        }

        if (bp_ion_is_running()) {
        	if (bp_implementation == BP_NONE)
        		bp_implementation = BP_ION;
        	else
        		bp_implementation = BP_MULTIPLE;
        }

        if (bp_ud3tn_is_running()) {
        	if (bp_implementation == BP_NONE)
        		bp_implementation = BP_UD3TN;
        	else
        		bp_implementation = BP_MULTIPLE;
        }

		if((error = al_bp_implementation_checks()) != BP_SUCCESS) {
			return error;
		}

		if((error = al_bp_compatibility_checks()) != BP_SUCCESS) {
			return error;
		}
    }
    
	return BP_SUCCESS;
}

/**
 *  \brief Fills scheme to the default URI scheme of the running bp implementantion.
 */
al_bp_error_t al_bp_get_default_scheme_running_bpi(al_types_scheme *scheme) {
	if(scheme == NULL) {
		return BP_ENULLPNTR;
	}

	switch (bp_implementation) {
		case BP_ION:
			*scheme = IPN_SCHEME;
			return BP_SUCCESS;
		case BP_DTNME:
		case BP_UD3TN:
			*scheme = DTN_SCHEME;
			return BP_SUCCESS;
		case BP_IBR:
			*scheme = DTN_SCHEME;
			return BP_SUCCESS;
        case BP_UNIBOBP:
            *scheme = IPN_SCHEME;
			return BP_SUCCESS;
		default:
			return BP_ENOBPI;
	}
}

/**
 * \brief Open a new connection to the router.
 *
 * On success, initializes the handle parameter as a new handle to the
 * daemon and returns BP_SUCCESS. On failure, sets handle to NULL and
 * returns a bp_errno error code.
 *
 * For ION this API attach the application to the bundle protocol
 */

al_bp_error_t al_bp_open(al_types_handle* handle)
{
	if (handle == NULL)
		return BP_ENULLPNTR;

	switch (bp_implementation)
	{
	case BP_DTNME:
		return bp_dtn_open(handle);

	case BP_ION:
		return bp_ion_attach();

	case BP_IBR:
		return bp_ibr_open(handle);

	case BP_UD3TN:
		return bp_ud3tn_open(handle);

    case BP_UNIBOBP:
        return bp_unibo_bp_open(handle);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/**
 * \brief Open a new connection to the router with a given IP addr and port
 *
 * On success, initializes the handle parameter as a new handle to the
 * daemon and returns BP_SUCCESS. On failure, sets handle to NULL and
 * returns a bp_errno error code.
 */

al_bp_error_t al_bp_open_with_ip(char *daemon_api_IP, int daemon_api_port, al_types_handle* handle)
{
	if (handle == NULL)
		return BP_ENULLPNTR;

	switch (bp_implementation)
	{
	case BP_DTNME:
		return bp_dtn_open_with_IP(daemon_api_IP, daemon_api_port, handle);

	case BP_ION:
		return bp_ion_open_with_IP(daemon_api_IP, daemon_api_port, handle);

	case BP_IBR:
		return bp_ibr_open_with_IP(daemon_api_IP, daemon_api_port, handle);

	case BP_UD3TN:
		return bp_ud3tn_open_with_IP(daemon_api_IP, daemon_api_port, handle);

    case BP_UNIBOBP:
        return bp_unibo_bp_open_with_IP(daemon_api_IP, daemon_api_port, handle);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/**
 * \brief Get the error associated with the given handle.
 */

al_bp_error_t al_bp_errno(al_types_handle handle)
{
	switch (bp_implementation)
	{
	case BP_DTNME:
		return bp_dtn_errno(handle);

	case BP_ION:
		return bp_ion_errno(handle);

	case BP_IBR:
		return bp_ibr_errno(handle);

	case BP_UD3TN: //by silvia lanzoni
		printf("Need ud3tn support in %s -- %s\n", __FILE__, __FUNCTION__);
		exit(-1);

    case BP_UNIBOBP:
        return bp_unibo_bp_errno(handle);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/**
 * \brief Build the local EID by passing the service_tag string to the right "colored" function
 * and the wanted eid_scheme (DTN_SCHEME or IPN_SCHEME)
 *
 * @service_tag service tag of the local eid. If type is IPN_SCHEME on a DTN2 implementation
 * 		service_tag must be in the form ipn_local_number.service_number because DTN2
 * 		(2.9 with patch)/DTNME does not know his ipn number
 */

al_bp_error_t al_bp_build_local_eid(al_types_handle handle,
		al_types_endpoint_id* local_eid,
		int ipn_node_number, int ipn_service_number,
		char *dtn_demux_string,
		al_types_scheme eid_scheme)
{
	if (local_eid == NULL || dtn_demux_string == NULL) {
		return BP_ENULLPNTR;
	}

	if(ipn_node_number < 0 || ipn_service_number < 0) {
		return BP_EINVAL;
	}

	/* It builds the service_tag containing either
	 *  the dtn demux or the ipn service number, converted into a string
	 * */
	char service_tag[AL_TYPES_MAX_EID_LENGTH];
	switch (eid_scheme)
	{
	case IPN_SCHEME:
		snprintf(service_tag, sizeof(service_tag), "%lu", (unsigned long) ipn_service_number); //write in service tag the service number
		break;
	case DTN_SCHEME:
	    strncpy(service_tag, dtn_demux_string, sizeof(service_tag)-1);//wite the demux_string in service_tag
		break;
	default:
		return BP_EINVAL;
	} 

	switch (bp_implementation)
	{
	case BP_DTNME:
		//if eid_scheme is IPN, service_tag must be in the form ipn_local_number.service_number
		//so we also pass ipn_node_number as a parameter due to the 
		//impossibility of obtaining it from the DTN2 BP agent.
		return bp_dtn_build_local_eid(handle, local_eid, ipn_node_number, service_tag, eid_scheme);

	case BP_ION:
		return bp_ion_build_local_eid(local_eid, service_tag, eid_scheme);

	case BP_IBR:
		return bp_ibr_build_local_eid(handle, local_eid, service_tag, eid_scheme);

	case BP_UD3TN:
		return bp_ud3tn_build_local_eid(handle, local_eid, service_tag, eid_scheme);

    case BP_UNIBOBP:
        return bp_unibo_bp_build_local_eid(handle, local_eid, service_tag, eid_scheme);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/**
 * \brief Create a bp registration.
 *
 * If the init_passive flag in the reginfo
 * struct is true, the registration is initially in passive state,
 * requiring a call to dtn_bind to activate it. Otherwise, the call to
 * dtn_bind is unnecessary as the registration will be created in the
 * active state and bound to the handle.
 */

al_bp_error_t al_bp_register(al_types_handle * handle,
		al_types_reg_info* reginfo,
		al_types_reg_id* newregid, 
		pthread_mutex_t *mutex_half_duplex)
{
	if (reginfo == NULL)
		return BP_ENULLPNTR;
	if (newregid == NULL)
		return BP_ENULLPNTR;

	switch (bp_implementation)
	{
	case BP_DTNME:
		pthread_mutex_init(mutex_half_duplex, NULL);
		return bp_dtn_register(*handle, reginfo, newregid);

	case BP_UD3TN:
		pthread_mutex_init(mutex_half_duplex, NULL);
		return bp_ud3tn_register(*handle, reginfo, newregid);

	case BP_ION:
		return bp_ion_register(handle, reginfo, newregid);

	case BP_IBR:
		return bp_ibr_register(*handle, reginfo, newregid);

    case BP_UNIBOBP:
        return bp_unibo_bp_register(*handle, reginfo);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/**
 * \brief Check for an existing registration on the given endpoint id.
 *
 * It returns BP_SUCCSS and fills in the registration id if it
 * exists, or returning ENOENT if it doesn't.
 */

al_bp_error_t al_bp_find_registration(al_types_handle handle,
		al_types_endpoint_id * eid,
		al_types_reg_id * newregid)
{
	if (eid == NULL)
		return BP_ENULLPNTR;

	switch (bp_implementation)
	{
	case BP_DTNME:
		return bp_dtn_find_registration(handle, eid, newregid);

	case BP_ION:
		return bp_ion_find_registration(handle, eid, newregid);

	case BP_IBR:
		return bp_ibr_find_registration(handle, eid, newregid);

	case BP_UD3TN:
			printf("Need ud3tn support in %s -- %s\n", __FILE__, __FUNCTION__);
			exit(-1);

    case BP_UNIBOBP:
        return bp_unibo_bp_find_registration(handle, eid);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/**
 * \brief Remove a registration
 */

al_bp_error_t al_bp_unregister(al_types_handle handle, 
				al_types_reg_id regid,
				al_types_endpoint_id eid,
				pthread_mutex_t *mutex_half_duplex){

	switch (bp_implementation)
	{
	case BP_DTNME:
		pthread_mutex_destroy(mutex_half_duplex);
		return bp_dtn_unregister(handle, regid);

	case BP_UD3TN:
		pthread_mutex_destroy(mutex_half_duplex);
		printf("Need ud3tn support in %s -- %s\n", __FILE__, __FUNCTION__);
		exit(-1);

	case BP_ION:
		return bp_ion_unregister(eid);

	case BP_IBR:
		return bp_ibr_unregister(handle, regid, eid);

    case BP_UNIBOBP:
        return bp_unibo_bp_unregister(handle);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/**
 * \brief Send a bundle either from memory or from a file.
 */

al_bp_error_t al_bp_send(al_types_handle handle,
		al_types_reg_id regid,
		al_types_bundle_spec* spec,
		al_types_bundle_payload* payload,
		pthread_mutex_t *mutex_half_duplex)
{
	al_types_endpoint_id none;
	if (spec == NULL)
		return BP_ENULLPNTR;
	if (payload == NULL)
		return BP_ENULLPNTR;

	/*if (spec->report_to.uri == NULL || strlen(spec->report_to.uri) == 0) {
		al_bp_get_none_endpoint(&none);
		al_bp_copy_eid(&(spec->report_to), &none);
	}*/

	if (strlen(spec->report_to.uri) == 0) {
			al_bp_get_none_endpoint(&none);
			al_bp_copy_eid(&(spec->report_to), &none);
		}

	switch (bp_implementation)
	{
	case BP_DTNME:
		{
			al_bp_error_t error;
			pthread_mutex_lock(mutex_half_duplex);
			error = bp_dtn_send(handle, regid, spec, payload);
			pthread_mutex_unlock(mutex_half_duplex);
			return error;
		}

	case BP_UD3TN: //Modified by CCaini: mutex removed as no more necessary in AAP2
		{
			al_bp_error_t error;
			error = bp_ud3tn_send(handle, regid, spec, payload);
			return error;
		}
	case BP_ION:
		return bp_ion_send(handle, regid, spec, payload);

	case BP_IBR:
		return bp_ibr_send(handle, regid, spec, payload);

    	case BP_UNIBOBP:
        return bp_unibo_bp_send(handle, spec, payload);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/**
 * \brief it receives a bundle.
 *
 * Blocking receive for a bundle, filling in the spec and payload
 * structures with the bundle data. The location parameter indicates
 * the manner by which the caller wants to receive payload data (i.e.
 * either in memory or in a file). The timeout parameter specifies an
 * interval in milliseconds to block on the server-side (-1 means
 * infinite wait)
 *
 * Note that it is advisable to call bp_free_payload on the returned
 * structure, otherwise the XDR routines will memory leak.
 */

al_bp_error_t al_bp_recv(al_types_handle handle,
		al_types_bundle_spec* spec,
		al_types_bundle_payload_location location,
		al_types_bundle_payload* payload,
		al_types_timeout timeout,
		pthread_mutex_t *mutex_half_duplex)
{
	if (spec == NULL)
		return BP_ENULLPNTR;
	if (payload == NULL)
		return BP_ENULLPNTR;

	switch (bp_implementation)
	{
	case BP_DTNME:
		{
/*
 * In DTNME it is not safe to send a bundle on an EID, while another thread is blocked on a receive on the same EID; this may impair applications that use
 * same registration to send or receive bundles concurrently, as DTNperf. To bypass the problem, we have inserted a mutex. The receive must be put in a loop where 
 * the timeout cannot be >= of a threhold; when the timeout fires the mutex is released, gioving a sending thread the opportunity to take it.
 * UD3TN AAP had the same problem, and the same solution; in AAP2 the problem has been solved, thus the current code does no more use the mutex 
 * (but it is no more compatoible with AAP)   
 */


			al_bp_error_t error = BP_ERECV;
			struct timespec now;
			boolean_t timeout_completed;
			uint64_t now_in_ms, end_in_ms;

			if(timeout <= MAX_TIMEOUT_FOR_HALF_DUPLEX_COMM) { // timeout in the range [0, MAX_TIMEOUT_FOR_HALF_DUPLEX_COMM]
			 	pthread_mutex_lock(mutex_half_duplex);
				error = bp_dtn_recv(handle, spec, location, payload, timeout);
				pthread_mutex_unlock(mutex_half_duplex);
				sched_yield();//An attempt to allow a concurrent "send" to take the mutex
				//CCaini and Bisacchi discussion
			} else { // timeout in the range (MAX_TIMEOUT_FOR_HALF_DUPLEX_COMM, 2^64-1]
				if(timeout != BP_INFINITE_WAIT) {
					// get starting time
					clock_gettime(CLOCK_REALTIME, &now);

					// convert starting time to ms
					now_in_ms = now.tv_sec * 1000 + (now.tv_nsec / (1e6)); // truncation towards zero!

					// calculate expiration time (now + timeout)
					end_in_ms = now_in_ms + timeout;

					timeout_completed = FALSE;
				}
				do {
					pthread_mutex_lock(mutex_half_duplex);

					// start receive with smaller hard-coded timeout
					error = bp_dtn_recv(handle, spec, location, payload, MAX_TIMEOUT_FOR_HALF_DUPLEX_COMM);
					pthread_mutex_unlock(mutex_half_duplex);
					sched_yield();//An attempt to allow a concurrent "send" to take the mutex
					//CCaini and Bisacchi discussion

					if(error != BP_ETIMEOUT) { // BP_SUCCESS  or  other errors
						break; // exit do while
					}

					if(timeout != BP_INFINITE_WAIT) {
						clock_gettime(CLOCK_REALTIME, &now);
						now_in_ms = now.tv_sec * 1000 + (now.tv_nsec / (1e6)); // truncation towards zero!
						timeout_completed = (now_in_ms >= end_in_ms);
						 if(!timeout_completed) {
						 	printf("[INFO] restart recv: the are still %ld ms left for completing the %ld timeout\n", end_in_ms - now_in_ms, timeout);
						 }

						// even if fewer milliseconds remain, the last timeout is still MAX_TIMEOUT.
					}

				} while ((timeout == BP_INFINITE_WAIT) || !timeout_completed); // still wait
			}
			return error;
		}

	case BP_UD3TN: //Modified by CCaini: mutex removed as no more necessary in AAP2; conversely, it is no more compatoble with AAP
		{
			al_bp_error_t error = BP_ERECV;
			error = bp_ud3tn_recv(handle, spec, location, payload, timeout);
			return error;
		}

	case BP_ION:
		return bp_ion_recv(handle, spec, location, payload, timeout);

	case BP_IBR:
		return bp_ibr_recv(handle, spec, location, payload, timeout);

    case BP_UNIBOBP:
        return bp_unibo_bp_recv(handle, spec, location, payload, timeout);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/**
 * \brief Close an open bp handle.
 *  Returns BP_SUCCESS on success.
 */

al_bp_error_t al_bp_close(al_types_handle handle)
{
	switch (bp_implementation)
	{
	case BP_DTNME:
		return bp_dtn_close(handle);

	case BP_ION:
		return bp_ion_close(handle);

	case BP_IBR:
		return bp_ibr_close(handle);

	case BP_UD3TN:
			return bp_ud3tn_close(handle);

    case BP_UNIBOBP:
        return bp_unibo_bp_close(handle);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}


/**
 *  \brief Invokes al_bp_close(), al_bp_unregister(), or both depending on the implementation.
 */
al_bp_error_t al_bp_close_and_unregister(al_types_handle handle,
						al_types_reg_id regid,
						al_types_endpoint_id eid,
						pthread_mutex_t *mutex_half_duplex) {
	// since the order of the closing operations (close() and unregister())
	// are implementation-dependent this utility function has been added
	// to be able to abstract the details.
	
	al_bp_error_t error;
	switch (bp_implementation)
	{
	case BP_DTNME:
		return al_bp_close(handle);

	case BP_ION:
		if((error = al_bp_close(handle)) != BP_SUCCESS) {
			return error;
		}
		return al_bp_unregister(handle, regid, eid, mutex_half_duplex);

	case BP_IBR:
		return al_bp_close(handle);

	case BP_UD3TN:
		return al_bp_close(handle);

    case BP_UNIBOBP:
        if((error = al_bp_unregister(handle, regid, eid, mutex_half_duplex)) != BP_SUCCESS) {
			return error;
		}
		return al_bp_close(handle);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/*************************************************************
 *
 *                     Utility Functions
 *
 *************************************************************/
//The first 3 could be replaced by "universal" functions, independent of BP implementations.
//If useful only to one AL function, they should be put into the corresponding AL file as internal functions
//If useful also to DTNsuite apps, they should be put into a new AL utility file

/**
 *\brief Copy the contents of one eid into another.
 */

void al_bp_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src)
{	
	switch (bp_implementation)
	{
	case BP_DTNME:
		bp_dtn_copy_eid(dst, src);
		break;

	case BP_ION:
		bp_ion_copy_eid(dst, src);
		break;

	case BP_IBR:
		bp_ibr_copy_eid(dst, src);
		break;

	case BP_UD3TN:
		bp_ud3tn_copy_eid(dst, src);
		break;

    case BP_UNIBOBP:
        bp_unibo_bp_copy_eid(dst, src);
        break;

	default: // cannot find bundle protocol implementation
		return ;
	}
}


/**
 * \brief Returns the null endpoint
 */

al_bp_error_t al_bp_get_none_endpoint(al_types_endpoint_id * eid_none)
{
	switch (bp_implementation)
	{
	case BP_DTNME:
		return bp_dtn_parse_eid_string(eid_none, dtn_none);

	case BP_ION:
		return bp_ion_parse_eid_string(eid_none, dtn_none);

	case BP_IBR:
		return bp_ibr_parse_eid_string(eid_none, dtn_none);

	case BP_UD3TN:
		return bp_ud3tn_parse_eid_string(eid_none, dtn_none);

    case BP_UNIBOBP:
        return bp_unibo_bp_parse_eid_string(eid_none, dtn_none);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}
}

/**
 * \brief Sets the value of the given payload structure to either a memory
 * buffer or a file location.
 *
 * Returns: 0 on success, bp_ESIZE if the memory location is
 * selected and the payload is too big.
 */

/*
 * Obsoleted by set_payload in al_bundle.c
 * al_bp_error_t al_bp_set_payload(al_types_bundle_payload* payload,
		al_types_bundle_payload_location location,
		char* val, int len)
{
	if (payload == NULL)
		return BP_ENULLPNTR;

	switch (bp_implementation)
	{
	case BP_DTN:
		return bp_dtn_set_payload(payload, location, val, len);

	case BP_ION:
		return bp_ion_set_payload(payload, location, val, len);

	case BP_IBR:
		return bp_ibr_set_payload(payload, location, val, len);

	case BP_UD3TN:
		return bp_ud3tn_set_payload(payload, location, val, len);

	default: // cannot find bundle protocol implementation
		return BP_ENOBPI;
	}

}
 */


/**
 * \brief Frees dynamic storage allocated by the xdr for a bundle extensions blocks in
 * bp_recv.
 */

void al_bp_free_extensions(al_types_bundle_spec* spec)
{
	switch (bp_implementation)
	{
	case BP_DTNME:
		bp_dtn_free_extensions(spec);
		break;

	case BP_ION:
		bp_ion_free_extensions(spec);
		break;

	case BP_IBR:
		bp_ibr_free_extensions(spec);
		break;

	case BP_UD3TN:
		bp_ud3tn_free_extensions(spec);
		break;

    case BP_UNIBOBP:
        bp_unibo_bp_free_extensions(spec);
        break;

	default: // cannot find bundle protocol implementation
		return ;
	}
}


/**
 * \brief Frees dynamic storage allocated by the xdr for a bundle payload in
 * bp_recv.
 */

void al_bp_free_payload(al_types_bundle_payload* payload)
{
    if (!payload) return;

	payload->status_report = NULL;
	switch (bp_implementation)
	{
	case BP_DTNME:
		bp_dtn_free_payload(payload);
		break;

	case BP_ION:
		bp_ion_free_payload(payload);
		break;

	case BP_IBR:
		bp_ibr_free_payload(payload);
		break;

	case BP_UD3TN:
		bp_ud3tn_free_payload(payload);
		break;

    case BP_UNIBOBP:
        bp_unibo_bp_free_payload(payload);
        break;

	default: // cannot find bundle protocol implementation
		return ;
	}
}



/**
 * \brief Get a string value associated with the bp error code.
 */


char * al_bp_strerror(int err){
	switch(err) {
	case BP_SUCCESS: 		return "success";
	case BP_EINVAL: 		return "invalid argument";
	case BP_ENULLPNTR:		return "operation on a null pointer";
	case BP_ECONNECT: 		return "error connecting to server";
	case BP_ETIMEOUT: 		return "operation timed out";
	case BP_ESIZE: 			return "payload too large";
	case BP_ENOTFOUND: 		return "not found";
	case BP_EINTERNAL: 		return "internal error";
	case BP_EBUSY:     		return "registration already in use";
	case BP_ENOSPACE:		return "no storage space";
	case BP_ENOTIMPL:		return "function not yet implemented";
	case BP_ENOBPI:			return "cannot find bundle protocol implementation";
	case BP_EMULTIBPI:		return "multiple running bundle protocol implementation found";
	case BP_EBADBPI:  		return "Unified API was not compiled for the running BP implementation";
	case BP_EATTACH:		return "cannot attach bundle protocol";
	case BP_EBUILDEID:		return "cannot build local eid";
	case BP_EOPEN :			return "cannot open the connection whit bp";
	case BP_EREG:			return "cannot register the eid";
	case BP_EPARSEEID:		return "cannot parse the endpoint string";
	case BP_ESEND:			return "cannot send Bundle";
	case BP_EUNREG:			return "cannot unregister eid";
	case BP_ERECV:			return "cannot receive bundle";
	case BP_ERECVINT:		return "receive bundle interrupted";
	case -1:				return "(invalid error code -1)";
	default:		   		break;
	}
	// there's a small race condition here in case there are two
	// simultaneous calls that will clobber the same buffer, but this
	// should be rare and the worst that happens is that the output
	// string is garbled
	static char buf[128];
	snprintf(buf, sizeof(buf), "(unknown error %d)", err);
	return buf;
}


