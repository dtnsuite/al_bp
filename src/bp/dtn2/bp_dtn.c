/** \file bp_dtn.c
 *
 *  \brief  This file contains the functions interfacing dtn2.
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *  \authors Silvia Lanzoni, silvia Lanzoni5@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *  01/01/20 | A. Bisacchi	   |  Implementation contribution.
 *  01/10/21 | S. Lanzoni	   |  Function names updated.
 *  11/08/23 | L. Fantini      |  Removed memcpy of payload
 */

#include "bp_dtn.h"

/*
 * if there is the DTN2 implementation on the
 * machine the API are actually implemented; otherwise they are just dummy functions
 * to avoid compilation errors.
 */
#ifdef DTN2_IMPLEMENTATION

#include "bp_dtn_conversions.h"

al_bp_error_t bp_dtn_open(al_types_handle* handle)
{
	dtn_handle_t dtn_handle = bp_to_dtn_handle(*handle);
	int result = dtn_open(& dtn_handle);
	* handle = bp_from_dtn_handle(dtn_handle);
	return bp_dtn_error(result);
}

al_bp_error_t bp_dtn_open_with_IP(char * daemon_api_IP, int daemon_api_port, al_types_handle * handle)
{
	dtn_handle_t dtn_handle = bp_to_dtn_handle(*handle);
	int result = dtn_open_with_IP(daemon_api_IP, daemon_api_port, & dtn_handle);
	* handle = bp_from_dtn_handle(dtn_handle);
	return bp_dtn_error(result);
}

al_bp_error_t bp_dtn_errno(al_types_handle handle)
{
	dtn_handle_t dtn_handle = bp_to_dtn_handle(handle);
	int result = dtn_errno(dtn_handle);
	handle = bp_from_dtn_handle(dtn_handle);
	return bp_dtn_error(result);
}

/**
 * Build a local eid for DTN2 implementation
 * if type == IPN_SCHEME, service_tag must be ipn_local_number.service_number
 */
al_bp_error_t bp_dtn_build_local_eid(al_types_handle handle,
								al_types_endpoint_id* local_eid,
								unsigned long ipn_node_number,
								const char* service_tag,
								al_types_scheme type)
{
	if (type == DTN_SCHEME)
	{
		dtn_handle_t dtn_handle = bp_to_dtn_handle(handle);
		dtn_endpoint_id_t dtn_local_eid = bp_to_dtn_endpoint_id(*local_eid);
		int result = dtn_build_local_eid(dtn_handle, & dtn_local_eid, service_tag);
		handle = bp_from_dtn_handle(dtn_handle);
		* local_eid = bp_from_dtn_endpoint_id(dtn_local_eid);
		if(result != DTN_SUCCESS) {
			return bp_dtn_errno(handle);
		}
		return BP_SUCCESS;
	}
	else if (type == IPN_SCHEME)
	{
		// build the string: {ipn_node_number}.{service_number}
		char *ptr;
		unsigned long service_number = strtol(service_tag, &ptr, 10);
		
		// "In  particular, if *nptr is not '\0' but **endptr is '\0' on return, the entire string is valid"
		if(*service_tag == '\0' || *ptr != '\0')
			return BP_EINVAL;
		
		if (ipn_node_number < 0 || service_number < 0)
			return BP_EPARSEEID;

		sprintf(local_eid->uri, "ipn:%lu.%lu", ipn_node_number, service_number);
		return BP_SUCCESS;
	}
	else
		return BP_EINVAL;

}

al_bp_error_t bp_dtn_register(al_types_handle handle,
						al_types_reg_info* reginfo,
						al_types_reg_id* newregid)
{
	al_bp_error_t status;
	al_types_reg_id tmp;
	status = bp_dtn_find_registration(handle, &(reginfo->endpoint), &tmp);
	if(status == BP_SUCCESS) {
		return BP_EBUSY;
	} else if(status != BP_ENOTFOUND) {
		return status;
	}

	dtn_handle_t dtn_handle = bp_to_dtn_handle(handle);
	dtn_reg_info_t dtn_reginfo = bp_to_dtn_reg_info(*reginfo);
	dtn_reg_id_t dtn_newregid = bp_to_dtn_reg_id(*newregid);
	int result = dtn_register(dtn_handle, & dtn_reginfo, & dtn_newregid);
	handle = bp_from_dtn_handle(dtn_handle);
	*reginfo = bp_from_dtn_reg_info(dtn_reginfo);
	*newregid = bp_from_dtn_reg_id(dtn_newregid);
	if(result != DTN_SUCCESS) {
		return bp_dtn_errno(handle);
	}
	return BP_SUCCESS;
}

al_bp_error_t bp_dtn_find_registration(al_types_handle handle,
						al_types_endpoint_id * eid,
						al_types_reg_id * newregid)
{
	dtn_handle_t dtn_handle = bp_to_dtn_handle(handle);
	dtn_endpoint_id_t dtn_eid = bp_to_dtn_endpoint_id(*eid);
	dtn_reg_id_t dtn_newregid = bp_to_dtn_reg_id(*newregid);
	int result = dtn_find_registration(dtn_handle, & dtn_eid, & dtn_newregid);
	handle = bp_from_dtn_handle(dtn_handle);
	* eid = bp_from_dtn_endpoint_id(dtn_eid);
	*newregid = bp_from_dtn_reg_id(dtn_newregid);
	if(result != DTN_SUCCESS) {
		return bp_dtn_errno(handle);
	}
	return BP_SUCCESS;
}

al_bp_error_t bp_dtn_unregister(al_types_handle handle,al_types_reg_id regid){
	dtn_handle_t dtn_handle = bp_to_dtn_handle(handle);
	dtn_reg_id_t dtn_regid = bp_to_dtn_reg_id(regid);
	int result = dtn_unregister(dtn_handle,dtn_regid);
	if(result != DTN_SUCCESS) {
		return bp_dtn_errno(handle);
	}
	return BP_SUCCESS;
}

al_bp_error_t bp_dtn_send(al_types_handle handle,
					al_types_reg_id regid,
					al_types_bundle_spec* spec,
					al_types_bundle_payload* payload)
{

	al_types_bundle_id id;
	id.source=spec->source;
	id.creation_ts.time=0;
	id.creation_ts.seqno=0;
	id.frag_offset=0;
	id.frag_length=0;
	dtn_handle_t dtn_handle = bp_to_dtn_handle(handle);
	dtn_reg_id_t dtn_regid = bp_to_dtn_reg_id(regid);
	dtn_bundle_spec_t dtn_spec = bp_to_dtn_bundle_spec(*spec);
	dtn_bundle_payload_t dtn_payload = bp_to_dtn_bundle_payload(*payload); // copy address: dtn_payload.buf.buf_val  -->  al_types_bundle_payload.buf.buf_val  -->  buffer
	dtn_bundle_id_t dtn_id = bp_to_dtn_bundle_id(id);
	int result = dtn_send(dtn_handle, dtn_regid, & dtn_spec, & dtn_payload, & dtn_id);
//	if (dtn_payload.filename.filename_val!=NULL) free (dtn_payload.filename.filename_val);
//	if (dtn_payload.buf.buf_val!=NULL) free (dtn_payload.buf.buf_val);
	handle = bp_from_dtn_handle(dtn_handle);
	regid = bp_from_dtn_reg_id(dtn_regid);
//	*spec = bp_from_dtn_bundle_spec(dtn_spec); //CCaini useless
//	*payload = bp_from_dtn_bundle_payload(dtn_payload); //CCaini useless
	spec->creation_ts = bp_from_dtn_timestamp(dtn_id.creation_ts);
	if(result != DTN_SUCCESS) {
		return bp_dtn_errno(handle);
	}
	return BP_SUCCESS;
}

al_bp_error_t bp_dtn_recv(al_types_handle handle,
					al_types_bundle_spec* spec,
					al_types_bundle_payload_location location,
					al_types_bundle_payload* payload,
					al_types_timeout timeout)
{
	dtn_handle_t dtn_handle = bp_to_dtn_handle(handle);
	dtn_bundle_spec_t dtn_spec = bp_to_dtn_bundle_spec(*spec);
	dtn_bundle_payload_location_t dtn_location = bp_to_dtn_bundle_payload_location(location);
	dtn_bundle_payload_t dtn_payload = bp_to_dtn_bundle_payload(*payload);
	dtn_timeval_t dtn_timeout = bp_to_dtn_timeout(timeout);
 	int result = dtn_recv(dtn_handle, & dtn_spec, dtn_location, & dtn_payload, dtn_timeout);
	handle = bp_from_dtn_handle(dtn_handle);
	*spec = bp_from_dtn_bundle_spec(dtn_spec);
	location = bp_from_dtn_bundle_payload_location(dtn_location);
	*payload = bp_from_dtn_bundle_payload(dtn_payload); // copy address of XDR allocated buffer
	timeout = bp_from_dtn_timeout(dtn_timeout);
	if(result != DTN_SUCCESS) {
		return bp_dtn_errno(handle);
	}
	return BP_SUCCESS;
}

al_bp_error_t bp_dtn_close(al_types_handle handle)
{
	dtn_handle_t dtn_handle = bp_to_dtn_handle(handle);
	int result = dtn_close(dtn_handle);
	handle = bp_from_dtn_handle(dtn_handle);
	return bp_dtn_error(result);
}

//TODO: verify the rational of the following lines
void bp_dtn_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src)
{
	dtn_endpoint_id_t dtn_dst = bp_to_dtn_endpoint_id(*dst);
	dtn_endpoint_id_t dtn_src = bp_to_dtn_endpoint_id(*src);
	dtn_copy_eid(& dtn_dst, & dtn_src);
	*dst = bp_from_dtn_endpoint_id(dtn_dst);
	*src = bp_from_dtn_endpoint_id(dtn_src);
}

al_bp_error_t bp_dtn_parse_eid_string(al_types_endpoint_id* eid, const char* str)
{
	dtn_endpoint_id_t dtn_eid = bp_to_dtn_endpoint_id(*eid);
	int result =  dtn_parse_eid_string(& dtn_eid, str);
	*eid = bp_from_dtn_endpoint_id(dtn_eid);
	return bp_dtn_error(result);
}

al_bp_error_t bp_dtn_set_payload(al_types_bundle_payload* payload,
							al_types_bundle_payload_location location,
							char* val, int len)
{
	dtn_bundle_payload_t dtn_payload = bp_to_dtn_bundle_payload(*payload);
	dtn_bundle_payload_location_t dtn_location = bp_to_dtn_bundle_payload_location(location);
	int result = dtn_set_payload(& dtn_payload, dtn_location, val, len);
	*payload = bp_from_dtn_bundle_payload(dtn_payload);
	return bp_dtn_error(result);
}

//dtn_bundle_payload.buf.buf_val
void bp_dtn_free_payload(al_types_bundle_payload* payload)
{
	if (payload->status_report != NULL)
	{
		free(payload->status_report);
		payload->status_report = NULL;
	}
	dtn_bundle_payload_t dtn_payload = bp_to_dtn_bundle_payload(*payload);
	dtn_free_payload(&dtn_payload);		// free allocated memory by XDR
	*payload = bp_from_dtn_bundle_payload(dtn_payload);
}

void bp_dtn_free_extensions(al_types_bundle_spec* spec)
{
        int i;
        for ( i=0; i<spec->extensions.extension_number; i++ ) {
            //printf("Freeing extension block [%d].data at 0x%08X\n", i, (unsigned int) *(spec->blocks.blocks_val[i].data.data_val));
            if (spec->extensions.extension_blocks[i].block_data.block_type_specific_data != NULL) {
		    free(spec->extensions.extension_blocks[i].block_data.block_type_specific_data);
		    spec->extensions.extension_blocks[i].block_data.block_type_specific_data = NULL;
            }
        }
        if (spec->extensions.extension_blocks != NULL) {
		free(spec->extensions.extension_blocks);
		spec->extensions.extension_blocks = NULL;
        }
        spec->extensions.extension_number = 0;
}

al_bp_error_t bp_dtn_error(int err)
{
	switch (err)
	{
	case DTN_SUCCESS:	return BP_SUCCESS;
	case DTN_EINVAL:	return BP_EINVAL;
	case DTN_ECONNECT:	return BP_ECONNECT;
	case DTN_ETIMEOUT:	return BP_ETIMEOUT;
	case DTN_ESIZE:		return BP_ESIZE;
	case DTN_ENOTFOUND:	return BP_ENOTFOUND;
	case DTN_EXDR:
	case DTN_ECOMM:
	case DTN_EINPOLL:
	case DTN_EVERSION:
	case DTN_EMSGTYPE:
	case DTN_EINTERNAL: return BP_EINTERNAL;
	case DTN_EBUSY:     return BP_EBUSY;
	case DTN_ENOSPACE:	return BP_ENOSPACE;
	default: 			return -1;
	}
}
/*
 * if there isn't the DTN2 implementation
 * the APIs are just dummy calls
 */
#else
al_bp_error_t bp_dtn_open(al_types_handle* handle)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_dtn_open_with_IP(char * daemon_api_IP, int daemon_api_port, al_types_handle * handle)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_dtn_errno(al_types_handle handle)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_dtn_build_local_eid(al_types_handle handle,
								al_types_endpoint_id* local_eid,
								unsigned long ipn_node_number,
								const char* service_tag,
								al_types_scheme type)
{
	return BP_ENOTIMPL;

}

al_bp_error_t bp_dtn_register(al_types_handle handle,
						al_types_reg_info* reginfo,
						al_types_reg_id* newregid)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_dtn_find_registration(al_types_handle handle,
						al_types_endpoint_id * eid,
						al_types_reg_id * newregid)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_dtn_unregister(al_types_handle handle,al_types_reg_id regid){
	return BP_ENOTIMPL;
}

al_bp_error_t bp_dtn_send(al_types_handle handle,
					al_types_reg_id regid,
					al_types_bundle_spec* spec,
					al_types_bundle_payload* payload)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_dtn_recv(al_types_handle handle,
					al_types_bundle_spec* spec,
					al_types_bundle_payload_location location,
					al_types_bundle_payload* payload,
					al_types_timeout timeout)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_dtn_close(al_types_handle handle)
{
	return BP_ENOTIMPL;
}

void bp_dtn_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src)
{

}

al_bp_error_t bp_dtn_parse_eid_string(al_types_endpoint_id* eid, const char* str)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_dtn_set_payload(al_types_bundle_payload* payload,
							al_types_bundle_payload_location location,
							char* val, int len)
{
	return BP_ENOTIMPL;
}

void bp_dtn_free_payload(al_types_bundle_payload* payload)
{

}

void bp_dtn_free_extensions(al_types_bundle_spec* spec)
{

}

al_bp_error_t bp_dtn_error(int err)
{
	return BP_ENOTIMPL;
}
#endif /* DTN2_IMPLEMENTATION */

boolean_t bp_dtn_is_running() {
	char* find_dtnd = "ps ax | grep -w dtnd	| grep -v grep > /dev/null";
	char* find_dtnme = "ps ax | grep -w dtnme | grep -v grep > /dev/null";
	if(system(find_dtnd) == 0 || system(find_dtnme) == 0) {
		return TRUE;
	}
	return FALSE;
}
