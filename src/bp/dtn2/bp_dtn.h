/** \file bp_dtn.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
  *
 *  \par Copyright
 *  	Copyright (c) 2013, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Silvia Lanzoni, silvia Lanzoni5@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *  01/10/21 | S. Lanzoni	   |  Function names updated.
 *
 */

#ifndef BP_DTN_H_
#define BP_DTN_H_

#include "al_types.h"
#include "al_bp.h"

#ifdef DTN2_IMPLEMENTATION

#ifdef HAVE_CONFIG_H
#  include <dtn-config.h>
#endif

#endif
/* al_bp_error_t is the type of the value returned by bp_dtn_open(). Analogously for other ones*/
al_bp_error_t bp_dtn_open(al_types_handle* handle);

al_bp_error_t bp_dtn_open_with_IP(char * daemon_api_IP, int daemon_api_port, al_types_handle * handle);

al_bp_error_t bp_dtn_errno(al_types_handle handle);

boolean_t bp_dtn_is_running();

al_bp_error_t bp_dtn_build_local_eid(al_types_handle handle,
								al_types_endpoint_id* local_eid,
								unsigned long ipn_node_number,
								const char* service_tag,
								al_types_scheme type);


al_bp_error_t bp_dtn_register(al_types_handle handle,
						al_types_reg_info* reginfo,
						al_types_reg_id* newregid);

al_bp_error_t bp_dtn_find_registration(al_types_handle handle,
						al_types_endpoint_id * eid,
						al_types_reg_id * newregid);

al_bp_error_t bp_dtn_unregister(al_types_handle handle,al_types_reg_id regid);

al_bp_error_t bp_dtn_send(al_types_handle handle,
					al_types_reg_id regid,
					al_types_bundle_spec* spec,
					al_types_bundle_payload* payload);

al_bp_error_t bp_dtn_recv(al_types_handle handle,
					al_types_bundle_spec* spec,
					al_types_bundle_payload_location location,
					al_types_bundle_payload* payload,
					al_types_timeout timeout);

al_bp_error_t bp_dtn_close(al_types_handle handle);

void bp_dtn_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src);


al_bp_error_t bp_dtn_parse_eid_string(al_types_endpoint_id* eid, const char* str);

al_bp_error_t bp_dtn_set_payload(al_types_bundle_payload* payload,
							al_types_bundle_payload_location location,
							char* val, int len);

void bp_dtn_free_payload(al_types_bundle_payload* payload);

void bp_dtn_free_extension_blocks(al_types_bundle_spec* spec);

void bp_dtn_free_metadata_blocks(al_types_bundle_spec* spec);

void bp_dtn_free_extensions(al_types_bundle_spec* spec);

/**
 * converts DTN errors in the corresponding al_bp_error_t values
 */
al_bp_error_t bp_dtn_error(int err);

#endif /* BP_DTN_H_ */
