/** \file bp_dtn_conversions.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *  \par Copyright
 *  	Copyright (c) 2013, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Silvia Lanzoni, silvia Lanzoni5@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *  01/10/21 | S.Lanzoni	   |  Function names updated.
 *
 */


#ifndef BP_DTN_CONVERSIONS_H_
#define BP_DTN_CONVERSIONS_H_

#ifdef DTN2_IMPLEMENTATION

#include "al_types.h"

#include <dtn_types.h>
#include <dtn_api.h>

/*
 * These functions convert bp abstract types in dtn types and viceversa
 * The prefix bp_dtn means the function takes a bp abstract type in and returns a dtn type
 * so the conversion is bp -> dtn
 * The prefix dtn_bp means the function takes a dtn type in and returns a bp abstract type
 * so the conversion is dtn -> bp
 */

dtn_handle_t bp_to_dtn_handle(al_types_handle handle);
al_types_handle bp_from_dtn_handle(dtn_handle_t handle);

dtn_endpoint_id_t bp_to_dtn_endpoint_id(al_types_endpoint_id endpoint_id);
al_types_endpoint_id bp_from_dtn_endpoint_id(dtn_endpoint_id_t endpoint_id);

dtn_timeval_t bp_to_dtn_timeval(al_types_timeval timeval);
al_types_timeval bp_from_dtn_timeval(dtn_timeval_t);

dtn_timeval_t bp_to_dtn_timeout(al_types_timeout timeout);
al_types_timeout bp_from_dtn_timeout(dtn_timeval_t timeout);


dtn_timestamp_t bp_to_dtn_timestamp(al_types_creation_timestamp timestamp);
al_types_creation_timestamp bp_from_dtn_timestamp(dtn_timestamp_t timestamp);

dtn_reg_token_t bp_to_dtn_reg_token(al_types_reg_token reg_token);
al_types_reg_token bp_from_dtn_reg_token(dtn_reg_token_t reg_token);

dtn_reg_id_t bp_to_dtn_reg_id(al_types_reg_id reg_id);
al_types_reg_id bp_from_dtn_reg_id(dtn_reg_id_t reg_id);

dtn_reg_info_t bp_to_dtn_reg_info(al_types_reg_info reg_info);
al_types_reg_info bp_from_dtn_reg_info(dtn_reg_info_t reg_info);

dtn_reg_flags_t bp_to_dtn_reg_flags(al_types_reg_flags reg_flags);
al_types_reg_flags bp_from_dtn_reg_flags(dtn_reg_flags_t reg_flags);

dtn_bundle_delivery_opts_t bp_to_dtn_bundle_delivery_opts(al_types_bundle_processing_control_flags bundle_delivery_opts);
al_types_bundle_processing_control_flags bp_from_dtn_bundle_delivery_opts(dtn_bundle_delivery_opts_t bundle_delivery_opts);

dtn_bundle_priority_t bp_to_dtn_bundle_priority(al_types_bundle_priority_enum bundle_priority);
al_types_bundle_priority_enum bp_from_dtn_bunlde_priority(dtn_bundle_priority_t bundle_priority);

dtn_bundle_spec_t bp_to_dtn_bundle_spec(al_types_bundle_spec bundle_spec);
al_types_bundle_spec bp_from_dtn_bundle_spec(dtn_bundle_spec_t bundle_spec);

dtn_bundle_payload_location_t bp_to_dtn_bundle_payload_location(al_types_bundle_payload_location bundle_payload_location);
al_types_bundle_payload_location bp_from_dtn_bundle_payload_location(dtn_bundle_payload_location_t bundle_payload_location);

dtn_status_report_reason_t bp_to_dtn_status_report_reason(al_types_status_report_reason status_report_reason);
al_types_status_report_reason bp_from_dtn_status_report_reason(dtn_status_report_reason_t status_report_reason);

dtn_status_report_flags_t bp_to_dtn_status_report_flags(al_types_status_report_flags status_repot_flags);
al_types_status_report_flags bp_from_dtn_status_report_flags(dtn_status_report_flags_t status_repot_flags);

dtn_bundle_id_t bp_to_dtn_bundle_id(al_types_bundle_id bundle_id);
al_types_bundle_id bp_from_dtn_bundle_id(dtn_bundle_id_t bundle_id);

dtn_bundle_status_report_t bp_to_dtn_bundle_status_report(al_types_bundle_status_report bundle_status_report);
al_types_bundle_status_report * bp_from_dtn_bundle_status_report(dtn_bundle_status_report_t bundle_status_report);

al_types_bundle_payload bp_from_dtn_bundle_payload(dtn_bundle_payload_t bundle_payload);
dtn_bundle_payload_t bp_to_dtn_bundle_payload(al_types_bundle_payload bundle_payload);


#endif /* DTN2_IMPLEMENTATION*/
#endif /* BP_DTN_CONVERSIONS_H_ */
