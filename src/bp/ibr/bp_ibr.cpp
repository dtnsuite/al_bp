/** \file bp_ibr.cpp
 *
 *  \brief  This file contains the implementation of the functions interfacing IBR-DTN. It nees g++
 *
 *  \par Copyright
 *  	Copyright (c) 2016, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Davide Pallotti, davide.pallotti@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/16 | D. Pallotti	   |  Implementation contribution.
 *
 *
 */

#include "bp_ibr.h"
#include <stdlib.h> // system

/*
 * if there is the IBR-DTN implementation on the
 * machine the functions are actually implemented; 
 * otherwise they are just dummy functions
 * to avoid compilation errors
 */
#ifdef IBRDTN_IMPLEMENTATION

#include <ibrdtn/api/Client.h>
#include <ibrdtn/data/StatusReportBlock.h> //AdministrativeBlock, StatusReportBlock
#include <ibrdtn/utils/Clock.h> //Clock::getTime();

#include <cstdlib>
#include <cstring>
#include <sstream> //istringstream, ostringstream
#include <unistd.h> //access, unlink
#include <ctime> //time
#include <cctype> //isdigit

#define PRINT_EXCEPTIONS

//private structure holding objects for an open-register-unregister-close session
struct IbrHandle {
	char* daemonIP;
	int daemonPort;
	ibrcommon::socketstream* stream;
	dtn::api::Client* client;
	al_bp_error_t error;
};


al_bp_error_t bp_ibr_errno(al_types_handle handle)
{
	IbrHandle* ibrHandle = (IbrHandle*) handle;
	if (ibrHandle == NULL)
		return BP_ENULLPNTR;
	
	return ibrHandle->error;
}

al_bp_error_t bp_ibr_open(al_types_handle* handle_p) //nonnull: parameters marked "nonnull" are checked for NULL in al_bp_api.c
{
	return bp_ibr_open_with_IP("localhost", 4550, handle_p);
}

al_bp_error_t bp_ibr_open_with_IP(const char* daemon_api_IP, 
                                  int daemon_api_port, 
                                  al_types_handle* handle_p) //nonnull
{
	if (daemon_api_IP == NULL)
		return BP_ENULLPNTR;
	
	ibrcommon::vaddress vaddress(daemon_api_IP, daemon_api_port);
	
	ibrcommon::tcpsocket* socket_p = new ibrcommon::tcpsocket(vaddress);
	//opened when socketstream is constructed,
	//closed when socketstream is closed,
	//deleted when socketstream is deleted (by ibrcommon::vsocket)
	
	ibrcommon::socketstream* stream;
	try {
		stream = new ibrcommon::socketstream(socket_p);
		
	} catch (const ibrcommon::socket_exception& e) {
#ifdef PRINT_EXCEPTIONS
		std::cerr << e.what() << std::endl;
		//"111: Connection refused" / "getaddrinfo(): Name or service not known"
#endif
		
		//because socketstream's constructor threw, its destructor was not called
		socket_p->down(); //unnecessary, called by the destructor
		delete socket_p;
		
		*handle_p = NULL;
		return BP_ECONNECT;
	}
	
	IbrHandle* ibrHandle = new IbrHandle();
	
	ibrHandle->daemonIP = new char[strlen(daemon_api_IP) + 1];
	strcpy(ibrHandle->daemonIP, daemon_api_IP);
	ibrHandle->daemonPort = daemon_api_port;
	
	ibrHandle->stream = stream;
	
	//remember that al_types_handle is just a pointer: typedef int* al_types_handle;
	//then from now on we will use al_types_handle as IbrHandle* instead of int*
	*handle_p = (al_types_handle) ibrHandle;
	
	return ibrHandle->error = BP_SUCCESS;
}

char* get_local_node(IbrHandle* ibrHandle) //nonnull
{
	if (ibrHandle->daemonIP == NULL || ibrHandle->daemonPort <= 0)
		return NULL;
	
	static char local_node[256]; //initialized to zero because it's static
	
	if (strlen(local_node) > 0)
		return local_node;
	
	ibrcommon::vaddress vaddress1(ibrHandle->daemonIP, ibrHandle->daemonPort);
	ibrcommon::tcpsocket* socket_p1 = new ibrcommon::tcpsocket(vaddress1); 
	ibrcommon::socketstream* stream_p1;
	try {
		stream_p1 = new ibrcommon::socketstream(socket_p1);
	} catch (ibrcommon::socket_exception& e) {
#ifdef PRINT_EXCEPTIONS
		std::cerr << e.what() << std::endl;
#endif
		delete socket_p1;
		return NULL;
	}
	
	static const char* const sender = "1073741823"; //a numeric demux token works for both the DTN scheme and the IPN scheme
	static const char* const receiver = "1073741824";
	static const char* const group = "dtn://bp_ibr_build_local_eid_group_node/bp_ibr_build_local_eid_group";
	
	dtn::api::Client client1(sender, *stream_p1);
	client1.connect();
	
	dtn::data::Bundle bundle;
	bundle.destination = EID(group);
	bundle.lifetime = 1;
	
	ibrcommon::BLOB::Reference blob = ibrcommon::BLOB::create();
	(*blob.iostream()) << ""; //dummy payload, needed
	bundle.push_back(blob);
	
	client1 << bundle;
	
	client1.close();
	stream_p1->close();
	
	
	ibrcommon::vaddress vaddress2(ibrHandle->daemonIP, ibrHandle->daemonPort);
	ibrcommon::tcpsocket* socket_p2 = new ibrcommon::tcpsocket(vaddress2); 
	ibrcommon::socketstream* stream_p2;
	try {
		stream_p2 = new ibrcommon::socketstream(socket_p2);
	} catch (ibrcommon::socket_exception& e) {
#ifdef PRINT_EXCEPTIONS
		std::cerr << e.what() << std::endl;
#endif
		delete socket_p2;
		return NULL;
	}
	
	dtn::api::Client client2(receiver, EID(group), *stream_p2);
	client2.connect();
	
	try {
		bundle = client2.getBundle(1);
	} catch (dtn::api::ConnectionException& e) {
#ifdef PRINT_EXCEPTIONS
		std::cerr << e.what() << std::endl;
#endif
		delete socket_p2;
		return NULL;
	}
	
	client2.close();
	stream_p2->close();
	
	if (bundle.source.getString().length() + 1 > 256)
		return NULL;
	
	char source[bundle.source.getString().length() + 1];
	strcpy(source, bundle.source.getString().c_str());
	
	char* demuxPtr = strstr(source, sender);
	if (demuxPtr == NULL)
		return NULL;
	--demuxPtr; //. or /
	strncpy(local_node, source, demuxPtr - source);
	local_node[demuxPtr - source] = '\0';
	
	return local_node;
}

al_bp_error_t bp_ibr_build_local_eid(al_types_handle handle,
                                     al_types_endpoint_id* local_eid, //nonnull
                                     const char* service_tag,
                                     al_types_scheme type)
{
	IbrHandle* ibrHandle = (IbrHandle*) handle;
	if (ibrHandle == NULL)
		return BP_ENULLPNTR;
	
	const char* const local_node = get_local_node(ibrHandle);
	if (local_node == NULL)
		return ibrHandle->error = BP_EBUILDEID;
	
	if (type == DTN_SCHEME) {
		
		strcpy(local_eid->uri, local_node);
		
		if (service_tag != NULL && strlen(service_tag) > 0) {
			if (service_tag[0] != '/')
				strcat(local_eid->uri, "/");
			
			strncat(local_eid->uri, service_tag, AL_TYPES_MAX_EID_LENGTH - strlen(local_eid->uri) - 1);
		
		} else { //random service_tag
			strcat(local_eid->uri, "/");
		
			static bool srand_called = false;
			if (!srand_called) {
				srand(time(NULL));
				srand_called = true;
			}
		
			uint32_t initial_len = strlen(local_eid->uri);
			for (uint32_t i = 0; i < 16; ++i)
				local_eid->uri[initial_len + i] = "abcdefghijklmnopqrstuvwxyz"[rand() % 26];
			local_eid->uri[initial_len + 16] = '\0';
			
			//NOTE if we left local_eid->uri empty, with "local_eid->uri[0] = '\0';",
			//upon registration the daemon would assign a random 16-letter long tag
			//with both capital and lower case letters;
			//since capital letters could cause incompatibilities with DTN2,
			//we choose to generate a random tag ourselves
		}
		
		return ibrHandle->error = BP_SUCCESS;
		
	} else if (type == IPN_SCHEME) {
		if (strcmp(service_tag, ".") == 0)
			return ibrHandle->error = BP_EBUILDEID;
		
		for (uint32_t i = 0; i < strlen(service_tag) && i < AL_TYPES_MAX_EID_LENGTH; ++i)
			if (!isdigit(service_tag[i]) && !(i == 0 && service_tag[i] == '.'))
				return ibrHandle->error = BP_EBUILDEID;		
		
		strcpy(local_eid->uri, local_node);
		if (service_tag[0] != '.')
			strcat(local_eid->uri, "."); //concatenate the dot to local eid
	//concatenate the first n characters of service tag to local eid, n=last parameter
		strncat(local_eid->uri, service_tag, AL_TYPES_MAX_EID_LENGTH - strlen(local_eid->uri) - 1);
		
		return ibrHandle->error = BP_SUCCESS;
		
	} else 
		return ibrHandle->error = BP_EINVAL;
}

al_bp_error_t bp_ibr_register(al_types_handle handle,
                              al_types_reg_info* reginfo, //nonnull
                              al_types_reg_id* newregid) //nonnull
{
	IbrHandle* ibrHandle = (IbrHandle*) handle;
	if (ibrHandle == NULL)
		return BP_ENULLPNTR;
	if (ibrHandle->stream == NULL)
		return ibrHandle->error = BP_ENULLPNTR;	

	const char* uri = reginfo->endpoint.uri;
	
	const char* const local_node = get_local_node(ibrHandle);
	if (local_node != NULL && strstr(uri, local_node) == uri)
		uri += strlen(local_node);
	
	if (uri[0] == '/' || uri[0] == '.')
		++uri;
	
	dtn::api::Client* client = new dtn::api::Client(std::string(uri), *(ibrHandle->stream));
	//NOTE the other constructor, Client(const std::string &app, const dtn::data::EID &group, ibrcommon::socketstream &stream),
	//allows to receive bundles addressed to both the local EID in the form dtn://<hostname>/<app> (also used as source EID) 
	//and the group EID, as long as it contains a colon ":" and the first colon is not the first or the last character (tested)
	
	client->connect();
	
	ibrHandle->client = client;
	
	if (!reginfo->init_passive) {
		//if needed, implement callback by overriding dtn::api::Client::received(const dtn::data::Bundle &b)
	}
	
	*newregid = 0; //don't know what this is
	
	return ibrHandle->error = BP_SUCCESS;
}

al_bp_error_t bp_ibr_find_registration(al_types_handle handle,
                                       al_types_endpoint_id* eid, //nonnull
                                       al_types_reg_id* newregid)
{
	IbrHandle* ibrHandle = (IbrHandle*) handle;
	if (ibrHandle == NULL)
		return BP_ENULLPNTR;
	
	//NOTE this function cannot be implemented using IBR-DTN's APIs,
	//but since it is used to check if a registration already exists, 
	//make it return false (error) as IBR allows multiple registrations of the same EID
	return ibrHandle->error = BP_ENOTFOUND;
}

al_bp_error_t bp_ibr_send(al_types_handle handle,
                          al_types_reg_id regid, //unused
                          al_types_bundle_spec* spec, //nonnull
                          al_types_bundle_payload* payload) //nonnull
{
	IbrHandle* ibrHandle = (IbrHandle*) handle;
	if (ibrHandle == NULL)
		return BP_ENULLPNTR;
	if (ibrHandle->client == NULL)
		return ibrHandle->error = BP_ENULLPNTR;
	
#if 0
	clock_t start = clock();
#endif
	dtn::data::Bundle bundle;
	//bundle.timestamp and bundle.sequencenumber are set now by the constructor
	
	bundle.source = EID(std::string(spec->source.uri));
	bundle.destination = EID(std::string(spec->destination.uri));
	//I assume spec->replyto is the report-to EID described in RFC5050
	bundle.reportto = EID(std::string(spec->report_to.uri));
	
	bundle.setPriority((dtn::data::PrimaryBlock::PRIORITY) spec->cardinal);
	
	if (spec->bundle_proc_ctrl_flags & BP_DOPTS_CUSTODY)
		bundle.set(dtn::data::PrimaryBlock::CUSTODY_REQUESTED, true);
	if (spec->bundle_proc_ctrl_flags & BP_DOPTS_DELIVERY_BSR)
		bundle.set(dtn::data::PrimaryBlock::REQUEST_REPORT_OF_BUNDLE_DELIVERY, true);
	if (spec->bundle_proc_ctrl_flags & BP_DOPTS_RECEPTION_BSR)
		bundle.set(dtn::data::PrimaryBlock::REQUEST_REPORT_OF_BUNDLE_RECEPTION, true);
	if (spec->bundle_proc_ctrl_flags & BP_DOPTS_FORWARD_BSR)
		bundle.set(dtn::data::PrimaryBlock::REQUEST_REPORT_OF_BUNDLE_FORWARDING, true);
	if (spec->bundle_proc_ctrl_flags & BP_DOPTS_CUSTODY_BSR)
		bundle.set(dtn::data::PrimaryBlock::REQUEST_REPORT_OF_CUSTODY_ACCEPTANCE, true);
	if (spec->bundle_proc_ctrl_flags & BP_DOPTS_DELETION_BSR)
		bundle.set(dtn::data::PrimaryBlock::REQUEST_REPORT_OF_BUNDLE_DELETION, true);
	if (spec->bundle_proc_ctrl_flags & BP_DOPTS_SINGLETON_DEST)
		bundle.set(dtn::data::PrimaryBlock::DESTINATION_IS_SINGLETON, true);
	if (spec->bundle_proc_ctrl_flags & BP_DOPTS_ACK_REQUESTED_BY_APPLICATION) //new
		bundle.set(dtn::data::PrimaryBlock::ACKOFAPP_REQUESTED, true);
	if (spec->bundle_proc_ctrl_flags & BP_DOPTS_MULTINODE_DEST)
		; //no equivalent in RFC5050 or IBR-DTN's APIs
	if (spec->bundle_proc_ctrl_flags & BP_DOPTS_DO_NOT_FRAGMENT)
		bundle.set(dtn::data::PrimaryBlock::DONT_FRAGMENT, true);
	
	bundle.lifetime = spec->lifetime;
	
	//return the bundle's "identity" to the caller
	spec->creation_ts.time = bundle.timestamp.get(); //bundle.timestamp = spec->creation_ts.secs;
	spec->creation_ts.seqno = bundle.sequencenumber.get(); //bundle.sequencenumber = spec->creation_ts.seqno;
	//no need for spec->delivery_regid in IBR-DTN's API
	
	//-------- spec->blocks --------
	
	//dtn::data::ExtensionBlock::Factory has a protected constructor and 
	//dtn::data::ExtensionBlock::Factory::get((uint8_t) spec->blocks.blocks_val[block_index].type);
	//always produces a "Factory not available" ibrcommon::Exception
	class ExtensionBlockFactory : public dtn::data::ExtensionBlock::Factory
	{
	public:
		ExtensionBlockFactory(block_t type)
			: dtn::data::ExtensionBlock::Factory(type), _type(type) {}
			
		virtual dtn::data::Block* create() {
			dtn::data::ExtensionBlock* extensionBlock = new dtn::data::ExtensionBlock();
			extensionBlock->setType(_type);
			return extensionBlock;
		}
	private:
		block_t _type;
	};
	
	for (uint32_t block_index = 0; block_index < spec->extensions.extension_number; ++block_index) {
		dtn::data::ExtensionBlock::Factory* extblockFactory;
		//pointer needed because ExtensionBlock::Factory is abstract
		
		extblockFactory = new ExtensionBlockFactory((uint8_t) spec->extensions.extension_blocks[block_index].block_type_code);
		
		// try {
			// extblockFactory = &dtn::data::ExtensionBlock::Factory::get(
				// (uint8_t) spec->blocks.blocks_val[block_index].type);
			
		// } catch (ibrcommon::Exception& e) { //always thrown
// #ifdef PRINT_EXCEPTIONS
			// std::cerr << e.what() << std::endl;
			// //"Factory not available"
// #endif
			// return BP_ESEND;
		// }
		
		dtn::data::Block* extblock = &bundle.push_back(*extblockFactory); 
		//pointer needed because Block is abstract
			
		if (spec->extensions.extension_blocks[block_index].block_processing_control_flags & (1 << 0))
			extblock->set(dtn::data::Block::REPLICATE_IN_EVERY_FRAGMENT, true);
		if (spec->extensions.extension_blocks[block_index].block_processing_control_flags & (1 << 1))
			extblock->set(dtn::data::Block::TRANSMIT_STATUSREPORT_IF_NOT_PROCESSED, true);
		if (spec->extensions.extension_blocks[block_index].block_processing_control_flags & (1 << 2))
			extblock->set(dtn::data::Block::DELETE_BUNDLE_IF_NOT_PROCESSED, true);
		if (spec->extensions.extension_blocks[block_index].block_processing_control_flags & (1 << 3)) //set automatically if needed
			; //extblock->set(dtn::data::Block::LAST_BLOCK, true); 
		if (spec->extensions.extension_blocks[block_index].block_processing_control_flags & (1 << 4))
			extblock->set(dtn::data::Block::DISCARD_IF_NOT_PROCESSED, true);
		if (spec->extensions.extension_blocks[block_index].block_processing_control_flags & (1 << 5))
			extblock->set(dtn::data::Block::FORWARDED_WITHOUT_PROCESSED, true);
		if (spec->extensions.extension_blocks[block_index].block_processing_control_flags & (1 << 6))
			extblock->set(dtn::data::Block::BLOCK_CONTAINS_EIDS, true);
		
		std::istringstream memin;
		memin.rdbuf()->pubsetbuf(spec->extensions.extension_blocks[block_index].block_data.block_type_specific_data,
		                         spec->extensions.extension_blocks[block_index].block_data.block_type_specific_data_len);
		
		extblock->deserialize(memin, spec->extensions.extension_blocks[block_index].block_data.block_type_specific_data_len);
	}
	
	//-------- al_types_bundle_payload* payload --------
	
	if (payload->location == BP_PAYLOAD_MEM) {
		if (payload->buf.buf_val == NULL)
			return ibrHandle->error = BP_ENULLPNTR;
		
		ibrcommon::BLOB::Reference blobRef = ibrcommon::BLOB::create();
		
		blobRef.iostream()->rdbuf()->pubsetbuf(payload->buf.buf_val, payload->buf.buf_len);
		
		bundle.push_back(blobRef);
		
	} else if (payload->location == BP_PAYLOAD_FILE || payload->location == BP_PAYLOAD_TEMP_FILE) {
		if (payload->filename.filename_val == NULL)
			return ibrHandle->error = BP_ENULLPNTR;
		
		try {
			ibrcommon::BLOB::Reference blobRef = ibrcommon::BLOB::open(
					ibrcommon::File(std::string(payload->filename.filename_val)));
			
			bundle.push_back(blobRef);
		
		} catch (ibrcommon::CanNotOpenFileException& e) {
#ifdef PRINT_EXCEPTIONS
			std::cerr << e.what() << std::endl;
			//filename
#endif
			return ibrHandle->error = BP_ESEND;
		}
		
	} else 
		return ibrHandle->error = BP_EINVAL;
	
	//NOTE al_types_bundle_status_report* payload->status_report is not used because it should reflect
	//the content of the payload itself, since "Administrative records are standard application data units" (RFC5050)
	
	ibrHandle->client->operator<<(bundle);	
#if 0
	clock_t end = clock();
	double elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
	std::cerr << "Elapsed time between bundle creation and sending: " << elapsed << std::endl;
	//up to 0.2 seconds for 50 MB
#endif

	//return an almost certainly correct timestamp and a fake yet consistent sequence number
	// id->creation_ts.secs = dtn::utils::Clock::getTime().get();
	dtn::data::Bundle bundle2;
	
	return ibrHandle->error = BP_SUCCESS;
}

al_bp_error_t bp_ibr_recv(al_types_handle handle,
                          al_types_bundle_spec* spec, //nonnull
                          al_types_bundle_payload_location location,
                          al_types_bundle_payload* payload, //nonnull
                          al_types_timeout timeout) //milliseconds
{
	IbrHandle* ibrHandle = (IbrHandle*) handle;
	if (ibrHandle == NULL)
		return BP_ENULLPNTR;
	if (ibrHandle->client == NULL)
		return ibrHandle->error = BP_ENULLPNTR;
	
	dtn::data::Bundle bundle;
	
	try {
		//NOTE unfortunately, dtn::api::Client::getBundle only accepts a timeout in seconds
		//and a zero timeout means an infinite wait, so the shortest possible timeout is one second!
		
		if (timeout == 0) //no wait
			timeout = 1; //(1+999)/1000 = 1s
		else if (timeout == (uint32_t)-1) //infinite wait
			timeout = 0; //(0+999)/1000 = 0 -> inf
		bundle = ibrHandle->client->getBundle((timeout + 999) / 1000); //to seconds and ceiling
	
	} catch (dtn::api::ConnectionTimeoutException& e) {
#ifdef PRINT_EXCEPTIONS
		std::cerr << e.what() << std::endl;
		//"Timeout."
#endif
		return ibrHandle->error = BP_ETIMEOUT;
		
	} catch (dtn::api::ConnectionAbortedException& e) { // not experienced yet
#ifdef PRINT_EXCEPTIONS
		std::cerr << e.what() << std::endl;
		//"Aborted."
#endif
		return ibrHandle->error = BP_ERECVINT;
		
	} catch (dtn::api::ConnectionException& e) { // not experienced yet
#ifdef PRINT_EXCEPTIONS
		std::cerr << e.what() << std::endl;
		//"A connection error occurred."
#endif
		return ibrHandle->error = BP_ERECV;
	}
	
	//-------- al_types_bundle_spec* spec --------
	
	memset(spec, 0, sizeof(*spec));
	
	memcpy(spec->source.uri, bundle.source.getString().c_str(), AL_TYPES_MAX_EID_LENGTH);
	spec->source.uri[AL_TYPES_MAX_EID_LENGTH - 1] = '\0'; //ensures the string terminator
	
	memcpy(spec->destination.uri, bundle.destination.getString().c_str(), AL_TYPES_MAX_EID_LENGTH);
	spec->destination.uri[AL_TYPES_MAX_EID_LENGTH - 1] = '\0';
	
	//I assume spec->replyto is the report-to EID described in RFC5050...
	memcpy(spec->report_to.uri, bundle.reportto.getString().c_str(), AL_TYPES_MAX_EID_LENGTH);
	spec->report_to.uri[AL_TYPES_MAX_EID_LENGTH - 1] = '\0';
	//...otherwise use
	// strcpy(spec->replyto.uri, "dtn:none"); //as per al_bp_ion.c	
	
	spec->cardinal = (al_types_bundle_priority_enum) bundle.getPriority();
	spec->ecos.ordinal = 0; //as per al_bp_dtn_conversions.c, ION-related
	
	uint32_t dopts = BP_DOPTS_NONE;
	if (bundle.get(dtn::data::PrimaryBlock::CUSTODY_REQUESTED))
		dopts |= BP_DOPTS_CUSTODY;
	if (bundle.get(dtn::data::PrimaryBlock::REQUEST_REPORT_OF_BUNDLE_DELIVERY))
		dopts |= BP_DOPTS_DELIVERY_BSR;
	if (bundle.get(dtn::data::PrimaryBlock::REQUEST_REPORT_OF_BUNDLE_RECEPTION))
		dopts |= BP_DOPTS_RECEPTION_BSR;
	if (bundle.get(dtn::data::PrimaryBlock::REQUEST_REPORT_OF_BUNDLE_FORWARDING))
		dopts |= BP_DOPTS_FORWARD_BSR;
	if (bundle.get(dtn::data::PrimaryBlock::REQUEST_REPORT_OF_CUSTODY_ACCEPTANCE))
		dopts |= BP_DOPTS_CUSTODY_BSR;
	if (bundle.get(dtn::data::PrimaryBlock::REQUEST_REPORT_OF_BUNDLE_DELETION))
		dopts |= BP_DOPTS_DELETION_BSR;
	if (bundle.get(dtn::data::PrimaryBlock::DESTINATION_IS_SINGLETON))
		dopts |= BP_DOPTS_SINGLETON_DEST;
	if (bundle.get(dtn::data::PrimaryBlock::ACKOFAPP_REQUESTED)) //new
		dopts |= BP_DOPTS_ACK_REQUESTED_BY_APPLICATION;
	if (false) //no equivalent in RFC5050 or IBR-DTN's APIs
		dopts |= BP_DOPTS_MULTINODE_DEST;
	if (bundle.get(dtn::data::PrimaryBlock::DONT_FRAGMENT))
		dopts |= BP_DOPTS_DO_NOT_FRAGMENT;
	spec->bundle_proc_ctrl_flags = (al_types_bundle_processing_control_flags) dopts;
	
	spec->lifetime = bundle.lifetime.get();
	
	spec->creation_ts.time = bundle.timestamp.get();
	spec->creation_ts.seqno = bundle.sequencenumber.get();
	
	spec->delivery_regid = 0; //again, no regid support in IBR-DTN's API
	
	spec->ecos.unreliable = 0; //ION-related
	spec->ecos.critical = 0; //ION-related
	spec->ecos.flow_label = 0; //ION-related
	
	//-------- spec->blocks --------
	
	spec->extensions.extension_blocks= 0;
	for (dtn::data::Bundle::iterator block_ptr_it = bundle.begin(); block_ptr_it != bundle.end(); ++block_ptr_it) {
		// *block_ptr_it is a refcnt_ptr
		if ((*block_ptr_it)->getType() != PayloadBlock::BLOCK_TYPE) {			
			++ spec->extensions.extension_number;
		}
	}
	
	spec->extensions.extension_blocks = new al_types_extension_block[spec->extensions.extension_number];
	
	//NOTE blocks with type 2, 3 and 4 give different kinds of problems,
	//probably because they have special meanings and thus are treated unusually
	
	uint32_t block_index = 0;
	for (dtn::data::Bundle::iterator block_ptr_it = bundle.begin(); block_ptr_it != bundle.end(); ++block_ptr_it) {
		if ((*block_ptr_it)->getType() != PayloadBlock::BLOCK_TYPE) {
		
			spec->extensions.extension_blocks[block_index].block_type_code = (*block_ptr_it)->getType();
			spec->extensions.extension_blocks[block_index].block_processing_control_flags = (*block_ptr_it)->getProcessingFlags().get();
			
			spec->extensions.extension_blocks[block_index].block_data.block_type_specific_data_len = (*block_ptr_it)->getLength();
			spec->extensions.extension_blocks[block_index].block_data.block_type_specific_data = new char[spec->extensions.extension_blocks[block_index].block_data.block_type_specific_data_len];
			
			std::ostringstream memout;
			memout.rdbuf()->pubsetbuf(spec->extensions.extension_blocks[block_index].block_data.block_type_specific_data,
			                          spec->extensions.extension_blocks[block_index].block_data.block_type_specific_data_len);
			
			size_t length = 0;
			(*block_ptr_it)->serialize(memout, length); 
			//might (PayloadBlock) or might not (ExtensionBlock) increase the second arg (length)
			
			++block_index;
		}
	}
	
	spec->extensions.extension_number = 0;
	spec->extensions.extension_blocks = NULL;
	
	//-------- al_types_bundle_payload* payload --------
	
	memset(payload, 0, sizeof(*payload));
	
	for (dtn::data::Bundle::iterator block_ptr_it = bundle.begin(); block_ptr_it != bundle.end(); ++block_ptr_it) {
		if ((*block_ptr_it)->getType() == PayloadBlock::BLOCK_TYPE) {
			
			
			if (location == BP_PAYLOAD_MEM) {
				payload->location = BP_PAYLOAD_MEM;
				
				memset(&payload->filename, 0, sizeof(payload->filename));
				
				payload->buf.buf_len = (*block_ptr_it)->getLength();
				payload->buf.buf_val = new char[payload->buf.buf_len];
				
				std::ostringstream memout;
				memout.rdbuf()->pubsetbuf(payload->buf.buf_val,
				                          payload->buf.buf_len);
				
				size_t length;
				(*block_ptr_it)->serialize(memout, length);
			
				payload->buf.buf_crc = 0;
				//NOTE is payload->buf.buf_crc actually wanted? besides, it was clearly added later
				
			} else if (location == BP_PAYLOAD_FILE || location == BP_PAYLOAD_TEMP_FILE) {
				payload->location = BP_PAYLOAD_FILE;
				
				memset(&payload->buf, 0, sizeof(payload->buf));
				
				std::stringstream filename;
				uint32_t i = 0;
				while (true) {
					filename.str("");
					filename << "/tmp/ibrdtn_payload_" << i;
					if (access(filename.str().c_str(), F_OK) != 0)
						break;
					++i;
				}
				payload->filename.filename_len = filename.str().length();
				payload->filename.filename_val = new char[filename.str().length() + 1];
				strcpy(payload->filename.filename_val, filename.str().c_str());
				
				ofstream fileout(payload->filename.filename_val);
				
				size_t length = 0;
				(*block_ptr_it)->serialize(fileout, length);
				
				fileout.close();		
				
			} else {
				bp_ibr_free_extensions(spec);
				return ibrHandle->error = BP_EINVAL;
			}
			
			//-------- al_types_bundle_status_report* payload->status_report --------
			
			if (bundle.get(dtn::data::PrimaryBlock::APPDATA_IS_ADMRECORD)) {
				StatusReportBlock srblock;
				
				try {
					srblock.read(*((dtn::data::PayloadBlock*) &(**block_ptr_it))); //downcast from Block to PayloadBlock (trust me)
					
					payload->status_report = new al_types_bundle_status_report;
					memset(payload->status_report, 0, sizeof(*payload->status_report));
					
					memcpy(payload->status_report->bundle_x_id.source.uri, srblock.bundleid.source.getString().c_str(), AL_TYPES_MAX_EID_LENGTH);
					payload->status_report->bundle_x_id.source.uri[AL_TYPES_MAX_EID_LENGTH - 1] = '\0';
					
					payload->status_report->bundle_x_id.creation_ts.time = srblock.bundleid.timestamp.get();
					payload->status_report->bundle_x_id.creation_ts.seqno = srblock.bundleid.sequencenumber.get();
					
					if (srblock.bundleid.isFragment()) {
						payload->status_report->bundle_x_id.frag_offset = srblock.bundleid.fragmentoffset.get();
						
						//it would be the original length if we were in the Primary Block;
						//in an Administrative Record it's the fragment length
						payload->status_report->bundle_x_id.frag_length = srblock.bundleid.getPayloadLength();
					}
					
					payload->status_report->flags = (al_types_status_report_flags) srblock.status;
					payload->status_report->reason = (al_types_status_report_reason) srblock.reasoncode;
					
					if (payload->status_report->flags & BP_STATUS_RECEIVED)
						payload->status_report->reception_ts = srblock.timeof_receipt.getTimestamp().get();
					if (payload->status_report->flags & BP_STATUS_CUSTODY_ACCEPTED)
						payload->status_report->custody_ts = srblock.timeof_custodyaccept.getTimestamp().get();
					if (payload->status_report->flags & BP_STATUS_FORWARDED)
						payload->status_report->forwarding_ts = srblock.timeof_forwarding.getTimestamp().get();
					if (payload->status_report->flags & BP_STATUS_DELIVERED)
						payload->status_report->delivery_ts = srblock.timeof_delivery.getTimestamp().get();
					if (payload->status_report->flags & BP_STATUS_DELETED)
						payload->status_report->deletion_ts  = srblock.timeof_deletion.getTimestamp().get();
				//	if (payload->status_report->flags & BP_STATUS_ACKED_BY_APP)
				//		payload->status_report->ack_by_app_ts = 0; //not RFC5050 compliant and not supported by IBR-DTN
					
				} catch (dtn::data::AdministrativeBlock::WrongRecordException& e) { //this AdministrativeBlock is a CustodySignalBlock
#ifdef PRINT_EXCEPTIONS
					std::cerr << e.what() << std::endl;
					//"This administrative block is not of the expected type."
#endif
				}
				
			} else {
				payload->status_report = NULL;
			}
			
			
			//"At most one of the blocks in the sequence may be a payload block" (RFC5050)
			break;
		}
	}
	
	return ibrHandle->error = BP_SUCCESS;
}

//it isn't clear whether al_bp_unregister is always called before or after al_bp_close, 
//and sometimes it appears not to be called at all,
//even though one would expect the former to be always called and always before the latter, 
//and the bp_ibr code follows that thought;
//thus, we modified bp_ibr_close to call bp_ibr_unregister if this wasn't called
//(and apparently bp_ibr_unregister doesn't throw any error if called (immediately?) after bp_ibr_close,
//but keep in mind that in such case dereferencing ibrHandle with 'ibrHandle->client' is undefined behavior,
//because ibrHandle's pointed data has already been deleted by bp_ibr_close with 'delete ibrHandle')

al_bp_error_t bp_ibr_unregister(al_types_handle handle, 
                                al_types_reg_id regid, //unused
                                al_types_endpoint_id eid) //unused
{
	IbrHandle* ibrHandle = (IbrHandle*) handle;
	if (ibrHandle == NULL)
		return BP_ENULLPNTR;
	
	if (ibrHandle->client == NULL)
		return ibrHandle->error = BP_SUCCESS;
	
	dtn::api::Client* client = ibrHandle->client;

	// client->abort(); //Aborts blocking calls of getBundle()
	client->close();
	
	// client->abort();
	//if Client's destructor is called while it is waiting for a bundle, 
	//for instance through a signal handler, it blocks on _receiver.join();
	//abort() should "Aborts blocking calls of getBundle()"
	
	delete client;
	
	// operator delete (client);
	//NOTE it turns out that abort() isn't enough: let's deallocate client without calling the constructor;
	//it's a really bad practice, but it works for now
	
	ibrHandle->client = NULL;
	
	return ibrHandle->error = BP_SUCCESS;
}

al_bp_error_t bp_ibr_close(al_types_handle handle)
{
	IbrHandle* ibrHandle = (IbrHandle*) handle;
	if (ibrHandle == NULL)
		return BP_ENULLPNTR;
	
	if (ibrHandle->client != NULL)
		bp_ibr_unregister(handle, al_types_reg_id(), al_types_endpoint_id());
	
	if (ibrHandle->stream == NULL)
		return ibrHandle->error = BP_SUCCESS;
	
	ibrcommon::socketstream* stream = ibrHandle->stream;
	stream->close();
	
	delete stream;
	
	// delete stream;
	//NOTE *sometimes*, when bp_ibr_close is called from a signal handler, it causes the error
	//"pure virtual method called   terminate called without an active exception   Aborted (core dumped)"
	//after all, it's not so strange for the asynchronicity of a signal handler to cause problems
	
	ibrHandle->stream = NULL;
	
	delete ibrHandle->daemonIP;
	ibrHandle->daemonIP = NULL;
	ibrHandle->daemonPort = 0;
	
	delete ibrHandle;
	
	return BP_SUCCESS;
}

/* Utility Functions */

al_bp_error_t bp_ibr_parse_eid_string(al_types_endpoint_id* eid,
                                      const char* str)
{
	if (str == NULL)
		return BP_ENULLPNTR;
	if (eid == NULL)
		return BP_ENULLPNTR;
	
	EID ibreid(str);
	//if str is malformed, a "dtn:none" EID is constructed
	
	//copy readonly str and trim it to compare with "dtn:none"
	char str_copy[strlen(str) + 1];
	strcpy(str_copy, str);
	char* trimmed_str = str_copy;
	while (*trimmed_str != '\0' && *trimmed_str <= ' ')
		++trimmed_str;
	while (*trimmed_str != '\0' && trimmed_str[strlen(trimmed_str) - 1] <= ' ')
		trimmed_str[strlen(trimmed_str) - 1] = '\0';
	
	if (strcmp(trimmed_str, "dtn:none") != 0 && strcmp(ibreid.getString().c_str(), "dtn:none") == 0)
		return BP_EPARSEEID;
	
	if (ibreid.getString().length() >= AL_TYPES_MAX_EID_LENGTH)
		return BP_ESIZE;
	
	strcpy(eid->uri, ibreid.getString().c_str());
	
	return BP_SUCCESS;
}

void bp_ibr_copy_eid(al_types_endpoint_id* dst,
                     al_types_endpoint_id* src)
{
	//I don't understand why this function is duplicated for each DTN implementation,
	//nor the need for the conversions in the other two implementations
	
	if (dst == NULL || src == NULL)
		return;
	
	strncpy(dst->uri, src->uri, AL_TYPES_MAX_EID_LENGTH);
	dst->uri[AL_TYPES_MAX_EID_LENGTH - 1] = '\0'; //src->uri might not be terminated and be longer than AL_TYPES_MAX_EID_LENGTH - 1
}

void bp_ibr_free_extensions(al_types_bundle_spec* spec)
{
	if (spec == NULL)
		return;
	if (spec->extensions.extension_blocks == NULL) {
		spec->extensions.extension_number = 0;
		return;
	}

	for (uint32_t i = 0; i < spec->extensions.extension_number; ++i) {
		delete spec->extensions.extension_blocks[i].block_data.block_type_specific_data;
	}

	delete spec->extensions.extension_blocks;
    spec->extensions.extension_blocks = NULL;
    spec->extensions.extension_number = 0;
}


al_bp_error_t bp_ibr_set_payload(al_types_bundle_payload* payload, //nonnull
                                 al_types_bundle_payload_location location,
                                 char* val, int len)
{
	memset(payload, 0, sizeof(*payload));
	
	payload->location = location;
	
	// if (val == NULL)
		// return BP_ENULLPNTR;
	
	if (location == BP_PAYLOAD_MEM) {
		payload->buf.buf_len = len;
		payload->buf.buf_val = val;
		
	} else if (location == BP_PAYLOAD_FILE || location == BP_PAYLOAD_TEMP_FILE) {
		payload->filename.filename_len = len;
		payload->filename.filename_val = val;
		
	} else
		return BP_EINVAL;
		
	return BP_SUCCESS;
}

void bp_ibr_free_payload(al_types_bundle_payload* payload) {
	if (payload == NULL)
		return;
	
	if (payload->filename.filename_val != NULL) {
		if (payload->location == BP_PAYLOAD_FILE || payload->location == BP_PAYLOAD_TEMP_FILE) {
			unlink(payload->filename.filename_val);
		}
		
		// delete payload->filename.filename_val; //causes "Error in `./dtnperf_vIBRDTN': double free or corruption (fasttop)"
		payload->filename.filename_val = NULL;
		payload->filename.filename_len = 0;
	}
	
	if (payload->buf.buf_val != NULL) {
		// delete payload->buf.buf_val; //already freed by DTNperf
		payload->buf.buf_val = NULL;
		payload->buf.buf_len = 0;
		payload->buf.buf_crc = 0;
	}
	
	if (payload->status_report != NULL) {
		delete payload->status_report;
		payload->status_report = NULL;
	}
}

#undef PRINT_EXCEPTIONS

#else /* IBRDTN_IMPLEMENTATION */

al_bp_error_t bp_ibr_errno(al_types_handle handle)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ibr_open(al_types_handle* handle_p)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ibr_open_with_IP(const char* daemon_api_IP, 
                                  int daemon_api_port, 
                                  al_types_handle* handle_p)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ibr_build_local_eid(al_types_handle handle,
                                     al_types_endpoint_id* local_eid,
                                     const char* service_tag,
                                     al_types_scheme type)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ibr_register(al_types_handle handle,
                              al_types_reg_info* reginfo,
                              al_types_reg_id* newregid)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ibr_find_registration(al_types_handle handle,
                                       al_types_endpoint_id* eid,
                                       al_types_reg_id* newregid)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ibr_send(al_types_handle handle,
                          al_types_reg_id regid,
                          al_types_bundle_spec* spec,
                          al_types_bundle_payload* payload)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ibr_recv(al_types_handle handle,
                          al_types_bundle_spec* spec,
                          al_types_bundle_payload_location location,
                          al_types_bundle_payload* payload,
                          al_types_timeout timeout)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ibr_unregister(al_types_handle handle, 
                                al_types_reg_id regid,
                                al_types_endpoint_id eid)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ibr_close(al_types_handle handle)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ibr_parse_eid_string(al_types_endpoint_id* eid,
                                      const char* str)
{
	return BP_ENOTIMPL;
}

void bp_ibr_copy_eid(al_types_endpoint_id* dst,
                     al_types_endpoint_id* src) {}

void bp_ibr_free_extensions(al_types_bundle_spec* spec) {}

al_bp_error_t bp_ibr_set_payload(al_types_bundle_payload* payload,
                                 al_types_bundle_payload_location location,
                                 char* val, int len)
{
	return BP_ENOTIMPL;
}

void bp_ibr_free_payload(al_types_bundle_payload* payload) {}

#endif /* IBRDTN_IMPLEMENTATION */

boolean_t bp_ibr_is_running()
{
	char find_ibrdtnd1[] = "ps axe | grep /usr/sbin/dtnd | grep -v grep > /dev/null"; //in constrast with dtn2 daemon in /usr/bin/dtnd
	char find_ibrdtnd2[] = "ps axe | grep /usr/local/sbin/dtnd | grep -v grep > /dev/null"; //when installed from source
	if(system(find_ibrdtnd1) == 0 || system(find_ibrdtnd2) == 0) {
		return TRUE;
	}
	return FALSE;
}