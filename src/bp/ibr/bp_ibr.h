/** \file bp_ibr.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .cpp file.
 *
 *  \par Copyright
 *  	Copyright (c) 2016, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Davide Pallotti, davide.pallotti@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/16 | D. Pallotti	   |  Implementation contribution.
 *
 *
 */

#ifndef BP_IBR_H_
#define BP_IBR_H_

#include "al_types.h"
#include "al_bp.h"

#ifdef __cplusplus
/* prevents the C++ compiler from performing name mangling on the functions defined in bp_ibr.cpp, 
   so that they can be successfully linked to the existing C code */
extern "C" {
#endif

al_bp_error_t bp_ibr_errno(al_types_handle handle);

boolean_t bp_ibr_is_running();

al_bp_error_t bp_ibr_open(al_types_handle* handle_p);

al_bp_error_t bp_ibr_open_with_IP(const char* daemon_api_IP, 
                                  int daemon_api_port, 
                                  al_types_handle* handle_p);

al_bp_error_t bp_ibr_build_local_eid(al_types_handle handle,
                                     al_types_endpoint_id* local_eid,
                                     const char* service_tag,
                                     al_types_scheme type);

al_bp_error_t bp_ibr_register(al_types_handle handle,
                              al_types_reg_info* reginfo,
                              al_types_reg_id* newregid);

al_bp_error_t bp_ibr_find_registration(al_types_handle handle,
                                       al_types_endpoint_id* eid,
                                       al_types_reg_id* newregid);

al_bp_error_t bp_ibr_send(al_types_handle handle,
                          al_types_reg_id regid,
                          al_types_bundle_spec* spec,
                          al_types_bundle_payload* payload);

al_bp_error_t bp_ibr_recv(al_types_handle handle,
                          al_types_bundle_spec* spec,
                          al_types_bundle_payload_location location,
                          al_types_bundle_payload* payload,
                          al_types_timeout timeout);

al_bp_error_t bp_ibr_unregister(al_types_handle handle, 
                                al_types_reg_id regid,
                                al_types_endpoint_id eid);

al_bp_error_t bp_ibr_close(al_types_handle handle);

/* Utility Functions */

al_bp_error_t bp_ibr_parse_eid_string(al_types_endpoint_id* eid,
                                      const char* str);

void bp_ibr_copy_eid(al_types_endpoint_id* dst,
                     al_types_endpoint_id* src);

void bp_ibr_free_extension_blocks(al_types_bundle_spec* spec);

void bp_ibr_free_extensions(al_types_bundle_spec* spec);

al_bp_error_t bp_ibr_set_payload(al_types_bundle_payload* payload,
                                 al_types_bundle_payload_location location,
                                 char* val, int len);

void bp_ibr_free_payload(al_types_bundle_payload* payload);

#ifdef __cplusplus
} //extern "C" {
#endif

#endif /* BP_IBR_H_ */
