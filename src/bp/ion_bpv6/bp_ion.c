/** \file bp_ion.c
 *
 *  \brief  This file contains the functions interfacing ION (bpv6)
 *
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *  01/01/20 | A. Bisacchi	   |  Implementation contribution.
 *
 */


#include "bp_ion.h"

#include "bp_ion_conversions.h"
/**
 * bp_open_source() is present only in ION >= 3.3.0
 * If using ION < 3.3.0, please comment the following line.
 */
#define BP_OPEN_SOURCE

/*
 * if there is the ION implementation on the
 * machine the API are implemented
 */
#ifdef ION_IMPLEMENTATION

/* It's for private function */
#include <ion.h>
int albp_parseAdminRecord(int *adminRecordType, BpStatusRpt *rpt,BpCtSignal *csig, void **otherPtr, Object payload);
int	albp_parseStatusRpt(BpStatusRpt *rpt, unsigned char *cursor,int unparsedBytes, int isFragment);
/*********************************/

al_bp_error_t bp_ion_attach(){
	int result;
	result = bp_attach();
	if(result == -1)
	{
		return BP_EATTACH;
	}
	return BP_SUCCESS;
}

al_bp_error_t bp_ion_open_with_IP(char * daemon_api_IP, int daemon_api_port, al_types_handle * handle)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_errno(al_types_handle handle)
{
	//printf("%s\n",system_error_msg());
	return BP_SUCCESS;
}

al_bp_error_t bp_ion_build_local_eid(al_types_endpoint_id* local_eid,
								const char* service_tag,
								al_types_scheme  type)
{
	char * eidString, * hostname;
	int result;
	VScheme * scheme;
	PsmAddress psmAddress;
	eidString = (char *)malloc(sizeof(char)*AL_TYPES_MAX_EID_LENGTH);
	
	if (eidString == NULL) {
		return BP_EBUILDEID;
	}

	if(type == IPN_SCHEME)
	{
		findScheme(IPNSCHEMENAME,&scheme,&psmAddress);
		if(psmAddress == 0)
		{
			/*Unknow scheme*/
			result = addScheme(IPNSCHEMENAME,"ipnfw","ipnadmin");
			if(result == 0) {
				free(eidString);
				return BP_EBUILDEID;
			}
		}
		long int service_num = strtol(service_tag,NULL,10);
		sprintf(eidString, "%s:%lu.%lu", IPNSCHEMENAME, (unsigned long int) getOwnNodeNbr(), service_num);
		(*local_eid) = bp_from_ion_endpoint_id(eidString);
	}
	else if(type == DTN_SCHEME)
	{
		findScheme(DTNSCHEMENAME,&scheme,&psmAddress);
		if(psmAddress == 0)
		{
			/*Unknow scheme*/
			result = addScheme(DTNSCHEMENAME,"dtn2fw","dtn2admin");
			if(result == 0) {
				free(eidString);
				return BP_EBUILDEID;
			}
		}

		hostname = (char *)malloc(sizeof(char)*100);
		if (hostname == NULL) {
			free(eidString);
			return BP_EBUILDEID;
		}
		result = gethostname(hostname, 100);
		if(result == -1) {
			free(eidString);
			free(hostname);
			return BP_EBUILDEID;
		}
		sprintf(eidString,"%s://%s.dtn/%s",DTNSCHEMENAME,hostname,service_tag);
		(*local_eid) = bp_from_ion_endpoint_id(eidString);
		free(hostname);
	}
	else {
		free(eidString);
		return BP_EBUILDEID;
	}
	//Free resource
	free(eidString);
	return BP_SUCCESS;
}

al_bp_error_t bp_ion_register(al_types_handle * handle,
						al_types_reg_info* reginfo,
						al_types_reg_id* newregid)
{
	int result;
	char * eid;
	bp_ion_handle  ion_handle;

	//ion_handle = bp_to_ion_handle(*handle); //this is useless because *handle is meaningless
	//initialization of ion_handle
	ion_handle = (bp_ion_handle ) malloc(sizeof(struct bp_ion_handle_st));
	if (ion_handle == NULL) {
		return BP_EREG;
	}
	
	ion_handle->recv = (BpSAP *) malloc(sizeof(BpSAP));
	if (ion_handle->recv == NULL) {
		free(ion_handle);
		return BP_EREG;
	}
	
	ion_handle->source = (BpSAP *) malloc(sizeof(BpSAP));
	if (ion_handle->source == NULL) {
		free(ion_handle->recv);
		free(ion_handle);
		return BP_EREG;
	}
	//end of initialization

	eid = bp_to_ion_endpoint_id(reginfo->endpoint);
/*	switch(reginfo->flags)
	{
		case BP_REG_DEFER: rule = EnqueueBundle;break;
		case BP_REG_DROP: rule = DiscardBundle;break;
		default: return BP_EINVAL;
	}*/
	//If the eid is not registrated then it will be registrated
	if(bp_ion_find_registration(*handle,&(reginfo->endpoint),newregid) ==  BP_ENOTFOUND)
	{
		result = addEndpoint(eid, DiscardBundle ,NULL);
		if(result == 0) {
			free(eid);
			return BP_EREG;
		}
	}
#ifdef BP_OPEN_SOURCE
	//result = bp_open(eid, (ion_handle->source));
	result = bp_open_source(eid, (ion_handle->source), 1);
	if(result == -1) {
		free(eid);
		return BP_EREG;
	}
#endif
	result = bp_open(eid, (ion_handle->recv));
	if(result == -1) {
		free(eid);
		return BP_EREG;
	}
	//Free resource
	free(eid);
	(*handle) = bp_from_ion_handle(ion_handle);
	return BP_SUCCESS;
}

al_bp_error_t bp_ion_find_registration(al_types_handle handle,
						al_types_endpoint_id * eid,
						al_types_reg_id * newregid)
{
	char * schemeName, * endpoint;
	VEndpoint * veid;
	PsmAddress psmAddress;
	MetaEid metaEid;
	VScheme * vscheme;
	endpoint = bp_to_ion_endpoint_id((*eid));
	schemeName = (char *)malloc(sizeof(char)*4);
	if (schemeName == NULL) {
		free(endpoint);
		return BP_EPARSEEID;
	}
	
	if(strncmp(endpoint,"ipn",3) == 0)
		strncpy(schemeName,"ipn",4);
	else
		strncpy(schemeName,"dtn",4);
	if(parseEidString(endpoint,&metaEid,&vscheme,&psmAddress) == 0)
	{
		free(schemeName);
		free(endpoint);
		return BP_EPARSEEID;
	}
	findEndpoint(schemeName,metaEid.nss,vscheme,&veid,&psmAddress);
	if(psmAddress == 0)
	{
		free(schemeName);
		free(endpoint);
		return BP_ENOTFOUND;
	}
	if (sm_TaskExists(veid->appPid))
	{
		if (veid->appPid != -1) {
			free(schemeName);
			free(endpoint);
			return BP_EBUSY;
		}
	}
	//Free resource
	free(schemeName);
	free(endpoint);

	return BP_SUCCESS;
}

al_bp_error_t bp_ion_unregister(al_types_endpoint_id eid)
{
	char * ion_eid = bp_to_ion_endpoint_id(eid);
	int result = removeEndpoint(ion_eid);
	free(ion_eid);
	if(result != 1)
	{
		return BP_EUNREG;
	}
	else
	{
		return BP_SUCCESS;
	}

}

al_bp_error_t bp_ion_send(al_types_handle handle,
					al_types_reg_id regid,
					al_types_bundle_spec* spec,
					al_types_bundle_payload* payload)
{
	bp_ion_handle  ion_handle = bp_to_ion_handle(handle);
#ifdef BP_OPEN_SOURCE
	// using SAP obtained exclusively for sending bundles
	BpSAP * bpSap = ion_handle->source;
#else
	// using unique SAP for sending/receiving bundles
	BpSAP * bpSap = ion_handle->recv;
#endif
	char * destEid = bp_to_ion_endpoint_id(spec->destination);
	char * reportEid = NULL;
	char * tokenClassOfService;
	int result, tmpCustody, tmpPriority, lifespan, ackRequested;
	unsigned char srrFlags;
	BpCustodySwitch custodySwitch;
	BpAncillaryData extendedCOS = { 0, 0, 0 };
	/* Set option bundle */
	reportEid = bp_to_ion_endpoint_id(spec->report_to);
	lifespan = (int) spec->lifetime;
	custodySwitch = NoCustodyRequested;
	srrFlags = bp_to_ion_bundle_srrFlags(spec->bundle_proc_ctrl_flags);
	ackRequested = 0;
	/* Create String for parse class of service */	
	if(spec->bundle_proc_ctrl_flags & BP_DOPTS_CUSTODY)
	{
			tmpCustody = 1;
	}
	else
	{
			tmpCustody = 0;
	}
	tmpPriority = bp_to_ion_bundle_priority(spec->cardinal);
	if(tmpPriority == -1)
		return BP_EINVAL;
	tokenClassOfService = (char *)malloc(sizeof(char)*255);
	sprintf(tokenClassOfService,"%1u.%1u.%lu.%1u.%1u.%lu", tmpCustody, tmpPriority, (unsigned long) spec->ecos.ordinal,
			spec->ecos.unreliable==TRUE?1:0, spec->ecos.critical==TRUE?1:0, (unsigned long) spec->ecos.flow_label);
	
	//printf("COS is: %s\n", tokenClassOfService);
	if (spec->extensions.extension_number > 0)
		{
		//Metadata conversion function to ION has been renamed
		//result = al_ion_metadata(spec->extensions.extension_number, spec->extensions.extension_blocks, &extendedCOS);
		result = bp_to_ion_extensions(spec->extensions.extension_number, spec->extensions.extension_blocks, &extendedCOS);
			if(result == 0) {
				free(destEid);
				free(reportEid);
				free(tokenClassOfService);
				return BP_EINVAL;
			}
		}

	result = bp_parse_quality_of_service(tokenClassOfService,&extendedCOS,&custodySwitch,&tmpPriority);
	if(result == 0) {
		free(destEid);
		free(reportEid);
		free(tokenClassOfService);
		return BP_EINVAL;
	}
	Payload ion_payload = bp_to_ion_bundle_payload((*payload), tmpPriority, extendedCOS);
	Object adu = ion_payload.content;
	Object newBundleObj;

	/* Send Bundle*/
	result = bp_send(*bpSap,destEid,reportEid,lifespan,tmpPriority,
			custodySwitch,srrFlags,ackRequested,&extendedCOS,adu,&newBundleObj);
	if(result == 0) {
		free(destEid);
		free(reportEid);
		free(tokenClassOfService);
		return BP_ENOSPACE;
	}
	if(result == -1) {
		free(destEid);
		free(reportEid);
		free(tokenClassOfService);
		return BP_ESEND;
	}

	/* Set Id Bundle Sent*/
//dz debug --- It is not safe to use the newBundleObj "address" to access SDR because it may
//dz debug --- have been released (or worse: reallocated) before bp_send returns. ION 3.3 should 
//dz debug --- have a new feature bp_open_source() which will preserve the sent bundle until
//dz debug --- it is released or expires. I am commenting out retrieving the dictionary because
//dz debug --- that can lead to allocating 4GB of memory if the newBundleObj data was overwritten.
//dz debug --- This should be updated after the new feature is available. 
//dz debug
	Bundle bundleION;
	Sdr bpSdr = bp_get_sdr();
	sdr_begin_xn(bpSdr);
	sdr_read(bpSdr,(char*)&bundleION,(SdrAddress) newBundleObj,sizeof(Bundle));
	sdr_end_xn(bpSdr);
	char * tmpEidSource;
	char * dictionary = retrieveDictionary(&bundleION);
	printEid(&(bundleION.id.source),dictionary,&tmpEidSource);
//dz debug             --- Note that calling retrieveDictionary without releasing it results in
//dz debug             --- an SDR "memory leak"
#ifdef BP_OPEN_SOURCE
	spec->source = bp_from_ion_endpoint_id(tmpEidSource);
	bp_release((SdrAddress) newBundleObj);
#else
	spec->source = bp_from_ion_endpoint_id("<TBD>");  //dz debug
#endif

//dz debug --- Note that the following values may not always be valid by the time they are read here
//dz debug --- but most of the time they should be okay and reading invalid values will not cause 
//dz debug --- a crash. Just setting these values to zeroes prevents the client Window option from working
//dz debug --- because ACKs from the server would never match. This compromise should work until invalid
//dz debug --- values fill up the window.
	spec->creation_ts = bp_from_ion_timestamp(bundleION.id.creationTime);
	//id->frag_offset = bundleION.id.fragmentOffset;
	//id->frag_length = bundleION.totalAduLength;

	if(bundleION.id.fragmentOffset< 0){
		printf("fragmentOffset <0");
	}
	handle = bp_from_ion_handle(ion_handle);
	//Free resource
	releaseDictionary(dictionary);
	free(destEid);
	free(reportEid);
	free(tokenClassOfService);
	return BP_SUCCESS;
}

al_bp_error_t bp_ion_recv(al_types_handle handle,
					al_types_bundle_spec* spec,
					al_types_bundle_payload_location location,
					al_types_bundle_payload* payload,
					al_types_timeout timeout)
{
	bp_ion_handle  ion_handle = bp_to_ion_handle(handle);
	// using SAP obtained exclusively for receiving bundles
	// or unique SAP if BP_OPEN_SOURCE is undefined
	BpSAP * bpSap = ion_handle->recv;
	BpDelivery dlv;
	int second_timeout = bp_to_ion_timeout(timeout);
	int result;
	result = bp_receive(*bpSap ,&dlv, second_timeout);
	if(result < 0)
	{
		return BP_ERECV;
	}
	if(dlv.result == BpReceptionTimedOut)
	{
		//printf("\nAL-BP: Result Timeout\n");
		return BP_ETIMEOUT;
	}
	if(dlv.result == BpReceptionInterrupted)
	{
		//printf("\nAL-BP: Reception Interrupted\n");
		return BP_ERECVINT;
	}
	if (dlv.result == BpEndpointStopped) {
		return BP_ERECV;
	}
	/* Set Bundle Spec */
	spec->creation_ts = bp_from_ion_timestamp(dlv.bundleCreationTime);
	spec->source = bp_from_ion_endpoint_id(dlv.bundleSourceEid);
	char * tmp = "dtn:none";
	spec->report_to = bp_from_ion_endpoint_id(tmp);

	//Laura Mazzuca: added support to metadata read
	if (dlv.metadataType!=0) {
			//Mazzuca's conversion function from ION has been renamed
			//ion_al_metadata(dlv, spec);
			bp_from_ion_extensions(dlv, spec);
	}

	/* Payload */
	Sdr bpSdr = bp_get_sdr();
	Payload ion_payload;
	ion_payload.content = dlv.adu;
	ion_payload.length = zco_source_data_length(bpSdr, dlv.adu);
	/* File Name if payload is saved in a file */
	char * filename_base = "/tmp/ion_";
	char * filename = (char *) malloc(sizeof(char)*256);
	//check for existing files
	int i = 0;
	boolean_t stop = FALSE;
	while (!stop)
	{
		sprintf(filename, "%s%d_%d", filename_base, getpid(), i);
		if (access(filename,F_OK) != 0)
			// if filename doesn't exist, exit
			stop = TRUE;
		i++;
	}
	(*payload)  = bp_from_ion_bundle_payload(ion_payload,location,filename);
	free(filename);
	/* Status Report */
	BpStatusRpt statusRpt;
	BpCtSignal ctSignal;
	void * acsptr;
	if(albp_parseAdminRecord(&dlv.adminRecord,&statusRpt,&ctSignal,&acsptr,dlv.adu) == 1)
	{
		al_types_bundle_status_report bp_statusRpt = bp_from_ion_bundle_status_report(statusRpt);
		if(payload->status_report == NULL)
		{
			payload->status_report = (al_types_bundle_status_report *) malloc(sizeof(al_types_bundle_status_report));
		}
		(*payload->status_report) = bp_statusRpt;
	}

	/* Release Delivery */
	bp_release_delivery(&dlv, 1);

	handle = bp_from_ion_handle(ion_handle);

	return BP_SUCCESS;
}

al_bp_error_t bp_ion_close(al_types_handle handle)
{
	bp_ion_handle  ion_handle = bp_to_ion_handle(handle);
	bp_close(*ion_handle->recv);
	bp_close(*ion_handle->source);
	//free ion_handle
	free(ion_handle->recv);
#ifdef BP_OPEN_SOURCE
	free(ion_handle->source);
#endif
	handle = bp_from_ion_handle(ion_handle);
	free(ion_handle);
	return BP_SUCCESS;
}

void bp_ion_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src)
{
	char * ion_dst;
	char * ion_src;
	int length;
	ion_src = bp_to_ion_endpoint_id((*src));
	length = strlen(ion_src)+1;
	ion_dst = (char *)malloc(sizeof(char)*length);
	memcpy(ion_dst,ion_src,length);
	(*dst) = bp_from_ion_endpoint_id(ion_dst);
	free(ion_dst);
	free(ion_src);
}

al_bp_error_t bp_ion_parse_eid_string(al_types_endpoint_id* eid, const char* str)
{
	char * endpoint;
	PsmAddress psmAddress;
	MetaEid metaEid;
	VScheme * vscheme;
	endpoint = (char *) malloc(sizeof(char)*AL_TYPES_MAX_EID_LENGTH);
	memcpy(endpoint,str,strlen(str)+1);
	if(parseEidString(endpoint,&metaEid,&vscheme,&psmAddress) == 0) {
		free(endpoint);
		return BP_EPARSEEID;
	}
	(*eid) = bp_from_ion_endpoint_id((char *)str);
	free(endpoint);
	return BP_SUCCESS;
}

al_bp_error_t bp_ion_set_payload(al_types_bundle_payload* payload,
							al_types_bundle_payload_location location,
							char* val, int len)
{
	memset(payload,0,sizeof(al_types_bundle_payload));
	payload->location = location;
	if(location == BP_PAYLOAD_MEM)
	{
		payload->buf.buf_len = len;
		payload->buf.buf_val= val;
	}
	else
	{
		payload->filename.filename_len = len;
		payload->filename.filename_val = val;
	}
	return BP_SUCCESS;
}

void bp_ion_free_payload(al_types_bundle_payload* payload)
{
	if(payload->status_report != NULL)
	{
		free(payload->status_report);
		payload->status_report = NULL;
	}
	if(payload->location != BP_PAYLOAD_MEM && payload->filename.filename_val != NULL)
	{
		int type = 0;
		Sdr bpSdr = bp_get_sdr();
		sdr_begin_xn(bpSdr);
		Object fileRef = sdr_find(bpSdr, payload->filename.filename_val, &type);
		if(fileRef != 0)
		{
			zco_destroy_file_ref(bpSdr, fileRef);
//by David Zoller remove filename from catalog 
                        sdr_uncatlg(bpSdr, payload->filename.filename_val);
		}
		else
		//delete the payload file
			unlink(payload->filename.filename_val);
		sdr_end_xn(bpSdr);
	}
	if (payload->location == BP_PAYLOAD_MEM && payload->buf.buf_val != NULL) {
		free(payload->buf.buf_val);
		payload->buf.buf_val = NULL;
	}
}

void bp_ion_free_extensions(al_types_bundle_spec* spec)
{
	 int i;
	for ( i=0; i<spec->extensions.extension_number; i++ ) {
//		printf("Freeing extension block [%d].data at 0x%08X\n",
//						   i, (unsigned int) *(spec->blocks.blocks_val[i].data.block_type_specific_data));
		if (spec->extensions.extension_blocks[i].block_data.block_type_specific_data_len > 0)
			free(spec->extensions.extension_blocks[i].block_data.block_type_specific_data);
	}
	free(spec->extensions.extension_blocks);
	spec->extensions.extension_blocks = NULL;
	spec->extensions.extension_number= 0;
}

al_bp_error_t bp_ion_error(int err)
{
	return BP_ENOTIMPL;
}

/***************** PRIVATE FUNCTION ******************
 * There are 2 private function to parse the payload
 * and have the status report.
 * This function are a copy of private function of ION
 *****************************************************/

/* *
 * Parse the admin record to have a status report.
 * Return 1 on success
 * */
int albp_parseAdminRecord(int *adminRecordType, BpStatusRpt *rpt, BpCtSignal *csig, void **otherPtr, Object payload)
{
	Sdr				bpSdr = bp_get_sdr();
	unsigned int	buflen;
	char			*buffer;
	ZcoReader		reader;
	char			*cursor;
	int				bytesToParse;
	int				unparsedBytes;
	int				bundleIsFragment;
	int				result;
	sdr_begin_xn(bpSdr);
	buflen = zco_source_data_length(bpSdr, payload);
	buffer = (char *) malloc(sizeof(char)*buflen);
	if ( buffer == NULL )
	{
		sdr_end_xn(bpSdr);
		return -1;
	}
	zco_start_receiving(payload, &reader);
	bytesToParse = zco_receive_source(bpSdr, &reader, buflen, buffer);
	if (bytesToParse < 0)
	{
		sdr_end_xn(bpSdr);
		free(buffer);
		return -1;
	}
	cursor = buffer;
	unparsedBytes = bytesToParse;
	if (unparsedBytes < 1)
	{
		sdr_end_xn(bpSdr);
		free(buffer);
		return -1;
	}
	*adminRecordType = (*cursor >> 4 ) & 0x0f;
	bundleIsFragment = *cursor & 0x01;
	cursor++;
	unparsedBytes--;
	switch (*adminRecordType)
	{
		case BP_STATUS_REPORT:
		result = albp_parseStatusRpt(rpt, (unsigned char *) cursor,unparsedBytes, bundleIsFragment); break;
		case BP_CUSTODY_SIGNAL:
		result = 0; break;
		default: result = 0; break;
	}
	sdr_end_xn(bpSdr);
	free(buffer);
	return result;
}

/* *
 * Parse cursor to have a status report
 * Return 1 on success
 * */
int	albp_parseStatusRpt(BpStatusRpt *rpt, unsigned char *cursor,int unparsedBytes, int isFragment)
{
	unsigned int	eidLength;
	memset((char *) rpt, 0, sizeof(BpStatusRpt));
	rpt->isFragment = isFragment;
	if (unparsedBytes < 1)
	{
		return 0;
	}
	rpt->flags = *cursor;
	cursor++;
	rpt->reasonCode = *cursor;
	cursor++;
	unparsedBytes -= 2;
	if (isFragment)
	{
		extractSmallSdnv(&(rpt->fragmentOffset), &cursor, &unparsedBytes);
		extractSmallSdnv(&(rpt->fragmentLength), &cursor, &unparsedBytes);
	}

	if (rpt->flags & BP_RECEIVED_RPT)
	{
		extractSmallSdnv(&(rpt->receiptTime.seconds), &cursor,&unparsedBytes);
		extractSmallSdnv(&(rpt->receiptTime.nanosec), &cursor,&unparsedBytes);
	}

	if (rpt->flags & BP_CUSTODY_RPT)
	{
		extractSmallSdnv(&(rpt->acceptanceTime.seconds), &cursor,&unparsedBytes);
		extractSmallSdnv(&(rpt->acceptanceTime.nanosec), &cursor,&unparsedBytes);
	}

	if (rpt->flags & BP_FORWARDED_RPT)
	{
		extractSmallSdnv(&(rpt->forwardTime.seconds), &cursor,&unparsedBytes);
		extractSmallSdnv(&(rpt->forwardTime.nanosec), &cursor,&unparsedBytes);
	}

	if (rpt->flags & BP_DELIVERED_RPT)
	{
		extractSmallSdnv(&(rpt->deliveryTime.seconds), &cursor,&unparsedBytes);
		extractSmallSdnv(&(rpt->deliveryTime.nanosec), &cursor,&unparsedBytes);
	}

	if (rpt->flags & BP_DELETED_RPT)
	{

		extractSmallSdnv(&(rpt->deletionTime.seconds), &cursor,&unparsedBytes);
		extractSmallSdnv(&(rpt->deletionTime.nanosec), &cursor,&unparsedBytes);
	}

	extractSdnv((uvast*)&(rpt->creationTime.seconds), &cursor, &unparsedBytes);
	extractSmallSdnv(&(rpt->creationTime.count), &cursor, &unparsedBytes);
	extractSmallSdnv(&eidLength, &cursor, &unparsedBytes);
	if (unparsedBytes != eidLength)
	{
		return 0;
	}
	rpt->sourceEid = MTAKE(eidLength + 1);
	if (rpt->sourceEid == NULL)
	{
		return -1;
	}
	memcpy(rpt->sourceEid, cursor, eidLength);
	rpt->sourceEid[eidLength] = '\0';
	return 1;
}

/*****************************************************/
/*
 * If there isn't the ION implementation
 * the API there are not implementation
 */
#else
al_bp_error_t bp_ion_attach()
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_open_with_IP(char * daemon_api_IP, int daemon_api_port, al_types_handle * handle)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_errno(al_types_handle handle)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_build_local_eid(al_types_endpoint_id* local_eid,
								const char* service_tag,
								al_types_scheme type)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_register(al_types_handle * handle,
						al_types_reg_info* reginfo,
						al_types_reg_id* newregid)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_find_registration(al_types_handle handle,
						al_types_endpoint_id * eid,
						al_types_reg_id * newregid)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_unregister(al_types_endpoint_id eid)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_send(al_types_handle handle,
					al_types_reg_id regid,
					al_types_bundle_spec* spec,
					al_types_bundle_payload* payload)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_recv(al_types_handle handle,
					al_types_bundle_spec* spec,
					al_types_bundle_payload_location location,
					al_types_bundle_payload* payload,
					al_types_timeout timeout)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_close(al_types_handle handle)
{
	return BP_ENOTIMPL;
}

void bp_ion_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src)
{

}

al_bp_error_t bp_ion_parse_eid_string(al_types_endpoint_id* eid, const char* str)
{
	return BP_ENOTIMPL;
}

al_bp_error_t bp_ion_set_payload(al_types_bundle_payload* payload,
							al_types_bundle_payload_location location,
							char* val, int len)
{
	return BP_ENOTIMPL;
}

void bp_ion_free_payload(al_types_bundle_payload* payload)
{

}

void bp_ion_free_extensions(al_types_bundle_spec* spec)
{

}


al_bp_error_t bp_ion_error(int err)
{
	return BP_ENOTIMPL;
}
#endif /* ION_IMPLEMENTATION */

boolean_t bp_ion_is_running() {
	char* find_ion = "ps ax | grep -w rfxclock | grep -v grep > /dev/null";
	if(system(find_ion) == 0) {
		return TRUE;
	}
	return FALSE;
}