/** \file bp_ion_conversions.c
 *
 *  \brief  This file contains the functions that convert Unified API abstract types (al_types) into
 *   ION types and vice versa.
*
 *  \par Copyright
 *  	Copyright (c) 2021, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *  \authors Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *  01/01/20 | A. Bisacchi	   |  Implementation contribution.
 *
 */



#ifdef ION_IMPLEMENTATION

#ifndef NEW_ZCO
#define  NEW_ZCO 1
#endif 

#include "bp_ion_conversions.h"

bp_ion_handle bp_to_ion_handle(al_types_handle handle){
	return (bp_ion_handle ) handle;
}
al_types_handle bp_from_ion_handle(bp_ion_handle handle){

	return (al_types_handle) handle;
}

char * bp_to_ion_endpoint_id(al_types_endpoint_id endpoint_id)
{
	char * eid_ion;
	int length_eid = strlen(endpoint_id.uri)+1;
	eid_ion = (char *)(malloc(sizeof(char)*length_eid));
	memcpy(eid_ion,endpoint_id.uri,length_eid);
	return eid_ion;
}
al_types_endpoint_id bp_from_ion_endpoint_id(char * endpoint_id)
{
	al_types_endpoint_id eid_bp;
	int length = strlen(endpoint_id)+1;
	memcpy(eid_bp.uri,endpoint_id,length);
	return eid_bp;
}

al_types_creation_timestamp bp_from_ion_timestamp(BpTimestamp timestamp)
{
	al_types_creation_timestamp bp_timestamp;
	bp_timestamp.time = timestamp.seconds;
	bp_timestamp.seqno = timestamp.count;
	return bp_timestamp;
}

DtnTime bp_to_ion_timeval(al_types_timeval timeval){
	DtnTime dtntime;
	dtntime.seconds = timeval;
	dtntime.nanosec = 0;
	return dtntime;
}
al_types_timeval bp_from_ion_timeval(DtnTime timeval){
	al_types_timeval time;
	time = timeval.seconds;
	return time;
}
int bp_to_ion_timeout(al_types_timeout timeout_in_ms) {
	if(timeout_in_ms == (al_types_timeout) BP_INFINITE_WAIT) {
		return -1;
	}
	return (int)(timeout_in_ms / 1000);
}
al_types_timeout bp_from_ion_timeout(int timeout_in_s){
	if(timeout_in_s == -1) {
		return (al_types_timeout) BP_INFINITE_WAIT;
	}
	return (al_types_timeout) (timeout_in_s * 1000);
}

unsigned char bp_to_ion_bundle_srrFlags(al_types_bundle_processing_control_flags bundle_delivery_opts){
	int opts = 0;
	if(bundle_delivery_opts & BP_DOPTS_RECEPTION_BSR)
		opts |= BP_RECEIVED_RPT;
	if(bundle_delivery_opts & BP_DOPTS_CUSTODY_BSR)
		opts |= BP_CUSTODY_RPT;
	if(bundle_delivery_opts & BP_DOPTS_DELIVERY_BSR)
		opts |= BP_DELIVERED_RPT;
	if(bundle_delivery_opts & BP_DOPTS_FORWARD_BSR)
	 	opts |= BP_FORWARDED_RPT;
	if(bundle_delivery_opts & BP_DOPTS_DELETION_BSR)
		opts |= BP_DELETED_RPT;
	 return opts;
}
al_types_bundle_processing_control_flags bp_from_ion_bundle_srrFlags(unsigned char srrFlags){
	al_types_bundle_processing_control_flags opts = BP_DOPTS_NONE;
	if(srrFlags & BP_RECEIVED_RPT)
		opts |= BP_DOPTS_RECEPTION_BSR;
	if(srrFlags & BP_CUSTODY_RPT)
		opts |= BP_DOPTS_CUSTODY_BSR;
	if(srrFlags & BP_DELIVERED_RPT)
		opts |= BP_DOPTS_DELIVERY_BSR;
	if(srrFlags & BP_FORWARDED_RPT)
		opts |= BP_DOPTS_FORWARD_BSR;
	if(srrFlags & BP_DELETED_RPT)
		opts |= BP_DOPTS_DELETION_BSR;
	return opts;
}

int bp_to_ion_bundle_priority(al_types_bundle_priority_enum bundle_priority){
	switch(bundle_priority)
	{
		case BP_PRIORITY_BULK : 	return BP_BULK_PRIORITY;
		case BP_PRIORITY_NORMAL : 	return BP_STD_PRIORITY;
		case BP_PRIORITY_EXPEDITED :return BP_EXPEDITED_PRIORITY;
		default : 					return -1;
	}
}

al_types_bundle_priority_enum bp_from_ion_bundle_priority(int bundle_priority){
		al_types_bundle_priority_enum bp_priority;
		switch(bundle_priority)
		{
			case BP_BULK_PRIORITY: bp_priority = BP_PRIORITY_BULK; break;
			case BP_STD_PRIORITY : bp_priority = BP_PRIORITY_NORMAL; break;
			case BP_EXPEDITED_PRIORITY : bp_priority = BP_PRIORITY_EXPEDITED; break;
			default : bp_priority = -1; break;
		}
		return bp_priority;
	}


int bp_to_ion_status_report_flags(al_types_status_report_flags status_report_flags){
	int ion_statusRpt_flags =0;
	if(status_report_flags & BP_STATUS_RECEIVED)
		ion_statusRpt_flags |= BP_STATUS_RECEIVE;
	if(status_report_flags & BP_STATUS_CUSTODY_ACCEPTED)
			ion_statusRpt_flags |= BP_STATUS_ACCEPT;
	if(status_report_flags & BP_STATUS_FORWARDED)
			ion_statusRpt_flags |= BP_STATUS_FORWARD;
	if(status_report_flags & BP_STATUS_DELIVERED)
			ion_statusRpt_flags |= BP_STATUS_DELIVER;
	if(status_report_flags & BP_STATUS_DELETED)
			ion_statusRpt_flags |= BP_STATUS_DELETE;
//	if(status_report_flags & BP_STATUS_ACKED_BY_APP)
//			ion_statusRpt_flags |= BP_STATUS_STATS;
	return ion_statusRpt_flags;
}
al_types_status_report_flags bp_from_ion_status_report_flags(int status_report_flags)
{
	al_types_status_report_flags bp_statusRpt_flags = 0;
	if(status_report_flags & BP_RECEIVED_RPT)
//	{
		bp_statusRpt_flags |= BP_STATUS_RECEIVED;
	//	printf("\tRECEIVED %d\n",bp_statusRpt_flags);
//	}
	if(status_report_flags & BP_CUSTODY_RPT)
//	{
		bp_statusRpt_flags |= BP_STATUS_CUSTODY_ACCEPTED;
	//	printf("\tCUSTODY %d\n",bp_statusRpt_flags);
//	}
	if(status_report_flags & BP_FORWARDED_RPT)
//	{
		bp_statusRpt_flags |= BP_STATUS_FORWARDED;
	//	printf("\tFORWARDED %d\n",bp_statusRpt_flags);
//	}
	if(status_report_flags & BP_DELIVERED_RPT)
//	{
		bp_statusRpt_flags |= BP_STATUS_DELIVERED;
	//	printf("\tDELIVERED %d\n",bp_statusRpt_flags);
//	}
	if(status_report_flags & BP_DELETED_RPT)
//	{
		bp_statusRpt_flags |= BP_STATUS_DELETED;
//		printf("\tDELETE  %d\n",bp_statusRpt_flags);
//	}
	//printf("\n\tflags al_bp: %d\n",bp_statusRpt_flags);*/
	return bp_statusRpt_flags;
}

BpSrReason bp_to_ion_status_report_reason(al_types_status_report_reason status_report_reason)
{
	return (BpSrReason) status_report_reason;
}
al_types_status_report_reason bp_from_ion_status_report_reason(BpSrReason status_report_reason)
{
	return (al_types_status_report_reason) status_report_reason;
}

//The dual function al_ion_bundle_status_report has been removed because the bundle status report can only be generated by the bundle protocol agent

al_types_bundle_status_report bp_from_ion_bundle_status_report(BpStatusRpt bundle_status_report)
{
	al_types_bundle_status_report bp_statusRpt;
	memset(&bp_statusRpt,0,sizeof(al_types_bundle_status_report));
	bp_statusRpt.custody_ts = bp_from_ion_timeval(bundle_status_report.acceptanceTime);
	bp_statusRpt.deletion_ts = bp_from_ion_timeval(bundle_status_report.deletionTime);
	bp_statusRpt.delivery_ts = bp_from_ion_timeval(bundle_status_report.deliveryTime);
	bp_statusRpt.flags = bp_from_ion_status_report_flags(bundle_status_report.flags);
	bp_statusRpt.forwarding_ts = bp_from_ion_timeval(bundle_status_report.forwardTime);
	bp_statusRpt.reception_ts = bp_from_ion_timeval(bundle_status_report.receiptTime);
	bp_statusRpt.reason = bp_from_ion_status_report_reason(bundle_status_report.reasonCode);
	bp_statusRpt.bundle_x_id.creation_ts = bp_from_ion_timestamp(bundle_status_report.creationTime);
	bp_statusRpt.bundle_x_id.frag_offset = (uint32_t) bundle_status_report.fragmentOffset;
	bp_statusRpt.bundle_x_id.frag_length = (uint32_t) bundle_status_report.fragmentLength;
	bp_statusRpt.bundle_x_id.source = bp_from_ion_endpoint_id(bundle_status_report.sourceEid);
	return bp_statusRpt;
}

Payload bp_to_ion_bundle_payload(al_types_bundle_payload bundle_payload, int  priority,BpAncillaryData ancillaryData)
{
	Payload payload;
	memset(&payload,0,sizeof(Payload));
	Sdr bpSdr = bp_get_sdr();
	sdr_begin_xn(bpSdr);
	if(bundle_payload.location == BP_PAYLOAD_MEM)
	{
		Object	buff;
		buff = sdr_malloc(bpSdr, bundle_payload.buf.buf_len);
		sdr_write(bpSdr, buff, bundle_payload.buf.buf_val, bundle_payload.buf.buf_len);
		#ifdef NEW_ZCO
			payload.content = ionCreateZco(ZcoSdrSource, buff, 0, bundle_payload.buf.buf_len, priority,
				ancillaryData.ordinal, ZcoOutbound, NULL);
		#else
			payload.content = zco_create(bpSdr, ZcoSdrSource, buff, 0, bundle_payload.buf.buf_len);
		#endif
		payload.length = zco_length(bpSdr,payload.content);
	}
	else
	{
		uint32_t dimFile = 0;
		struct stat st;
		int type = 0;
		memset(&st, 0, sizeof(st));
		stat(bundle_payload.filename.filename_val, &st);
		dimFile = st.st_size;
		Object fileRef = sdr_find(bpSdr, bundle_payload.filename.filename_val, &type);
		if(fileRef == 0)
		{
			#ifdef NEW_ZCO
				fileRef = zco_create_file_ref(bpSdr, bundle_payload.filename.filename_val, "", ZcoOutbound);
			#else
				fileRef = zco_create_file_ref(bpSdr, bundle_payload.filename.filename_val, "");				
			#endif
			sdr_catlg(bpSdr, bundle_payload.filename.filename_val, 0, fileRef);
		}
		#ifdef NEW_ZCO
			payload.content = ionCreateZco(ZcoFileSource, fileRef, 0, (unsigned int) dimFile, priority,
			ancillaryData.ordinal, ZcoOutbound, NULL);
		#else
			payload.content = zco_create(bpSdr, ZcoFileSource, fileRef, 0, (unsigned int) dimFile);
		#endif
		payload.length = zco_length(bpSdr,payload.content);
	}
	sdr_end_xn(bpSdr);
	return payload;
}

al_types_bundle_payload bp_from_ion_bundle_payload(Payload bundle_payload,
								al_types_bundle_payload_location location,char * filename){
	al_types_bundle_payload payload;
	memset(&payload,0,sizeof(al_types_bundle_payload));
	char *buffer = (char*) malloc(sizeof(char) * bundle_payload.length);
	ZcoReader zcoReader;
	memset(&zcoReader,0,sizeof(ZcoReader));
	Sdr sdr = bp_get_sdr();
	zco_start_receiving(bundle_payload.content,&zcoReader);
	sdr_begin_xn(sdr);
	zco_receive_source(sdr,&zcoReader,bundle_payload.length,buffer);
	sdr_end_xn(sdr);
	payload.location = location;
	if(location == BP_PAYLOAD_MEM)
	{
		payload.buf.buf_len = bundle_payload.length;
		payload.buf.buf_val = buffer;
		//payload.buf.buf_val = (char *)malloc(sizeof(char)*bundle_payload.length);
		//memcpy(payload.buf.buf_val, buffer, bundle_payload.length);
	}
	else
	{
		FILE * f = fopen(filename, "w+");
		fwrite(buffer, bundle_payload.length, 1, f);
		fclose(f);
		payload.filename.filename_len = strlen(filename)+1;
		payload.filename.filename_val = (char *)malloc(sizeof(char)*(payload.filename.filename_len));
		strcpy(payload.filename.filename_val,filename);
		free(buffer);
	}

	//free(buffer);
	return payload;
}


//NEW METADATA RELATED FUNCTIONS

/* Author: Laura Mazzuca, laura.mazzuca@studio.unibo.it
 *
 * ION allows a bundle structure to contain only ONE metadata block.
 * Therefore the conversion from BP to ION involves only one metadata block, but the following code
 * is written bearing in mind a possible future extension to multiple blocks.
 */
//ex int al_ion_metadata(u32_t metadata_len, al_bp_extension_block_t* metadata_val, BpAncillaryData* extendedCOS);


int bp_to_ion_extensions(uint32_t extension_number, al_types_extension_block *extension_blocks, BpAncillaryData* extendedCOS) {

	int i = 0;

	for (i = 0; i < extension_number; i++)
	{
		/*
		 * assign metadata type (check on valid types is in parsing function)...
		 */

		char backup_specific_data[1000];
		strcpy (backup_specific_data, extension_blocks->block_data.block_type_specific_data );
		char * metadata_type_string = strtok (backup_specific_data, "/");
		uint32_t metadata_type = atoi(metadata_type_string);
		char metadata_string [1000];
		int total_length = (int) strlen(extension_blocks->block_data.block_type_specific_data) - 1;
		int partial_length = (int) strlen(metadata_type_string);
		if(total_length != partial_length){
		char * temp_metadata_string = strtok (NULL, "\0");
		strcpy (metadata_string,temp_metadata_string);
		}

		extendedCOS->metadataType = metadata_type;

		/*
		 * check if datalen doesn't exceed ion maximum metadata length
		 */

			uint32_t datalen = strlen(metadata_string);

		if (datalen >= BP_MAX_METADATA_LEN)
			{	return 0;	}

		//printf("\n %d \n", extendedCOS->extensions->type );

		/*
		 * ...and check if a string has been specified by command line.
		 * If yes, assign its value to BPAncillaryData variable.
		 */
		if (datalen != 0)
		{
			//use of this syntax because extendedCOS->metadataLen is unsigned char
			extendedCOS->metadataLen = datalen & 0xFF;

			//use of memcpy because extendedCOS->metadata is array of unsigned char
			memcpy(extendedCOS->metadata, metadata_string, datalen + 1);
		}
		else
		{
			/*
			 * otherwise just initialize variables to null values.
			 */
			extendedCOS->metadataLen = 0;
			memset(extendedCOS->metadata, 0, sizeof(extendedCOS->metadata));
		}
	}
	return 1;
}

/* Author: Laura Mazzuca, laura.mazzuca@studio.unibo.it
 *
 * ION allows a bundle structure to contain only ONE metadata block.
 * Therefore the conversion from ION to BP involves only one metadata block, but the following code
 * is written bearing in mind a possible future extension to multiple blocks.
 * If this happened:
 * 1) the assignment from the AncillaryData contained in the BpDelivery bundle should be modified accordingly
 * 2) each metadata block should become an array element, and a for should be added to convert all blocks.
 *
 */
//ex    int ion_al_metadata(BpDelivery dlvBundle, al_bp_bundle_spec_t * spec);

void bp_from_ion_extensions(BpDelivery dlvBundle, al_types_bundle_spec * spec) {

	int i = 0;

	spec->extensions.extension_number = 1;

	/*
	 * This re-initialization of metablock_type_specific_data is to avoid eventual segmentation faluts.
	 */
	free(spec->extensions.extension_blocks);
	spec->extensions.extension_blocks = (al_types_extension_block *)malloc(sizeof(al_types_extension_block));

	/*
	 * metadata variables are of type unsigned char, so we must cast them to unsigned integers.
	 */
	spec->extensions.extension_blocks[i].block_type_code = (unsigned int)dlvBundle.metadataType;
	spec->extensions.extension_blocks[i].block_processing_control_flags=0;

	spec->extensions.extension_blocks[i].block_data.block_type_specific_data_len = (unsigned int) dlvBundle.metadataLen;
	if (dlvBundle.metadataLen > 0)
	{
		spec->extensions.extension_blocks[i].block_data.block_type_specific_data = malloc(BP_MAX_METADATA_LEN * sizeof(char));
		memcpy(spec->extensions.extension_blocks[i].block_data.block_type_specific_data, dlvBundle.metadata, dlvBundle.metadataLen + 1);
	}
	else
	{
		spec->extensions.extension_blocks[0].block_data.block_type_specific_data = NULL;
	}
}

//TODO:potrebbero anche essere eliminate, per ora le lascio qui

unsigned char bp_from_ion_irf_trace(unsigned char ion_irf_trace)
{
	unsigned char al_irf_trace;
	al_irf_trace = ion_irf_trace;
	return al_irf_trace;
}

unsigned char bp_to_ion_irf_trace(unsigned char al_irf_trace)
{
	unsigned char ion_irf_trace;
	ion_irf_trace = al_irf_trace;
	return ion_irf_trace;
}

#endif /* ION_IMPLEMENTATION */
