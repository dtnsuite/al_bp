/** \file bp_ion_conversions.h
 *
 *  \brief  This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *  \par Copyright
 *  	Copyright (c) 2013, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *
 */
#ifndef BP_ION_CONVERSIONS_H_
#define BP_ION_CONVERSIONS_H_

#ifdef ION_IMPLEMENTATION

#include "al_types.h"

#include <bpP.h>

/**
 * Encapsulates in a structure 2 different SAPs:
 * recv is the SAP used to receive bundles (initiated by bp_open())
 * source is the SAP used to send bundles (initiated by bp_open_source()).
 * If using ION < 3.3.0, only recv is used both to send and receive bundles
 * Introduced with ION 3.3.0
 */
struct bp_ion_handle_st{
	BpSAP *recv;
	BpSAP *source;
};

typedef struct bp_ion_handle_st * bp_ion_handle ;

bp_ion_handle  bp_to_ion_handle(al_types_handle handle);
al_types_handle bp_from_ion_handle(bp_ion_handle  handle);

char * bp_to_ion_endpoint_id(al_types_endpoint_id endpoint_id);
al_types_endpoint_id bp_from_ion_endpoint_id(char * endpoint_id);

al_types_creation_timestamp bp_from_ion_timestamp(BpTimestamp timestamp);

DtnTime bp_to_ion_timeval(al_types_timeval timeval);
al_types_timeval bp_from_ion_timeval(DtnTime timeval);

int bp_to_ion_timeout(al_types_timeout timeout_in_ms);
al_types_timeout bp_from_ion_timeout(int timeout_in_s);

unsigned char bp_to_ion_bundle_srrFlags(al_types_bundle_processing_control_flags bundle_delivery_opts);
al_types_bundle_processing_control_flags bp_from_ion_bundle_srrFlags(unsigned char ssrFlag);

/* *
 * This conversion convert only the al_types_bundle_priority_enum
 * The ordinal number is setted in bp_ion_send()
 * */

int bp_to_ion_bundle_priority(al_types_bundle_priority_enum bundle_priority);
al_types_bundle_priority_enum bp_from_ion_bundle_priority(int bundle_priority);

BpSrReason bp_to_ion_status_report_reason(al_types_status_report_reason status_report_reason);
al_types_status_report_reason bp_from_ion_status_report_reason(BpSrReason status_report_reason);

int bp_to_ion_status_report_flags(al_types_status_report_flags status_repot_flags);
al_types_status_report_flags bp_from_ion_status_report_flags(int status_repot_flags);

al_types_bundle_status_report bp_from_ion_bundle_status_report(BpStatusRpt bundle_status_report);

al_types_bundle_payload bp_from_ion_bundle_payload(Payload bundle_payload,al_types_bundle_payload_location location,char * filename);
Payload bp_to_ion_bundle_payload(al_types_bundle_payload bundle_payload, int  priority, BpAncillaryData extendedCOS);

/* Author: Laura Mazzuca, laura.mazzuca@studio.unibo.it
 *
 * This function converts al_bp metadata type into ion metadata type, BPAncillaryData.
 *
 */
int bp_to_ion_extensions(uint32_t extension_number, al_types_extension_block *extension_blocks, BpAncillaryData* extendedCOS);

/* Author: Laura Mazzuca, laura.mazzuca@studio.unibo.it
 *
 * This function converts the metadata found in the ion bundle structure BPDelivery
 * into abstract layer metadata
 */
void bp_from_ion_extensions(BpDelivery dlvBundle, al_types_bundle_spec * spec);


unsigned char bp_from_ion_irf_trace(unsigned char ion_irf_trace);
unsigned char bp_to_ion_irf_trace(unsigned char al_irf_trace);

#endif /* ION_IMPLEMENTATION */
#endif  /* BP_ION_CONVERSIONS_H_ */
