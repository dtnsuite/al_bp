/** \file bp_ud3tn.h
 *
 * \brief This file contains the prototypes of the functions implemented in the homonymous .c file.
 *
 *
 * \copyright (c) 2024 Alma Mater Studiorum, University of Bologna.
 * All rights reserved.
 *
 *  \par License
 *  This file is part of Unified-API. Unified-API is free software:
 *  you can redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *  Unified-API is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Fabio Colonna, fabio.colonna3@studio.unibo.it
 */

#ifndef BP_UD3TN_H
#define BP_UD3TN_H

#ifdef HAVE_CONFIG_H
#include <dtn-config.h>
#endif

#include "al_bp.h"
#include "al_types.h"

#include <stdbool.h>

bool
bp_ud3tn_is_running();

al_bp_error_t
bp_ud3tn_open(al_types_handle *out_handle);

al_bp_error_t
bp_ud3tn_open_with_IP(const char *const ip, int port, al_types_handle *out_handle);

al_bp_error_t
bp_ud3tn_build_local_eid(al_types_handle handle, al_types_endpoint_id *local_eid,
                         const char *const service_tag, al_types_scheme type);

al_bp_error_t
bp_ud3tn_register(al_types_handle handle, al_types_reg_info *const reg_info,
                  al_types_reg_id *out_reg_id);

al_bp_error_t
bp_ud3tn_close(al_types_handle handle);

al_bp_error_t
bp_ud3tn_send(al_types_handle handle, al_types_reg_id reg_id, al_types_bundle_spec *const spec,
              al_types_bundle_payload *const payload);

al_bp_error_t
bp_ud3tn_recv(al_types_handle handle, al_types_bundle_spec *out_spec,
              al_types_bundle_payload_location location, al_types_bundle_payload *out_payload,
              al_types_timeout timeout);

void
bp_ud3tn_copy_eid(al_types_endpoint_id *dst, al_types_endpoint_id *const src);

al_bp_error_t
bp_ud3tn_parse_eid_string(al_types_endpoint_id *out_eid, const char *const str);

al_bp_error_t
bp_ud3tn_set_payload(al_types_bundle_payload *const   payload,
                     al_types_bundle_payload_location location, const char *const buf,
                     uint32_t buf_length);

void
bp_ud3tn_free_extensions(al_types_bundle_spec *const spec);

void
bp_ud3tn_free_payload(al_types_bundle_payload *const payload);

al_bp_error_t
bp_ud3tn_error(int code);

/**
 * @note NOT IMPLEMENTED
 */
al_bp_error_t
bp_ud3tn_find_registration(al_types_handle handle, al_types_endpoint_id *const eid,
                           al_types_reg_id *out_reg_id);

/**
 * @note NOT IMPLEMENTED
 */
al_bp_error_t
bp_ud3tn_unregister(al_types_handle handle, al_types_reg_id reg_id);

/**
 * @note NOT IMPLEMENTED
 */
al_bp_error_t
bp_ud3tn_errno(al_types_handle handle);

#endif// BP_UD3TN_H