/** \file bp_ud3tn_conversions.h
 *
 * \brief This file contains the prototypes of the functions implemented in the
 * homonymous .c file.
 *
 * \copyright (c) 2024 Alma Mater Studiorum, University of Bologna.
 * All rights reserved.
 * 
 *  \par License
 *  This file is part of Unified-API. Unified-API is free software:
 *  you can redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *  Unified-API is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Fabio Colonna, fabio.colonna3@studio.unibo.it
 *
 *  ***********************************************/

#pragma once

#ifdef UD3TN_IMPLEMENTATION

// We try to force gcc to inline these functions to avoid overhead
#define FORCE_INLINE __attribute__((always_inline)) inline

#include "al_types.h"
#include "aap2_backend_types.h"
#include <string.h>

// HANDLE CONVERSION

FORCE_INLINE ud3tn_handle_t
bp_to_ud3tn_handle(al_types_handle handle)
{
    return (ud3tn_handle_t) handle;
}

FORCE_INLINE al_types_handle
bp_from_ud3tn_handle(ud3tn_handle_t handle)
{
    return (al_types_handle) handle;
}

// EID CONVERSION

FORCE_INLINE ud3tn_endpoint_id_t
bp_to_ud3tn_endpoint_id(al_types_endpoint_id *const endpoint_id)
{
    ud3tn_endpoint_id_t ud3tn_eid;
    strncpy(ud3tn_eid.uri, endpoint_id->uri, AL_TYPES_MAX_EID_LENGTH);
    return ud3tn_eid;
}

FORCE_INLINE al_types_endpoint_id
bp_from_ud3tn_endpoint_id(ud3tn_endpoint_id_t *const endpoint_id)
{
    al_types_endpoint_id bp_eid;
    strncpy(bp_eid.uri, endpoint_id->uri, AL_TYPES_MAX_EID_LENGTH);
    return bp_eid;
}

// TIMEVAL CONVERSION

FORCE_INLINE ud3tn_timeval_t
bp_to_ud3tn_timeval(al_types_timeval timeval)
{
    return (al_types_timeval) timeval;
}

FORCE_INLINE al_types_timeval
bp_from_ud3tn_timeval(ud3tn_timeval_t timeval)
{
    return (ud3tn_timeval_t) timeval;
}

// SCHEME CONVERSTION

FORCE_INLINE ud3tn_scheme_t
bp_to_ud3tn_scheme(al_types_scheme scheme)
{
    switch (scheme) {
        case IPN_SCHEME:
            return UD3TN_IPN_SCHEME;
        case DTN_SCHEME:
            return UD3TN_DTN_SCHEME;
    }

    return -1;
}

FORCE_INLINE al_types_scheme
bp_from_ud3tn_scheme(ud3tn_scheme_t scheme)
{
    switch (scheme) {
        case UD3TN_IPN_SCHEME:
            return IPN_SCHEME;
        case UD3TN_DTN_SCHEME:
            return DTN_SCHEME;
    }

    return -1;
}

// TIMEOUT CONVERSION

FORCE_INLINE ud3tn_timeval_t
bp_to_ud3tn_timeout(al_types_timeout timeout_ms)
{
    return timeout_ms == BP_INFINITE_WAIT ? UD3TN_NO_TIMEOUT : (ud3tn_timeval_t) timeout_ms;
}

FORCE_INLINE al_types_timeout
bp_from_ud3tn_timeout(ud3tn_timeval_t timeout_ms)
{
    return timeout_ms == UD3TN_NO_TIMEOUT ? BP_INFINITE_WAIT : (al_types_timeout) timeout_ms;
}

// TIMESTAMP CONVERSION

FORCE_INLINE ud3tn_creation_timestamp_t
bp_to_ud3tn_timestamp(al_types_creation_timestamp *const ts)
{
    ud3tn_creation_timestamp_t ud3tn_timestamp = {.seqno = ts->seqno, .time = ts->time};
    return ud3tn_timestamp;
}

FORCE_INLINE al_types_creation_timestamp
bp_from_ud3tn_timestamp(ud3tn_creation_timestamp_t *const ts)
{
    al_types_creation_timestamp bp_timestamp = {.seqno = ts->seqno, .time = ts->time};
    return bp_timestamp;
}

// PAYLOAD LOCATION CONVERSION

FORCE_INLINE ud3tn_bundle_payload_location_t
bp_to_ud3tn_bundle_payload_location(al_types_bundle_payload_location loc)
{
    return (ud3tn_bundle_payload_location_t) loc;
}

FORCE_INLINE al_types_bundle_payload_location
bp_from_ud3tn_bundle_payload_location(ud3tn_bundle_payload_location_t loc)
{
    return (al_types_bundle_payload_location) loc;
}

// STATUS REPORT REASON CONVERSION

FORCE_INLINE ud3tn_status_report_reason_t
bp_to_ud3tn_status_report_reason(al_types_status_report_reason srr)
{
    return (ud3tn_status_report_reason_t) srr;// void*
}

FORCE_INLINE al_types_status_report_reason
bp_from_ud3tn_status_report_reason(ud3tn_status_report_reason_t srr)
{
    return *((al_types_status_report_reason *) srr);// void*
}

// STATUS REPORT FLAGS CONVERSION

FORCE_INLINE ud3tn_status_report_flags_t
bp_to_ud3tn_status_report_flags(al_types_status_report_flags srf)
{
    return (ud3tn_status_report_flags_t) srf;
}

FORCE_INLINE al_types_status_report_flags
bp_from_ud3tn_status_report_flags(ud3tn_status_report_flags_t srf)
{
    return *((al_types_status_report_flags *) srf);
}

// BUNDLE ID CONVERSION

FORCE_INLINE ud3tn_bundle_id_t
bp_to_ud3tn_bundle_id(al_types_bundle_id *const al_bid)
{
    ud3tn_bundle_id_t u_bid = {.source      = bp_to_ud3tn_endpoint_id(&(al_bid->source)),
                               .creation_ts = bp_to_ud3tn_timestamp(&(al_bid->creation_ts)),
                               .frag_offset = al_bid->frag_offset < 0 ? -1 : al_bid->frag_offset,
                               .frag_length = al_bid->frag_length};

    return u_bid;
}

FORCE_INLINE al_types_bundle_id
bp_from_ud3tn_bundle_id(ud3tn_bundle_id_t *const u_bid)
{
    al_types_bundle_id al_bid = {.source      = bp_from_ud3tn_endpoint_id(&(u_bid->source)),
                                 .creation_ts = bp_from_ud3tn_timestamp(&(u_bid->creation_ts)),
                                 .frag_offset = u_bid->frag_offset < 0 ? -1 : u_bid->frag_offset,
                                 .frag_length = u_bid->frag_length};

    return al_bid;
}

// BUNDLE STATUS REPORT CONVERSION

FORCE_INLINE al_types_bundle_status_report
bp_from_ud3tn_bundle_status_report(ud3tn_bundle_status_report_t *const u_bsr)
{
    al_types_bundle_status_report al_bsr = {
            .reason        = bp_from_ud3tn_status_report_reason(u_bsr->reason),
            .flags         = bp_from_ud3tn_status_report_flags(u_bsr->flags),
            .reception_ts  = u_bsr->reception_ts,
            .custody_ts    = u_bsr->custody_ts,
            .forwarding_ts = u_bsr->forwarding_ts,
            .delivery_ts   = u_bsr->delivery_ts,
            .deletion_ts   = u_bsr->deletion_ts,
            .ack_by_app_ts = u_bsr->ack_by_app_ts};

    return al_bsr;
}


// REG INFO CONVERSION

FORCE_INLINE ud3tn_reg_info_t
bp_to_ud3tn_reg_info(al_types_reg_info *const al_rinfo)
{
    ud3tn_reg_info_t u_rinfo = {
            .endpoint  = bp_to_ud3tn_endpoint_id(&(al_rinfo->endpoint)),
            .flags     = al_rinfo->flags,
            // More fields to be added
    };

    return u_rinfo;
}

FORCE_INLINE al_types_reg_info
bp_from_ud3tn_reg_info(ud3tn_reg_info_t *const u_rinfo)
{
    al_types_reg_info al_rinfo = {
            .endpoint  = bp_from_ud3tn_endpoint_id(&(u_rinfo->endpoint)),
            .flags     = u_rinfo->flags,
            // More fields to be added
    };

    return al_rinfo;
}

// BUNDLE SPEC CONVERSION

FORCE_INLINE ud3tn_bundle_spec_t
bp_to_ud3tn_bundle_spec(al_types_bundle_spec *const al_spec)
{
    ud3tn_bundle_spec_t u_spec = {
            .source      = bp_to_ud3tn_endpoint_id(&(al_spec->source)),
            .dest        = bp_to_ud3tn_endpoint_id(&(al_spec->destination)),
            .creation_ts = bp_to_ud3tn_timestamp(&(al_spec->creation_ts))
            // More fields to be added
    };

    return u_spec;
}

FORCE_INLINE al_types_bundle_spec
bp_from_ud3tn_bundle_spec(ud3tn_bundle_spec_t *const u_spec)
{
    al_types_bundle_spec al_spec = {
            .source      = bp_from_ud3tn_endpoint_id(&(u_spec->source)),
            .destination = bp_from_ud3tn_endpoint_id(&(u_spec->dest)),
            .creation_ts = bp_from_ud3tn_timestamp(&(u_spec->creation_ts))
            // More fields to be added
    };

    return al_spec;


    // 	int i;
    // 	ud3tn_extension_block_t bp_bundle_block;
    //memset(&bp_bundle_spec, 0, sizeof(bp_bundle_spec));
    // 	memset(&bp_bundle_block, 0, sizeof(bp_bundle_block));
    //bp_bundle_spec.source = bp_from_ud3tn_endpoint_id(bundle_spec.source);
    // 	bp_bundle_spec.dest = bp_from_ud3tn_endpoint_id(bundle_spec.dest);
    // 	bp_bundle_spec.replyto = bp_from_ud3tn_endpoint_id(bundle_spec.replyto);
    // 	bp_bundle_spec.priority =
    // ud3tn_al_bundle_priority(bundle_spec.priority); 	bp_bundle_spec.dopts =
    // bp_from_ud3tn_bundle_delivery_opts(bundle_spec.dopts);
    // 	bp_bundle_spec.expiration =
    // bp_from_ud3tn_timeval(bundle_spec.expiration); 	bp_bundle_spec.creation_ts
    // = bp_from_ud3tn_timestamp(bundle_spec.creation_ts);
    // 	bp_bundle_spec.delivery_regid =
    // bp_from_ud3tn_reg_id(bundle_spec.delivery_regid);
    // 	bp_bundle_spec.blocks.blocks_len = bundle_spec.blocks.blocks_len;
    // 	bp_bundle_spec.metadata.metadata_len =
    // bundle_spec.metadata.metadata_len; 	if(bp_bundle_spec.blocks.blocks_len ==
    // 0) 		bp_bundle_spec.blocks.blocks_val = NULL; 	else
    // 	{
    // 		bp_bundle_spec.blocks.blocks_val =
    // //dz debug				(al_bp_extension_block_t*)
    // malloc(bundle_spec.blocks.blocks_len); 				(al_bp_extension_block_t*)
    // malloc(bundle_spec.blocks.blocks_len * sizeof(al_bp_extension_block_t));
    // for(i=0; i<bundle_spec.blocks.blocks_len; i++)
    // 	    {
    // 	    	bp_bundle_block = bundle_spec.blocks.blocks_val[i];
    // 	        bp_bundle_spec.blocks.blocks_val[i].type = bp_bundle_block.type;
    // 	        bp_bundle_spec.blocks.blocks_val[i].flags =
    // bp_bundle_block.flags; 	        bp_bundle_spec.blocks.blocks_val[i].data.data_len
    // = bp_bundle_block.data.data_len; 	        if(bp_bundle_block.data.data_len == 0)
    // 	        	bp_bundle_spec.blocks.blocks_val[i].data.data_val =
    // NULL; 	        else
    // 	        {
    // //dz debug bp_bundle_spec.blocks.blocks_val[i].data.data_val =
    // //dz debug	        			(char*)
    // malloc(bp_bundle_block.data.data_len + 1);
    // //dz debug
    // memcpy(bp_bundle_spec.blocks.blocks_val[i].data.data_val,
    // //dz debug	            		bp_bundle_block.data.data_val,
    // (bp_bundle_block.data.data_len) + 1);
    // 	        	bp_bundle_spec.blocks.blocks_val[i].data.data_val =
    // 	        			(char*)
    // malloc(bp_bundle_block.data.data_len);
    // 	            memcpy(bp_bundle_spec.blocks.blocks_val[i].data.data_val,
    // 	            		bp_bundle_block.data.data_val,
    // bp_bundle_block.data.data_len);

    // //XXX/dz - Just copied the data above so this is not needed and would
    // overwrite the data_val pointerr
    // //dz debug bp_bundle_spec.blocks.blocks_val[i].data.data_val =
    // //dz debug (char*)bp_bundle_block.data.data_val;
    // 	        }
    // 	    }
    // 	}
    // 	if (bp_bundle_spec.metadata.metadata_len == 0)
    // 		bp_bundle_spec.metadata.metadata_val = NULL;
    // 	else
    // 	{
    // 		bp_bundle_spec.metadata.metadata_val =
    // 				(al_bp_extension_block_t*)
    // malloc(bundle_spec.metadata.metadata_len); 	    for(i=0;
    // i<bundle_spec.metadata.metadata_len; i++)
    // 	    {
    // 	    	bp_bundle_block = bundle_spec.metadata.metadata_val[i];
    // 	        bp_bundle_spec.metadata.metadata_val[i].type =
    // bp_bundle_block.type; 	        bp_bundle_spec.metadata.metadata_val[i].flags =
    // bp_bundle_block.flags;
    // 	        bp_bundle_spec.metadata.metadata_val[i].data.data_len =
    // bp_bundle_block.data.data_len; 	        if(bp_bundle_block.data.data_len == 0)
    // 	        	bp_bundle_spec.metadata.metadata_val[i].data.data_val =
    // NULL; 	        else
    // 	        {
    // 	        	bp_bundle_spec.metadata.metadata_val[i].data.data_val =
    // 	        			(char*)
    // malloc(bp_bundle_block.data.data_len + 1);
    // 	            memcpy(bp_bundle_spec.metadata.metadata_val[i].data.data_val,
    // 	            		bp_bundle_block.data.data_val,
    // (bp_bundle_block.data.data_len) + 1);
    // 	            bp_bundle_spec.metadata.metadata_val[i].data.data_val =
    // 	            		(char*)bp_bundle_block.data.data_val;
    // 	        }
    // 	    }
    // 	}
}


// BUNDLE PAYLOAD CONVERSION

FORCE_INLINE ud3tn_bundle_payload_t
bp_to_ud3tn_bundle_payload(al_types_bundle_payload *const al_pl)
{
    ud3tn_bundle_payload_t u_pl = {0};
    switch (al_pl->location) {
        case BP_PAYLOAD_MEM:
            u_pl.buf.buf_len = al_pl->buf.buf_len;
            u_pl.buf.buf_val = (al_pl->buf.buf_len == 0) ? NULL : al_pl->buf.buf_val;
            //u_pl.location = BP_PAYLOAD_MEM;
            break;
        case BP_PAYLOAD_FILE:
            //u_pl.location = BP_PAYLOAD_FILE;
            //memcpy(u_pl.filename, al_pl->filename, sizeof al_pl.filename);
            break;
        default:
            break;
    }

    //ud3tn_bundle_status_report_t u_sr = bp_to_ud3tn_bundle_status_report(al_pl->status_report);
    //u_pl.status_report = al_pl->status_report ? &u_sr : NULL;

    return u_pl;
}

FORCE_INLINE al_types_bundle_payload
bp_from_ud3tn_bundle_payload(ud3tn_bundle_payload_t *const u_pl)
{
    al_types_bundle_payload al_pl = {
            .location = BP_PAYLOAD_MEM,
            .filename = {0},
            .buf =
                    {
                            .buf_val = u_pl->buf.buf_val,
                            .buf_len = u_pl->buf.buf_len,
                    },
            .status_report = NULL,
    };

    /*
    al_pl.buf.buf_val = (char *) malloc(u_pl->buf.buf_len);
    memcpy(al_pl.buf.buf_val, u_pl->buf.buf_val, u_pl->buf.buf_len);
    al_pl->buf.buf_len = al_pl->buf.buf_len;
    */

    return al_pl;

    // bp_bundle_payload.location =
    // bp_from_ud3tn_bundle_payload_location(bundle_payload.location);
    // bp_bundle_payload.filename.filename_len =
    // bundle_payload.filename.filename_len; if
    // (bundle_payload.filename.filename_len == 0 ||
    // bundle_payload.filename.filename_val == NULL)
    // {
    // 	bp_bundle_payload.filename.filename_val = NULL;
    // 	bp_bundle_payload.filename.filename_len = 0;
    // }
    // else
    // {
    // 	bp_bundle_payload.filename.filename_val = (char*)
    // malloc(bundle_payload.filename.filename_len + 1);
    // 	strncpy(bp_bundle_payload.filename.filename_val,
    // bundle_payload.filename.filename_val,
    // bundle_payload.filename.filename_len + 1);
    // }
    // bp_bundle_payload.buf.buf_len = bundle_payload.buf.buf_len;
    // if (bundle_payload.buf.buf_len == 0 || bundle_payload.buf.buf_val ==
    // NULL)
    // {
    // 	bp_bundle_payload.buf.buf_len = 0;
    // 	bp_bundle_payload.buf.buf_val = NULL;
    // }
    // else
    // {
    // 	bp_bundle_payload.buf.buf_val = (char*)
    // malloc(bundle_payload.buf.buf_len); 	memcpy(bp_bundle_payload.buf.buf_val,
    // bundle_payload.buf.buf_val, bundle_payload.buf.buf_len); }if
    // (bundle_payload.status_report == NULL)
    // {
    // 	bp_bundle_payload.status_report = NULL;
    // }
    // else
    // {
    // 	al_types_bundle_status_report * bp_bundle_status_report =
    // bp_from_ud3tn_bundle_status_report(*(bundle_payload.status_report));
    // bp_bundle_payload.status_report = bp_bundle_status_report;
    // }
}

/*

The following functions are commented because the types they convert have not 
been implemented yet in the UD3TN C API.

// REG FLAGS CONVERSION

ud3tn_reg_flags_t
bp_to_ud3tn_reg_flags(al_types_reg_flags reg_flags)
{
    return (ud3tn_reg_flags_t) reg_flags;
}

al_types_reg_flags
bp_from_ud3tn_reg_flags(ud3tn_reg_flags_t reg_flags)
{
    return *((al_types_reg_flags *) reg_flags);
}

// DELIVERY OPTS CONVERSION

ud3tn_bundle_delivery_opts_t
bp_to_ud3tn_bundle_delivery_opts(al_types_bundle_processing_control_flags bundle_delivery_opts)
{
    return (ud3tn_bundle_delivery_opts_t) bundle_delivery_opts;
}

al_types_bundle_processing_control_flags
bp_from_ud3tn_bundle_delivery_opts(ud3tn_bundle_delivery_opts_t bundle_delivery_opts)
{
    return *((al_types_bundle_processing_control_flags *) bundle_delivery_opts);
}

// BUNDLE PRIORITY CONVERSION

ud3tn_bundle_priority_t
bp_to_ud3tn_bundle_priority(al_types_bundle_priority_enum bundle_priority)
{
    return (ud3tn_bundle_priority_t) bundle_priority;
}

al_types_bundle_priority_enum
ud3tn_al_bundle_priority(ud3tn_bundle_priority_t bundle_priority)
{
    al_types_bundle_priority_enum bp_priority;

    bp_priority = *((al_types_bundle_priority_enum *) bundle_priority);
    return bp_priority;
}

*/

#endif /* UD3TN_IMPLEMENTATION */