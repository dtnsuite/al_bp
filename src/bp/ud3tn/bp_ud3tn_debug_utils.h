/** \file bp_ud3tn_debug.h
 *
 * \copyright (c) 2024 Alma Mater Studiorum, University of Bologna.
 * All rights reserved.
 *
 *  \par License
 *  This file is part of Unified-API. Unified-API is free software:
 *  you can redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *  Unified-API is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 * \author Fabio Colonna, fabio.colonna3@studio.unibo.it
 */

#pragma once

/**
 * @note Environment variables used by UD3TN to control debug output
 * (both errors and messages). Inspired from DTN2
 */
#define AAP_ENABLE_DEBUG "UD3TN_AAP_DEBUG"

/**
 * @note Macros for printing debug messages / errors belonging to the lower UD3TN code.
 * This includes the aap_bridge, the .cpp file (where the timeout & mutex logic
 * is implemented) and the conversion file.
 */
#define DEBUG_MSG(fmt, ...)                                                                                            \
    do                                                                                                                 \
    {                                                                                                                  \
        if (getenv(AAP_ENABLE_DEBUG)) fprintf(stderr, "[UD3TN DEBUG MSG]: " fmt, ##__VA_ARGS__);                       \
    } while (0)

#define DEBUG_ERR(fmt, ...)                                                                                            \
    do                                                                                                                 \
    {                                                                                                                  \
        if (getenv(AAP_ENABLE_DEBUG))                                                                                  \
            fprintf(stderr, "[UD3TN DEBUG ERR]: In %s(): " fmt, __FUNCTION__, ##__VA_ARGS__);                          \
    } while (0)
