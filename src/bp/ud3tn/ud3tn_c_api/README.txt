The files contained in this directory have been designed to become 
integral part of ud3tn offical package.
For this reason, they are released under the Apache 2.0 licence. 
See: https://www.apache.org/licenses/LICENSE-2.0.txt
They have been developed by students of University of Bologna, under 
the co-supervision of Felix Walter, the author of ud3tn.
These files are part of Unified-API only temporarily.
They provide support to the new AAP2 protcol only. Support to the old AAP 
has been removed.  
