/** \file ud3tn_c_api.c
 *
 * \brief This file contains the functions that implement a C API on top of the
 * ud3tn AAP2. Destined to be moved to ud3tn official code.
 *
 * \copyright (c) 2024 Alma Mater Studiorum, University of Bologna.
 *
 * \par License
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * \author Fabio Colonna, fabio.colonna3@studio.unibo.it
 * \author Beatrice Barbieri, beatrice.barbieri7@studio.unibo.it 
 */

#ifdef UD3TN_IMPLEMENTATION

#include "aap2_backend.h"
#include "aap2_backend_types.h"
#include "bp_ud3tn_debug_utils.h"
#include "cla_tcp_util.h"

#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pb.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

/* --------------- PRIVATE FUNCTIONS --------------- */

// Representation of AAP 2.0 BundleADUFlags repeated enum field.
struct bundle_adu_flags {
    bool bpdu;
    bool with_bdm_auth;
};

static size_t set_adu_flags(
    aap2_BundleADUFlags *flags_field, struct bundle_adu_flags flags)
{
    size_t count = 0;

    if (flags.bpdu)
        flags_field[count++] = aap2_BundleADUFlags_BUNDLE_ADU_BPDU;
    if (flags.with_bdm_auth)
        flags_field[count++] = (aap2_BundleADUFlags_BUNDLE_ADU_WITH_BDM_AUTH);

    return count;
}

/**
 * @note BB: callback functions for nanopb I/O.
 */
static bool pb_recv_callback(pb_istream_t *stream, uint8_t *buf, size_t count)
{
    if (count == 0)
        return true;

    const int sock = (intptr_t) stream->state;
    ssize_t recvd = tcp_recv_all(sock, buf, count);

    // EOF?
    if (!recvd)
        stream->bytes_left = 0;
    else if (recvd < 0)
        DEBUG_ERR("AAP2Client recv() error");

    return (size_t) recvd == count;
}

static bool write_callback(pb_ostream_t *stream, const uint8_t *buf, size_t count)
{
    struct tcp_write_to_socket_param *const wsp = stream->state;

    tcp_write_to_socket(wsp, buf, count);

    return !wsp->errno_;
}

/**
 * @note BB: utility functions for sending and receiving messages
 */

/**
 * @brief Used for sending a message to the AAP socket.
 *
 * @param socket_fd [INPUT] Socket on which to send the message
 * @param fields [INPUT] Message description as provided by nanopb
 * @param src_struct [INPUT] Message to send
 * @return int 0 if successful, errno otherwise
 */
static int
send_mesg_aap2(const int socket_fd, const pb_msgdesc_t *const fields, const void *const src_struct)
{
    struct tcp_write_to_socket_param wsp = {
        .socket_fd = socket_fd,
        .errno_ = 0,
    };

    pb_ostream_t stream = {
        &write_callback,
        &wsp,
        SIZE_MAX,
        0,
        NULL,
    };

    const bool ret = pb_encode_ex(
        &stream,
        fields,
        src_struct,
        PB_ENCODE_DELIMITED);

    if (wsp.errno_) {
        DEBUG_ERR("AAP2Client, send() error");
    } else if (!ret) {
        DEBUG_ERR("AAP2Client: Protobuf encode error");
    }

    return wsp.errno_;
}

/**
 * @note BB: this function receives the first welcome message
 */
static ud3tn_error_t
recv_welcome_mesg_aap2(int input_fd, pb_istream_t *input, aap2_AAPMessage *out_mesg)
{
    // throwaway read for incompatibility byte
    uint8_t byte;
    int inc = read(input_fd, &byte, 1);
    (void) inc;

    bool success = pb_decode_ex(
        input,
        aap2_AAPMessage_fields,
        out_mesg,
        PB_DECODE_DELIMITED);

    if (!success) {
        DEBUG_ERR("AAP2Agent: Protobuf decode error: %d\n", success);
        return UD3TN_EINTERNAL;
    }

    // moved deallocation as pb_release would delete all dynamically generated message contents, including strings.
    return UD3TN_SUCCESS;
}

/**
 * @note BB: new function made to receive aap2 message and response types.
 * recv_adu_mesg_aap2, listed below, has the specific purpose of receiving ADU responses with a timeout.
 * This function is not identical to send_mesg_aap2: that is because of the fact that the output stream has to be 
 * created everytime, whereas input streams are created once and saved in the UD3TNClientInfo struct.
 */

static ud3tn_error_t
recv_mesg_aap2(pb_istream_t *input, const pb_msgdesc_t *const fields, void * dest_struct)
{
    const bool success = pb_decode_ex(
        input,
        fields,
        dest_struct,
        PB_DECODE_DELIMITED);

    if (!success) {
        DEBUG_ERR("Protobuf decode error: %d\n", success);
        return UD3TN_EINTERNAL;
    }

    return UD3TN_SUCCESS;
}

/**
 * @brief Used for receiving a bundle from the AAP socket. The data is received
 * and then returned to the public function that called this.
 * @param handle [INPUT] Structure that defines a connection with the AAP
 * @param timeout [INPUT] Timeout at which to stop waiting for a bundle
 * @param payload [INPUT] Pointer to payload that is only modified in case message is BundleADU
 * @param mesg [OUTPUT] Bundle content incapsulated in an aap_message struct
 * (type is AAP_INVALID on error)
 * @return ud3tn_error_t UD3TN_SUCCESS on success, UD3TN_EINTERNAL on error,
 * UD3TN_ETIMEOUT if the timeout expires
 * BB changes to recv_mesg:
 * removed some information (eid, payload_length) contained in out_mesg since now not all messages contain it.
 */

static ud3tn_error_t
recv_adu_mesg_aap2(int input_fd, pb_istream_t *input, ud3tn_timeval_t timeout, aap2_AAPMessage *out_mesg)
{
    DEBUG_MSG("Entering %s\n", __FUNCTION__);

    // Used to handle select()
    fd_set read_fds_mask; // Mask of file descriptors to be monitored for read
    struct timeval tout;  // Timeout for select()
    int ready_fds;        // Number of file descriptors ready for read

    do {
        // Select modifies timeout and mask every time, need to reset them everytime
        FD_ZERO(&read_fds_mask);
        FD_SET(input_fd, &read_fds_mask);
        tout.tv_sec = timeout / 1000;
        tout.tv_usec = (timeout % 1000) * 1000;
        printf("%ld", tout.tv_sec);

        // select() will block until either the socket is ready to be read or
        // the timeout expires If timeout is NULL, then select() waits
        // indefinitely until the socket is ready to be read
        if ((ready_fds = select(input_fd + 1, &read_fds_mask, NULL, NULL,
                 timeout == UD3TN_NO_TIMEOUT ? NULL : (&tout)))
            <= 0) {
            DEBUG_ERR("select(): %s\n", ready_fds == 0 ? "Timed out" : strerror(errno));
            return ready_fds == 0 ? UD3TN_ETIMEOUT : UD3TN_EINTERNAL;
        }

        bool success = pb_decode_ex(
            input,
            aap2_AAPMessage_fields,
            out_mesg,
            PB_DECODE_DELIMITED);

        if (!success) {
            DEBUG_ERR("recv(): Protobuf decode error (-> assuming connection was closed)");
            // pb_release(aap2_AAPMessage_fields, out_mesg); nothing to deallocate as pb_decode_ex does this automatically
            return UD3TN_ERECV;
        }

        DEBUG_MSG("Done: EID: %s, Payload length: %lu, Type: %d\n", out_mesg->msg.adu.src_eid,
            out_mesg->msg.adu.payload_length, out_mesg->which_msg);
        break;

    } while (true);

    return UD3TN_SUCCESS;
}

/**
 * @note BB: this function receives the payload of a bundle. Note that this is because the payload and BundleADU are sent
 * separately.
 */
static uint8_t *receive_payload(pb_istream_t *istream, size_t payload_length)
{
    if (payload_length > BUNDLE_MAX_SIZE) {
        DEBUG_ERR("AAP2Client: Payload too large!");
        return NULL;
    }

    uint8_t *payload = malloc(payload_length);

    if (!payload) {
        DEBUG_ERR("AAP2Client: Payload alloc error!");
        return NULL;
    }

    const bool success = pb_read(istream, payload, payload_length);

    if (!success) {
        free(payload);
        payload = NULL;
        DEBUG_ERR("AAP2Client: Payload read error!");
    }

    return payload;
}

/**
 * @note BB: this function connects to the socket.
 */
static int
connect_to_socket(int domain, void *const addr, socklen_t sz, ud3tn_sockets *sockets)
{
    int sock_fd = socket(domain, SOCK_STREAM, 0);
    if (sock_fd < 0) {
        DEBUG_ERR("socket(): %s\n", strerror(errno));
        return -1;
    }

    if (connect(sock_fd, (struct sockaddr *) addr, sz) < 0) {
        DEBUG_ERR("connect(): %s\n", strerror(errno));
        close(sock_fd);
        return -1;
    }

    sockets->one = sock_fd;

    sock_fd = socket(domain, SOCK_STREAM, 0);
    if (sock_fd < 0) {
        DEBUG_ERR("socket(): %s\n", strerror(errno));
        return -1;
    }

    if (connect(sock_fd, (struct sockaddr *) addr, sz) < 0) {
        DEBUG_ERR("connect(): %s\n", strerror(errno));
        close(sock_fd);
        return -1;
    }

    sockets->two = sock_fd;
    return 0;
}

/* --------------- PUBLIC FUNCTIONS --------------- */

ud3tn_error_t
ud3tn_close(ud3tn_handle_t handle)
{
    DEBUG_MSG("Entering %s\n", __FUNCTION__);

    // Params check
    if (!handle) {
        DEBUG_ERR("Invalid input param\n");
        return UD3TN_ENULLPNTR;
    }
    // close(handle->socket);
    shutdown(handle->input_socket, SHUT_RDWR);
    shutdown(handle->output_socket, SHUT_RDWR);
    close(handle->input_socket);
    close(handle->output_socket);
    free(handle);
    return UD3TN_SUCCESS;
}

/**
 * @note BB: this function creates two connections on two different sockets in order to perform bidirectional
 * communication. it also creates the input streams for each connection and then receives the two welcome messages
 */

ud3tn_error_t
ud3tn_open(ud3tn_handle_t *out_handle, char* sock_path)
{
    DEBUG_MSG("Entering %s\n", __FUNCTION__);
    struct _aap2_AAPMessage welcome_mesg = aap2_AAPMessage_init_default;
    struct sockaddr_un aap2_addr;
    ud3tn_error_t recv_err;

    DEBUG_MSG("Using AAP UNIX socket path: %s\n", sock_path);

    *out_handle = (UD3TNClientInfo *) malloc(sizeof(UD3TNClientInfo));
    if (!out_handle) {
        DEBUG_ERR("malloc(): %s\n", strerror(errno));
        *out_handle = NULL;
        return UD3TN_EINTERNAL;
    }

    aap2_addr.sun_family = AF_UNIX;
    strncpy(aap2_addr.sun_path, sock_path, sizeof(aap2_addr.sun_path) - 1);

    ud3tn_sockets *fds = (ud3tn_sockets *) malloc(sizeof(ud3tn_sockets));
    if ((connect_to_socket(AF_UNIX, &aap2_addr, sizeof aap2_addr, fds)) < 0)
        return UD3TN_EOPEN;
    (*out_handle)->input_socket = fds->one;
    (*out_handle)->output_socket = fds->two;

    (*out_handle)->input_socket_in = (pb_istream_t) {
        &pb_recv_callback,
        (void *) (intptr_t) (*out_handle)->input_socket,
        SIZE_MAX,
        NULL,
    };

    (*out_handle)->output_socket_in = (pb_istream_t) {
        &pb_recv_callback,
        (void *) (intptr_t) (*out_handle)->output_socket,
        SIZE_MAX,
        NULL,
    };

    recv_err = recv_welcome_mesg_aap2((*out_handle)->input_socket, &(*out_handle)->input_socket_in, &welcome_mesg);
    if (welcome_mesg.which_msg != aap2_AAPMessage_welcome_tag || recv_err != UD3TN_SUCCESS) {
        if (recv_err != UD3TN_SUCCESS)
            DEBUG_ERR("Error in receiving input welcome message\n");
        else
            DEBUG_ERR("AAP2 did not welcome the input connection request\n");
        pb_release(aap2_AAPMessage_fields, &welcome_mesg);
        ud3tn_close(*out_handle);
        return UD3TN_EOPEN;
    }

    recv_err = recv_welcome_mesg_aap2((*out_handle)->output_socket, &(*out_handle)->output_socket_in, &welcome_mesg);
    if (welcome_mesg.which_msg != aap2_AAPMessage_welcome_tag || recv_err != UD3TN_SUCCESS) {
        if (recv_err != UD3TN_SUCCESS)
            DEBUG_ERR("Error in receiving output welcome message\n");
        else
            DEBUG_ERR("AAP2 did not welcome the output connection request\n");
        pb_release(aap2_AAPMessage_fields, &welcome_mesg);
        ud3tn_close(*out_handle);
        return UD3TN_EOPEN;
    }

    strncpy((*out_handle)->nodeID, welcome_mesg.msg.welcome.node_id, sizeof((*out_handle)->nodeID) - 1);

    // deallocation is here in order to keep welcome_mesg accessible to open
    pb_release(aap2_AAPMessage_fields, &welcome_mesg);

    DEBUG_MSG("Got welcome message back: Connected to the AAP2 through a UNIX socket\n");
    DEBUG_MSG("Node ID: %s\n", (*out_handle)->nodeID);
    return UD3TN_SUCCESS;
}

ud3tn_error_t
ud3tn_open_with_ip(const char *const addr, int port, ud3tn_handle_t *out_handle)
{
    DEBUG_MSG("Entering %s\n", __FUNCTION__);

    // Params check
    if (port < 0) {
        DEBUG_ERR("Invalid port number: %d\n", port);
        *out_handle = NULL;
        return UD3TN_EINVAL;
    }

    if (!addr) {
        DEBUG_ERR("INET address is null\n");
        *out_handle = NULL;
        return UD3TN_EINVAL;
    }

    struct hostent *aap2_hostent;
    struct _aap2_AAPMessage welcome_mesg = aap2_AAPMessage_init_default;
    struct sockaddr_in aap2_addr;
    ud3tn_error_t recv_err;

    *out_handle = (UD3TNClientInfo *) malloc(sizeof(UD3TNClientInfo));
    if (!out_handle) {
        DEBUG_ERR("malloc(): %s\n", strerror(errno));
        *out_handle = NULL;
        return UD3TN_EINTERNAL;
    }

    aap2_hostent = gethostbyname(addr);
    if (!aap2_hostent) {
        DEBUG_ERR("gethostbyname(): %s\n", strerror(h_errno));
        ud3tn_close(*out_handle);
        return UD3TN_ENOTFOUND;
    }

    // AAP2 INET construction
    memset(&aap2_addr, 0, sizeof aap2_addr);
    aap2_addr.sin_family = AF_INET;
    aap2_addr.sin_port = htons(port);
    memcpy(&aap2_addr.sin_addr.s_addr, aap2_hostent->h_addr, aap2_hostent->h_length);

    ud3tn_sockets *fds = (ud3tn_sockets *) malloc(sizeof(ud3tn_sockets));
    if ((connect_to_socket(AF_INET, &aap2_addr, sizeof aap2_addr, fds)) < 0)
        return UD3TN_EOPEN;
    (*out_handle)->input_socket = fds->one;
    (*out_handle)->output_socket = fds->two;

    (*out_handle)->input_socket_in = (pb_istream_t) {
        &pb_recv_callback,
        (void *) (intptr_t) (*out_handle)->input_socket,
        SIZE_MAX,
        NULL,
    };

    (*out_handle)->output_socket_in = (pb_istream_t) {
        &pb_recv_callback,
        (void *) (intptr_t) (*out_handle)->output_socket,
        SIZE_MAX,
        NULL,
    };

    recv_err = recv_welcome_mesg_aap2((*out_handle)->input_socket, &(*out_handle)->input_socket_in, &welcome_mesg);
    if (welcome_mesg.which_msg != aap2_AAPMessage_welcome_tag || recv_err != UD3TN_SUCCESS) {
        if (recv_err != UD3TN_SUCCESS)
            DEBUG_ERR("Error in receiving input welcome message\n");
        else
            DEBUG_ERR("AAP2 did not welcome the input connection request\n");
        pb_release(aap2_AAPMessage_fields, &welcome_mesg);
        ud3tn_close(*out_handle);
        return UD3TN_EOPEN;
    }

    recv_err = recv_welcome_mesg_aap2((*out_handle)->output_socket, &(*out_handle)->output_socket_in, &welcome_mesg);
    if (welcome_mesg.which_msg != aap2_AAPMessage_welcome_tag || recv_err != UD3TN_SUCCESS) {
        if (recv_err != UD3TN_SUCCESS)
            DEBUG_ERR("Error in receiving output welcome message\n");
        else
            DEBUG_ERR("AAP2 did not welcome the output connection request\n");
        pb_release(aap2_AAPMessage_fields, &welcome_mesg);
        ud3tn_close(*out_handle);
        return UD3TN_EOPEN;
    }

    strncpy((*out_handle)->nodeID, welcome_mesg.msg.welcome.node_id, sizeof((*out_handle)->nodeID) - 1);

    // deallocation is here in order to keep welcome_mesg accessible to open
    pb_release(aap2_AAPMessage_fields, &welcome_mesg);

    DEBUG_MSG(
        "Got welcome message back: Connected to the AAP through a INET socket towards: %s:%d\n",
        addr, port);
    DEBUG_MSG("Node ID: %s\n", (*out_handle)->nodeID);
    return UD3TN_SUCCESS;
}

/**
 * @note BB: This function performs the configuration of both connections. One where the client is passive (to receive AAPMessage
 * sent by the AA) and one where the client is active (to send AAPMessage to the AA). 
 *
 */

ud3tn_error_t
ud3tn_register(ud3tn_handle_t handle, ud3tn_reg_info_t *const reg_info)
{
    // Params check
    if (!handle || !reg_info /* || !out_reg_id */) {
        DEBUG_ERR("Invalid input param(s)\n");
        return UD3TN_EINVAL;
    }

    DEBUG_MSG("Entering %s\n", __FUNCTION__);

    int error;
    aap2_AAPMessage reg_mesg = aap2_AAPMessage_init_default;
    aap2_AAPResponse resp_mesg = aap2_AAPResponse_init_default;

    reg_mesg.which_msg = aap2_AAPMessage_config_tag;
    reg_mesg.msg.config.endpoint_id = (char *) calloc(256, sizeof(char));
    ud3tn_error_t recv_err;

    if (!reg_mesg.msg.config.endpoint_id) {
        DEBUG_ERR("calloc() [eid]: %s\n", strerror(errno));
        return UD3TN_EINTERNAL;
    }

    strcpy(reg_mesg.msg.config.endpoint_id, reg_info->endpoint.uri);

    // reg.mesg.msg.config.is_subscriber = reg_info->is_subscriber;
    reg_mesg.msg.config.auth_type = reg_info->auth_type;
    reg_mesg.msg.config.is_subscriber = false; // false means you can only send bundles
    // hardcoded for now. to be implemented in upper layers
    reg_mesg.msg.config.secret = (char *) calloc(6, sizeof(char));
    strcpy(reg_mesg.msg.config.secret, "hello");

    // Sending the registration message to the AAP2
    if ((error = send_mesg_aap2(handle->output_socket, aap2_AAPMessage_fields, &reg_mesg)) > 0) {
        DEBUG_ERR("send_mesg_aap2(): %s\n", strerror(error));
        pb_release(aap2_AAPMessage_fields, &reg_mesg);
        return UD3TN_EREG;
    }

    // Receiving the response from AAP2
    recv_err = recv_mesg_aap2(&handle->output_socket_in, aap2_AAPResponse_fields, &resp_mesg);
    if (resp_mesg.response_status != aap2_ResponseStatus_RESPONSE_STATUS_SUCCESS || recv_err != UD3TN_SUCCESS) {
        DEBUG_ERR("AAP2 did not ACK the registration\n");
        pb_release(aap2_AAPResponse_fields, &resp_mesg);
        return UD3TN_EREG;
    }

    // only changing is_subscriber param
    reg_mesg.msg.config.is_subscriber = true; // true means you can receive bundles

    // Sending the registration message to the AAP2
    if ((error = send_mesg_aap2(handle->input_socket, aap2_AAPMessage_fields, &reg_mesg)) > 0) {
        DEBUG_ERR("send_mesg_aap2(): %s\n", strerror(error));
        pb_release(aap2_AAPMessage_fields, &reg_mesg);
        return UD3TN_EREG;
    }

    // Receiving the response from AAP2
    recv_err = recv_mesg_aap2(&handle->input_socket_in, aap2_AAPResponse_fields, &resp_mesg);
    if (resp_mesg.response_status != aap2_ResponseStatus_RESPONSE_STATUS_SUCCESS || recv_err != UD3TN_SUCCESS) {
        DEBUG_ERR("AAP2 did not ACK the registration\n");
        pb_release(aap2_AAPResponse_fields, &resp_mesg);
        return UD3TN_EREG;
    }

    pb_release(aap2_AAPMessage_fields, &reg_mesg);
    pb_release(aap2_AAPResponse_fields, &resp_mesg);

    DEBUG_MSG("Got ACK back: Successfully registered to AAP2\n");
    return UD3TN_SUCCESS;
}

/**
 * @note BB: this function fills a BundleADU with input parameters, sends it along with its payload
 * and retreives the timestamp and seqno from the response.
 */
ud3tn_error_t
ud3tn_send(ud3tn_handle_t handle, ud3tn_bundle_spec_t *const spec,
    ud3tn_bundle_payload_t *const payload)
{
    DEBUG_MSG("Entering %s\n", __FUNCTION__);

    // Params check
    if (!handle || !spec || !payload) {
        DEBUG_ERR("Invalid input param(s)\n");
        return UD3TN_EINVAL;
    }

    aap2_AAPMessage send_mesg = aap2_AAPMessage_init_default;
    struct bundle_adu_flags flags = {
        .bpdu = (spec->proc_flags == BUNDLE_FLAG_ADMINISTRATIVE_RECORD),
        .with_bdm_auth = spec->bdm_auth_validated,
    };

    send_mesg.which_msg = aap2_AAPMessage_adu_tag;
    send_mesg.msg.adu.dst_eid = spec->dest.uri;
    send_mesg.msg.adu.src_eid = spec->source.uri;
    send_mesg.msg.adu.payload_length = payload->buf.buf_len;
    send_mesg.msg.adu.adu_flags_count = set_adu_flags(
        send_mesg.msg.adu.adu_flags, flags);

    int send_result = send_mesg_aap2(
        handle->output_socket,
        aap2_AAPMessage_fields,
        &send_mesg);

    if (send_result < 0) {
        DEBUG_ERR("send_mesg_aap2(): %s\n", strerror(send_result));
        return UD3TN_ESEND;
    }
    
    struct tcp_write_to_socket_param wsp = {
        .socket_fd = handle->output_socket,
        .errno_ = 0,
    };

    pb_ostream_t ostream = (pb_ostream_t) {
        &write_callback,
        &wsp,
        SIZE_MAX,
        0,
        NULL,
    };

    const bool ret = pb_write(&ostream, (const unsigned char *) payload->buf.buf_val, payload->buf.buf_len);

    if (wsp.errno_) {
        DEBUG_ERR("AAP2Client send() error: %d", wsp.errno_);
        send_result = -1;
    } else if (!ret) {
        DEBUG_ERR(
            "AAP2Agent: pb_write() error: %s",
            PB_GET_ERROR(&ostream));
        send_result = -1;
    }

    if (send_result < 0) {
        DEBUG_ERR("send_mesg_aap2(): %s\n", strerror(send_result));
        return UD3TN_ESEND;
    }

    aap2_AAPResponse res_mesg = aap2_AAPResponse_init_default;
    send_result = recv_mesg_aap2(&handle->output_socket_in, aap2_AAPResponse_fields, &res_mesg);
    if (res_mesg.response_status != aap2_ResponseStatus_RESPONSE_STATUS_SUCCESS || send_result != UD3TN_SUCCESS) {
        DEBUG_ERR("AAP did not confirm the bundle sent\n");
        pb_release(aap2_AAPResponse_fields, &res_mesg);
        return UD3TN_ESEND;
    }

    DEBUG_MSG("Got SUCCESS back. Successfully sent bundle\n");

    // retrieve timestamp and seqno from response
    if (!res_mesg.has_bundle_headers) {
        DEBUG_ERR("Error in received response.\n");
        pb_release(aap2_AAPResponse_fields, &res_mesg);
        return UD3TN_ESEND;
    }

    spec->creation_ts.time = res_mesg.bundle_headers.creation_timestamp_ms;
    spec->creation_ts.seqno = res_mesg.bundle_headers.sequence_number;
    pb_release(aap2_AAPResponse_fields, &res_mesg);
    return UD3TN_SUCCESS;
}

/**
 * @note BB: received a BundleADU and its payload.
 */
ud3tn_error_t
ud3tn_recv(ud3tn_handle_t handle, ud3tn_bundle_spec_t *out_spec,
    ud3tn_bundle_payload_t *out_payload, ud3tn_timeval_t timeout)
{
    int error;
    // Params check
    if (!handle || !out_spec || !out_payload) {
        DEBUG_ERR("Invalid input param(s)\n");
        return UD3TN_EINVAL;
    }

    DEBUG_MSG("Entering %s\n", __FUNCTION__);

    aap2_AAPMessage recv_mesg = aap2_AAPMessage_init_default;
    aap2_AAPResponse res_mesg = aap2_AAPResponse_init_default;
    res_mesg.has_bundle_headers = false;
    res_mesg.has_dispatch_result = true;
    ud3tn_error_t recv_err;

    if (timeout == UD3TN_NO_TIMEOUT)
        DEBUG_MSG("About to recv with no timeout\n");
    else
        DEBUG_MSG("About to start recv with timeout: %lu\n", timeout);

    recv_err = recv_adu_mesg_aap2(handle->input_socket, &handle->input_socket_in, timeout, &recv_mesg);

    if (recv_err != UD3TN_SUCCESS) {
        DEBUG_ERR("recv_adu_mesg_aap2(): %s\n",
            recv_err == UD3TN_ETIMEOUT ? "Timeout" : "Could not receive");

        // setting response parameters
        res_mesg.response_status = (recv_err == UD3TN_ETIMEOUT ? aap2_ResponseStatus_RESPONSE_STATUS_TIMEOUT : aap2_ResponseStatus_RESPONSE_STATUS_ERROR);
        pb_release(aap2_AAPMessage_fields, &recv_mesg);

        // sending faulty response
        if ((error = send_mesg_aap2(handle->input_socket, aap2_AAPResponse_fields, &res_mesg)) > 0) {
            DEBUG_ERR("send_mesg_aap2(): %s\n", strerror(error));
            return UD3TN_ERECV;
        }
        return recv_err;
    }

    DEBUG_MSG("Got a message back. Type is: %d\n", recv_mesg.which_msg);

    // Read payload    
    uint8_t *payload_data;
    payload_data = receive_payload(
        &handle->input_socket_in,
        recv_mesg.msg.adu.payload_length);

    out_payload->buf.buf_len = recv_mesg.msg.adu.payload_length;
    out_payload->buf.buf_val = (char *) payload_data;
    strncpy(out_spec->source.uri, recv_mesg.msg.adu.src_eid, sizeof(out_spec->source.uri) - 1);
    out_spec->creation_ts.time = recv_mesg.msg.adu.creation_timestamp_ms;
    out_spec->creation_ts.seqno = recv_mesg.msg.adu.sequence_number;

    // after bundle has been received, ack has to be sent back.
    res_mesg.response_status = aap2_ResponseStatus_RESPONSE_STATUS_SUCCESS;
    //setting bundle headers
    res_mesg.has_bundle_headers = true;
    res_mesg.bundle_headers.creation_timestamp_ms = recv_mesg.msg.adu.creation_timestamp_ms;
    res_mesg.bundle_headers.sequence_number = recv_mesg.msg.adu.sequence_number;
    res_mesg.bundle_headers.dst_eid = recv_mesg.msg.adu.src_eid;
    res_mesg.bundle_headers.src_eid = recv_mesg.msg.adu.dst_eid;

    if ((error = send_mesg_aap2(handle->input_socket, aap2_AAPResponse_fields, &res_mesg)) > 0) {
        DEBUG_ERR("send_mesg_aap2(): %s\n", strerror(error));
        pb_release(aap2_AAPMessage_fields, &res_mesg);
        return UD3TN_EREG;
    }

    pb_release(aap2_AAPMessage_fields, &recv_mesg);

    return UD3TN_SUCCESS;
}

ud3tn_error_t
ud3tn_build_local_eid(ud3tn_handle_t handle, ud3tn_endpoint_id_t *out_local_eid,
    const char *const service_tag, ud3tn_scheme_t eid_scheme)
{
    DEBUG_MSG("Entering %s\n", __FUNCTION__);

    // Params check
    if (!handle || !out_local_eid || !service_tag) {
        DEBUG_ERR("Invalid input param(s)\n");
        return UD3TN_EINVAL;
    }

    char pch[30];
    switch (eid_scheme) {
        case UD3TN_DTN_SCHEME:
            if (!strncmp(handle->nodeID, "dtn", 3)) {
                sprintf(out_local_eid->uri, "%s%s", handle->nodeID, service_tag);
                return UD3TN_SUCCESS;
            }

            DEBUG_ERR("Cannot register as DTN as the node EID is not dtn\n");
            return UD3TN_EINVAL;
            // uD3TN in the node ID includes the delimiter ud3tn daemon does not
            // start if the dtn eid is not followed by the delimiter (e.g.
            // dtn:/a.dtn/)
        case UD3TN_IPN_SCHEME:
            if (!strncmp(handle->nodeID, "ipn", 3)) {
                strcpy(pch, handle->nodeID);
                strcpy(pch, strtok(pch, ".")); // extract from ipn:node.0 ipn:n
                sprintf(out_local_eid->uri, "%s.%s", pch,
                    service_tag); // concatenate "." and the service tag
                return UD3TN_SUCCESS;
            }

            DEBUG_ERR("Cannot register as IPN as the node EID is not ipn\n");
            return UD3TN_EINVAL;
        default:
            DEBUG_ERR("Wrong EID scheme: %d\n", eid_scheme);
            return UD3TN_EINVAL;
    }
}

ud3tn_error_t
ud3tn_copy_eid(ud3tn_endpoint_id_t *dst, ud3tn_endpoint_id_t *const src)
{
    // Params check
    if (!dst || !src) {
        DEBUG_ERR("Invalid input param(s)\n");
        return UD3TN_EINVAL;
    }

    memset(dst->uri, 0, sizeof(dst->uri));
    strncpy(dst->uri, src->uri, sizeof(dst->uri));
    return UD3TN_SUCCESS;
}

ud3tn_error_t
ud3tn_parse_eid_string(ud3tn_endpoint_id_t *out_eid, const char *const str)
{
    // Params check
    if (!str || !out_eid) {
        DEBUG_ERR("Invalid input param(s)\n");
        return UD3TN_EINVAL;
    }

    memset(out_eid->uri, 0, sizeof(out_eid->uri));
    strncpy(out_eid->uri, str, sizeof(out_eid->uri) - 1);
    return UD3TN_SUCCESS;
}

ud3tn_error_t
ud3tn_set_payload(ud3tn_bundle_payload_t *out_payload, const uint8_t *const buf,
    uint32_t buf_length)
{
    // Params check
    if (!out_payload || !buf || buf_length < 0) {
        DEBUG_ERR("Invalid input param(s)\n");
        return UD3TN_EINVAL;
    }

    memcpy(out_payload->buf.buf_val, buf, buf_length);
    out_payload->buf.buf_len = buf_length;
    return UD3TN_SUCCESS;
}

void ud3tn_free_payload(ud3tn_bundle_payload_t *const payload)
{
    DEBUG_MSG("Entering %s\n", __FUNCTION__);

    // Params check
    /*
    if (!payload || !payload->buf.buf_val) {
        DEBUG_ERR("Invalid input param\n");
        return;
    }
    */

    free(payload->buf.buf_val);
    payload->buf.buf_len = 0;
}

/*legacy code, never called*/
#ifdef UNIFIED_API_FUNCTION_WORKING_WHEN_AAP2
ud3tn_error_t
ud3tn_ping_aap(ud3tn_handle_t handle)
{
    DEBUG_MSG("Entering %s\n", __FUNCTION__);

    // Params check
    if (!handle) {
        DEBUG_ERR("Invalid input param\n");
        return UD3TN_EINVAL;
    }

    int error;
    struct aap_message ping_mesg = { .type = AAP_MESSAGE_PING };

    if ((error = send_mesg_aap2(handle->socket, &ping_mesg)) > 0) { // Sends ping to AAP
        DEBUG_ERR("send_mesg_aap2(): %s\n", strerror(error));
        return UD3TN_ESEND;
    }

    aap_message_clear(&ping_mesg);

    aap_parser_init(&(handle->parser));

    error = recv_adu_mesg_aap2(handle, UD3TN_NO_TIMEOUT, &ping_mesg); // Waits for response
    if (ping_mesg.type != AAP_MESSAGE_PING || error != UD3TN_SUCCESS) {
        DEBUG_ERR("AAP did not pong the ping\n");
        return UD3TN_ERECV;
    }

    aap_parser_deinit(&(handle->parser));

    DEBUG_MSG("Got pong from AAP: Type is: %d\n", ping_mesg.type);
    return UD3TN_SUCCESS;
}
#else
ud3tn_error_t
ud3tn_ping_aap(ud3tn_handle_t handle)
{
    return UD3TN_ENOTIMPL;
}
#endif // UNIFIED_API_FUNCTION_WORKING_WHEN_AAP2
#endif // UD3TN_IMPLEMENTATION
