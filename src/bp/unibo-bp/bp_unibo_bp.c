/** \file bp_unibo_bp_conversions.c
 *
 *  \brief  This file contains the functions interfacing Unibo-BP
 *
 *  \par Copyright
 *  	Copyright (c) 2022, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Lorenzo Persampieri, lorenzo.persampieri@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  14/11/22 | L. Persampieri  |  Initial Implementation.
 *  02/02/24 | A. Genovese     |  Added metadata support
 */

#include "bp_unibo_bp.h"
//#define UNIBOBP_IMPLEMENTATION
#ifdef UNIBOBP_IMPLEMENTATION

#include <Unibo-BP/bp.h>
#include "bp_unibo_bp_conversions.h"
#include "unified_api_system_libraries.h"

static bool validate_unibo_bp_api(UniboBPUserHandle handle, UniboBPError error) {
    if (error != UniboBP_NoError) {
        if (error == UniboBPError_Server) {
            fprintf(stderr, "Unibo-BP - %s\n", unibo_bp_user_error_to_string(handle));
        }
        return false;
    }
    return true;
}

static bool validate_unibo_bp_sender_api(UniboBPSenderHandle handle, UniboBPError error) {
    if (error != UniboBP_NoError) {
        if (error == UniboBPError_Server) {
            fprintf(stderr, "Unibo-BP - %s\n", unibo_bp_sender_error_to_string(handle));
        }
        return false;
    }
    return true;
}

static bool validate_unibo_bp_receiver_api(UniboBPReceiverHandle handle, UniboBPError error) {
    if (error != UniboBP_NoError) {
        if (error == UniboBPError_Server) {
            fprintf(stderr, "Unibo-BP - %s\n", unibo_bp_receiver_error_to_string(handle));
        }
        return false;
    }
    return true;
}

bool bp_unibo_bp_is_running() {
    return unibo_bp_check_default();
}

al_bp_error_t bp_unibo_bp_open(al_types_handle* handle) {
    UniboBPUserHandle unibo_bp_handle;
    UniboBPError unibo_bp_error;
    unibo_bp_error = unibo_bp_connect_default(&unibo_bp_handle);
    if (!validate_unibo_bp_api(unibo_bp_handle, unibo_bp_error)) {
        return al_from_unibo_bp_error(unibo_bp_error);
    }
    *handle = (al_types_handle) unibo_bp_handle;
    return BP_SUCCESS;
}

al_bp_error_t bp_unibo_bp_open_with_IP(char * daemon_api_IP,
                                       int daemon_api_port,
                                       al_types_handle * handle) {
    (void) daemon_api_IP;
    (void) daemon_api_port;
    (void) handle;
    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_errno(al_types_handle handle) {
    (void) handle;
    return BP_SUCCESS;
}

static uint64_t convert_string_to_uint64_t(const char* str) {
    if (sizeof (unsigned long) >= 8) {
        return strtoul(str, NULL, 10);
    } else {
        return strtoull(str, NULL, 10);
    }
}

al_bp_error_t bp_unibo_bp_build_local_eid(al_types_handle handle,
                                          al_types_endpoint_id* local_eid,
                                          const char* service_tag,
                                          al_types_scheme type) {
    UniboBPError unibo_bp_error;
    UniboBPUserHandle unibo_bp_handle = al_to_unibo_bp_handle(handle);
    UniboBPScheme unibo_bp_scheme = al_to_unibo_bp_scheme(type);

    UniboBPEID unibo_bp_eid = NULL;
    unibo_bp_error = unibo_bp_get_admin_id(unibo_bp_handle, unibo_bp_scheme, &unibo_bp_eid);

    if (!validate_unibo_bp_api(unibo_bp_handle, unibo_bp_error)) {
        unibo_bp_eid_destroy(&unibo_bp_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    unibo_bp_error = UniboBPError_InvalidArgument; // default
    switch (unibo_bp_scheme) {
        case UniboBPScheme_unknown:
            unibo_bp_error = UniboBPError_InvalidArgument;
            break;
        case UniboBPScheme_dtn:
            unibo_bp_error = unibo_bp_eid_dtn_set_demux(unibo_bp_cast_eid_dtn(unibo_bp_eid),
                                                        service_tag);
            break;
        case UniboBPScheme_ipn:
            unibo_bp_eid_ipn_set_service_number(unibo_bp_cast_eid_ipn(unibo_bp_eid),
                                                convert_string_to_uint64_t(service_tag));
            unibo_bp_error = UniboBP_NoError;
            break;
        case UniboBPScheme_imc:
            unibo_bp_error = UniboBPError_InvalidArgument;
            break;
    }

    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_eid_destroy(&unibo_bp_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    unibo_bp_error = unibo_bp_eid_to_string(unibo_bp_const_cast_eid(unibo_bp_eid), local_eid->uri, sizeof(local_eid->uri));
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_eid_destroy(&unibo_bp_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    unibo_bp_eid_destroy(&unibo_bp_eid);

    return al_from_unibo_bp_error(UniboBP_NoError);
}


al_bp_error_t bp_unibo_bp_register(al_types_handle handle,
                                   al_types_reg_info* reginfo) {
    UniboBPError unibo_bp_error;
    UniboBPUserHandle unibo_bp_handle = al_to_unibo_bp_handle(handle);
    UniboBPEID unibo_bp_eid = NULL;
    unibo_bp_error = unibo_bp_eid_create(reginfo->endpoint.uri, &unibo_bp_eid);
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_eid_destroy(&unibo_bp_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }
    unibo_bp_error = unibo_bp_bind(unibo_bp_handle, &unibo_bp_eid);
    if (!validate_unibo_bp_api(unibo_bp_handle, unibo_bp_error)) {
        unibo_bp_eid_destroy(&unibo_bp_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }
    return al_from_unibo_bp_error(UniboBP_NoError);
}

al_bp_error_t bp_unibo_bp_find_registration(al_types_handle handle,
                                            al_types_endpoint_id * eid) {
    (void) handle;
    (void) eid;
    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_unregister(al_types_handle handle) {
    UniboBPUserHandle unibo_bp_handle = al_to_unibo_bp_handle(handle);
    unibo_bp_unbind(unibo_bp_handle);
    // ignore error -- they are not so important here
    return al_from_unibo_bp_error(UniboBP_NoError);
}

// not found in Unified API but we need it here
static uint64_t al_get_payload_length(al_types_bundle_payload* payload) {
    if (payload->location == BP_PAYLOAD_MEM) {
        return payload->buf.buf_len;
    } else {
        FILE* f = fopen(payload->filename.filename_val, "r");
        fseek(f, 0, SEEK_END); // seek to end of file
        uint64_t size = ftell(f); // get current file pointer
        // if you do not close the file here: fseek(f, 0, SEEK_SET); // seek back to beginning of file
        fclose(f);
        return size;
    }
}

al_bp_error_t bp_unibo_bp_send(al_types_handle handle,
                               al_types_bundle_spec* spec,
                               al_types_bundle_payload* payload) {
    UniboBPError unibo_bp_error;
    UniboBPUserHandle unibo_bp_handle = al_to_unibo_bp_handle(handle);
    UniboBPOutboundBundle unibo_bp_bundle;

    unibo_bp_error = unibo_bp_outbound_bundle_create(&unibo_bp_bundle);
    if (unibo_bp_error != UniboBP_NoError) {
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    UniboBPEID unibo_bp_source_eid = NULL;
    unibo_bp_error = unibo_bp_eid_create(spec->source.uri, &unibo_bp_source_eid);
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_outbound_bundle_destroy(&unibo_bp_bundle);
        unibo_bp_eid_destroy(&unibo_bp_source_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }
    unibo_bp_error = unibo_bp_outbound_bundle_set_source_node_id(unibo_bp_bundle, &unibo_bp_source_eid);
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_outbound_bundle_destroy(&unibo_bp_bundle);
        unibo_bp_eid_destroy(&unibo_bp_source_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    UniboBPEID unibo_bp_destination_eid = NULL;
    unibo_bp_error = unibo_bp_eid_create(spec->destination.uri, &unibo_bp_destination_eid);
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_outbound_bundle_destroy(&unibo_bp_bundle);
        unibo_bp_eid_destroy(&unibo_bp_destination_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }
    unibo_bp_error = unibo_bp_outbound_bundle_set_destination_eid(unibo_bp_bundle, &unibo_bp_destination_eid);
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_outbound_bundle_destroy(&unibo_bp_bundle);
        unibo_bp_eid_destroy(&unibo_bp_destination_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    UniboBPEID unibo_bp_report_to_eid = NULL;
    unibo_bp_error = unibo_bp_eid_create(spec->report_to.uri, &unibo_bp_report_to_eid);
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_outbound_bundle_destroy(&unibo_bp_bundle);
        unibo_bp_eid_destroy(&unibo_bp_report_to_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }
    unibo_bp_error = unibo_bp_outbound_bundle_set_report_to_eid(unibo_bp_bundle, &unibo_bp_report_to_eid);
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_outbound_bundle_destroy(&unibo_bp_bundle);
        unibo_bp_eid_destroy(&unibo_bp_report_to_eid);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    // set a default CRC for the primary block (must not be "no CRC" since we have not BPSEC)
    unibo_bp_outbound_bundle_set_crc_type_code(unibo_bp_bundle, UniboBPCRCTypeCode_X25CRC16);

    uint64_t unibo_bp_bundle_processing_control_flags = al_to_unibo_bp_bundle_processing_control_flags(spec->bundle_proc_ctrl_flags);
    unibo_bp_outbound_bundle_set_bundle_processing_control_flags(unibo_bp_bundle, unibo_bp_bundle_processing_control_flags);

    unibo_bp_outbound_bundle_set_lifetime(unibo_bp_bundle, al_to_unibo_bp_lifetime(spec->lifetime));

    unibo_bp_outbound_bundle_ecos_set_flags(unibo_bp_bundle, al_to_unibo_bp_ecos_flags(&spec->ecos));
    unibo_bp_outbound_bundle_ecos_set_cardinal_priority(unibo_bp_bundle, al_to_unibo_bp_cardinal_priority(spec->cardinal));
    unibo_bp_outbound_bundle_ecos_set_ordinal_priority(unibo_bp_bundle, spec->ecos.ordinal);
    unibo_bp_outbound_bundle_ecos_set_qos_tag(unibo_bp_bundle, spec->ecos.flow_label);

    //////// metadata (extensions)  /////////
    if (spec->extensions.extension_number > 0){
        // I could also directly insert the "al_to" functions into those of unibo_bp, but this is better for debugging...                                
        //UniboBPMetadataType metadata_type = al_to_unibo_bp_extensions_metadata_type(spec->extensions.extension_number, spec->extensions.extension_blocks);
        //uint64_t metadata_length = al_to_unibo_bp_extensions_metadata_length(spec->extensions.extension_number, spec->extensions.extension_blocks);
        //char *metadata_string = al_to_unibo_bp_extensions_metadata_string(spec->extensions.extension_number, spec->extensions.extension_blocks);

        UniboBPMetadataType metadata_type;
        uint64_t metadata_length;
        char *metadata_string = NULL;

        al_to_unibo_bp_extensions_metadata(spec->extensions.extension_blocks, &metadata_type, &metadata_length, &metadata_string);

        unibo_bp_outbound_bundle_metadata_set_type(unibo_bp_bundle, metadata_type);
        unibo_bp_outbound_bundle_metadata_set_length(unibo_bp_bundle, metadata_length);
        unibo_bp_outbound_bundle_metadata_set_string(unibo_bp_bundle, metadata_string);

        free(metadata_string);
    }
    ////////////////////////////////////////////////////////////

    UniboBPBlockReader unibo_bp_payload_reader = NULL;
    unibo_bp_error = al_to_unibo_bp_payload_reader(payload, &unibo_bp_payload_reader);
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_block_reader_destroy(&unibo_bp_payload_reader);
        unibo_bp_outbound_bundle_destroy(&unibo_bp_bundle);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    unibo_bp_error = unibo_bp_outbound_bundle_set_payload(unibo_bp_bundle,
                                                          al_get_payload_length(payload),
                                                          UniboBPCRCTypeCode_noCRC, // TODO not found payload CRC Type Code in Unified-API
                                                          &unibo_bp_payload_reader);

    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_block_reader_destroy(&unibo_bp_payload_reader);
        unibo_bp_outbound_bundle_destroy(&unibo_bp_bundle);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    UniboBPSenderHandle unibo_bp_sender_handle = unibo_bp_get_sender_handle(unibo_bp_handle);
    unibo_bp_error = unibo_bp_send_bundle(unibo_bp_sender_handle, unibo_bp_bundle);

    if (!validate_unibo_bp_sender_api(unibo_bp_sender_handle, unibo_bp_error)) {
        unibo_bp_outbound_bundle_destroy(&unibo_bp_bundle);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    ConstUniboBPOutboundBundle const_unibo_bp_bundle = unibo_bp_const_cast_outbound_bundle(unibo_bp_bundle);
    spec->creation_ts.time = unibo_bp_outbound_bundle_get_creation_time(const_unibo_bp_bundle);
    spec->creation_ts.seqno = unibo_bp_outbound_bundle_get_sequence_number(const_unibo_bp_bundle);

    unibo_bp_outbound_bundle_destroy(&unibo_bp_bundle);

    return al_from_unibo_bp_error(UniboBP_NoError);
}

// not found in Unified API but we need it here
static al_bp_error_t al_create_payload(al_types_bundle_payload* payload, al_types_bundle_payload_location location, uint64_t length) {
    if (location == BP_PAYLOAD_MEM) {
        // save payload in memory
        payload->location = BP_PAYLOAD_MEM;
        payload->buf.buf_len = length;
        payload->buf.buf_val = (char*) malloc(length);
        if (!payload->buf.buf_val) {
            payload->buf.buf_len = 0;
            return BP_ENULLPNTR;
        }
    } else {
        // save payload in file
        payload->location = BP_PAYLOAD_FILE;
        payload->filename.filename_val = (char*) malloc(sizeof(char) * 256);
        if (!payload->filename.filename_val) {
            return BP_ENULLPNTR;
        }
        strcpy(payload->filename.filename_val, "unibo-bp-XXXXXX");
        int fd = mkstemp(payload->filename.filename_val);
        if (fd < 0) {
            return BP_ERRBASE;
        }
        close(fd);
        payload->filename.filename_len = strlen(payload->filename.filename_val);
    }

    return BP_SUCCESS;
}

al_bp_error_t bp_unibo_bp_recv(al_types_handle handle,
                               al_types_bundle_spec* spec,
                               al_types_bundle_payload_location location,
                               al_types_bundle_payload* payload,
                               al_types_timeval timeout) {
    UniboBPError unibo_bp_error;
    UniboBPUserHandle unibo_bp_handle = al_to_unibo_bp_handle(handle);
    UniboBPReceiverHandle unibo_bp_receiver_handle = unibo_bp_get_receiver_handle(unibo_bp_handle);
    UniboBPInboundBundle unibo_bp_bundle;

    unibo_bp_error = unibo_bp_receive_bundle_blocking(unibo_bp_receiver_handle, &unibo_bp_bundle, timeout);
    if (!validate_unibo_bp_receiver_api(unibo_bp_receiver_handle, unibo_bp_error)) {
        if (unibo_bp_error == UniboBPError_BundleNotFound) {
            // probably connection closed on server-side -- we need to handle this special case here
            return BP_ERECV;
        } else {
            return al_from_unibo_bp_error(unibo_bp_error);
        }
    }

    ConstUniboBPInboundBundle const_unibo_bp_bundle = unibo_bp_const_cast_inbound_bundle(unibo_bp_bundle);
    const uint64_t bundle_id = unibo_bp_inbound_bundle_get_internal_id(const_unibo_bp_bundle);

    uint64_t payload_length = unibo_bp_inbound_bundle_get_payload_length(const_unibo_bp_bundle);
    al_bp_error_t al_bp_error = al_create_payload(payload, location, payload_length);
    if (al_bp_error != BP_SUCCESS) {
        unibo_bp_release_bundle_delivery(unibo_bp_receiver_handle, bundle_id);
        unibo_bp_inbound_bundle_destroy(&unibo_bp_bundle);
        return al_bp_error;
    }

    UniboBPBlockWriter unibo_bp_payload_writer = NULL;
    unibo_bp_error = al_to_unibo_bp_payload_writer(payload, &unibo_bp_payload_writer);
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_release_bundle_delivery(unibo_bp_receiver_handle, bundle_id);
        unibo_bp_inbound_bundle_destroy(&unibo_bp_bundle);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    // retrieve payload
    unibo_bp_error = unibo_bp_read_payload(unibo_bp_receiver_handle, bundle_id, unibo_bp_payload_writer);
    unibo_bp_block_writer_destroy(&unibo_bp_payload_writer);
    if (!validate_unibo_bp_receiver_api(unibo_bp_receiver_handle, unibo_bp_error)) {
        unibo_bp_release_bundle_delivery(unibo_bp_receiver_handle, bundle_id);
        unibo_bp_inbound_bundle_destroy(&unibo_bp_bundle);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    // release bundle delivery since we do not need to query anymore the server for information about this bundle
    unibo_bp_error = unibo_bp_release_bundle_delivery(unibo_bp_receiver_handle, bundle_id);
    if (!validate_unibo_bp_receiver_api(unibo_bp_receiver_handle, unibo_bp_error)) {
        unibo_bp_inbound_bundle_destroy(&unibo_bp_bundle);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    ConstUniboBPEID unibo_bp_source_eid = unibo_bp_inbound_bundle_get_source_node_id(const_unibo_bp_bundle);
    unibo_bp_error = unibo_bp_eid_to_string(unibo_bp_source_eid, spec->source.uri, sizeof(spec->source.uri));
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_inbound_bundle_destroy(&unibo_bp_bundle);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    ConstUniboBPEID unibo_bp_destination_eid = unibo_bp_inbound_bundle_get_destination_eid(const_unibo_bp_bundle);
    unibo_bp_error = unibo_bp_eid_to_string(unibo_bp_destination_eid, spec->destination.uri, sizeof(spec->destination.uri));
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_inbound_bundle_destroy(&unibo_bp_bundle);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    ConstUniboBPEID unibo_bp_report_to_eid = unibo_bp_inbound_bundle_get_report_to_eid(const_unibo_bp_bundle);
    unibo_bp_error = unibo_bp_eid_to_string(unibo_bp_report_to_eid, spec->report_to.uri, sizeof(spec->report_to.uri));
    if (unibo_bp_error != UniboBP_NoError) {
        unibo_bp_inbound_bundle_destroy(&unibo_bp_bundle);
        return al_from_unibo_bp_error(unibo_bp_error);
    }

    spec->creation_ts.time = unibo_bp_inbound_bundle_get_creation_time(const_unibo_bp_bundle);
    spec->creation_ts.seqno = unibo_bp_inbound_bundle_get_sequence_number(const_unibo_bp_bundle);

    const uint64_t unibo_bp_bundle_processing_control_flags = unibo_bp_inbound_bundle_get_bundle_processing_control_flags(const_unibo_bp_bundle);
    spec->bundle_proc_ctrl_flags = al_from_unibo_bp_bundle_processing_control_flags(unibo_bp_bundle_processing_control_flags);

    spec->lifetime = al_from_unibo_bp_lifetime(unibo_bp_inbound_bundle_get_lifetime(const_unibo_bp_bundle));

    const uint16_t unibo_bp_ecos_flags = unibo_bp_inbound_bundle_ecos_get_flags(const_unibo_bp_bundle);
    al_from_unibo_bp_ecos_flags(unibo_bp_ecos_flags, &spec->ecos);
    spec->cardinal = al_from_unibo_bp_cardinal_priority(unibo_bp_inbound_bundle_ecos_get_cardinal_priority(const_unibo_bp_bundle));
    spec->ecos.ordinal = unibo_bp_inbound_bundle_ecos_get_ordinal_priority(const_unibo_bp_bundle);
    spec->ecos.flow_label = unibo_bp_inbound_bundle_ecos_get_qos_tag(const_unibo_bp_bundle);

    ///////////////// metadata (extensions) /////////////////
    if(unibo_bp_inbound_bundle_metadata_get_type(const_unibo_bp_bundle) != 0) {
        spec->extensions.extension_number = 1; //Number of metadata blocks to be converted to BP
        al_from_unibo_bp_extensions_metadata(const_unibo_bp_bundle, &spec->extensions.extension_blocks);
        //al_from_unibo_bp_extensions_metadata(const_unibo_bp_bundle, spec);
    }
    //////////////////////////////////////////////

    if (unibo_bp_bundle_processing_control_flags & UniboBPBundleProcessingControlFlag_isAdministrativeRecord) {
        UniboBPBlockReader unibo_bp_payload_reader = NULL;
        unibo_bp_error = al_to_unibo_bp_payload_reader(payload, &unibo_bp_payload_reader);
        if (unibo_bp_error != UniboBP_NoError) {
            unibo_bp_inbound_bundle_destroy(&unibo_bp_bundle);
            return al_from_unibo_bp_error(unibo_bp_error);
        }
        al_bp_error = al_from_unibo_bp_administrative_record(payload, unibo_bp_payload_reader);
        unibo_bp_block_reader_destroy(&unibo_bp_payload_reader);
        if (al_bp_error != BP_SUCCESS) {
            unibo_bp_inbound_bundle_destroy(&unibo_bp_bundle);
            return al_bp_error;
        }
    }

    unibo_bp_inbound_bundle_destroy(&unibo_bp_bundle);

    return al_from_unibo_bp_error(UniboBP_NoError);
}

al_bp_error_t bp_unibo_bp_close(al_types_handle handle) {
    UniboBPUserHandle unibo_bp_handle = al_to_unibo_bp_handle(handle);
    unibo_bp_disconnect(&unibo_bp_handle);
    return al_from_unibo_bp_error(UniboBP_NoError);
}

void bp_unibo_bp_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src) {
    strncpy(dst->uri, src->uri, sizeof(dst->uri) - 1);
    dst->uri[sizeof(dst->uri) - 1] = '\0';
}

al_bp_error_t bp_unibo_bp_parse_eid_string(al_types_endpoint_id* eid,
                                           const char* str) {
    if (!unibo_bp_eid_validate(str)) {
        return BP_EPARSEEID;
    }
    strncpy(eid->uri, str, sizeof(eid->uri) - 1);
    eid->uri[sizeof(eid->uri) - 1] = '\0';

    return al_from_unibo_bp_error(UniboBP_NoError);
}

al_bp_error_t bp_unibo_bp_set_payload(al_types_bundle_payload* payload,
                                      al_types_bundle_payload_location location,
                                      char* val,
                                      int len) {
    payload->location = location;

    switch (location) {
        case BP_PAYLOAD_FILE:
            payload->filename.filename_val = val;
            payload->filename.filename_len = len;
            break;
        case BP_PAYLOAD_MEM:
            payload->buf.buf_val = val;
            payload->buf.buf_len = len;
            break;
        case BP_PAYLOAD_TEMP_FILE:
            payload->filename.filename_val = val;
            payload->filename.filename_len = len;
            break;
        default:
            return al_from_unibo_bp_error(UniboBPError_InvalidArgument);
    }

    return al_from_unibo_bp_error(UniboBP_NoError);
}

void bp_unibo_bp_free_payload(al_types_bundle_payload* payload) {
    if (!payload) return;

    free(payload->status_report);
    payload->status_report = NULL;
    if (payload->location == BP_PAYLOAD_FILE || payload->location == BP_PAYLOAD_TEMP_FILE) {
        if (payload->filename.filename_val) {
            remove(payload->filename.filename_val);
        }
        free(payload->filename.filename_val);
        payload->filename.filename_val = NULL;
        payload->filename.filename_len = 0;
    } else {
        free(payload->buf.buf_val);
        payload->buf.buf_val = NULL;
        payload->buf.buf_len = 0;
    }
}

void bp_unibo_bp_free_extensions(al_types_bundle_spec* spec) {
    if (!spec) return;

    for (unsigned int i = 0; i < spec->extensions.extension_number; i++) {
        free(spec->extensions.extension_blocks[i].block_data.block_type_specific_data);
        spec->extensions.extension_blocks[i].block_data.block_type_specific_data = NULL;
        spec->extensions.extension_blocks[i].block_data.block_type_specific_data_len = 0;
    }
    free(spec->extensions.extension_blocks);
    spec->extensions.extension_blocks = NULL;
    spec->extensions.extension_number = 0;
}

#else

bool bp_unibo_bp_is_running(const char *directory) {
    (void) directory; // unused

    return false;
}

al_bp_error_t bp_unibo_bp_open(al_types_handle* handle) {
    (void) handle; // unused

    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_open_with_IP(char * daemon_api_IP,
                                       int daemon_api_port,
                                       al_types_handle * handle) {
    (void) daemon_api_IP; // unused
    (void) daemon_api_port; // unused
    (void) handle; // unused

    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_errno(al_types_handle handle) {
    (void) handle; // unused

    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_build_local_eid(al_types_handle handle,
                                          al_types_endpoint_id* local_eid,
                                          const char* service_tag,
                                          al_types_scheme type) {
    (void) handle; // unused
    (void) local_eid; // unused
    (void) service_tag; // unused
    (void) type; // unused

    return BP_ENOTIMPL;
}


al_bp_error_t bp_unibo_bp_register(al_types_handle handle,
                                   al_types_reg_info* reginfo) {
    (void) handle; // unused
    (void) reginfo; // unused

    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_find_registration(al_types_handle handle,
                                            al_types_endpoint_id * eid) {
    (void) handle; // unused
    (void) eid; // unused

    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_unregister(al_types_handle handle) {
    (void) handle; // unused

    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_send(al_types_handle handle,
                               al_types_bundle_spec* spec,
                               al_types_bundle_payload* payload) {
    (void) handle; // unused
    (void) spec; // unused
    (void) payload; //unused

    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_recv(al_types_handle handle,
                               al_types_bundle_spec* spec,
                               al_types_bundle_payload_location location,
                               al_types_bundle_payload* payload,
                               al_types_timeval timeout) {
    (void) handle; // unused
    (void) spec; // unused
    (void) location; // unused
    (void) payload; // unused
    (void) timeout; // unused

    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_close(al_types_handle handle) {
    (void) handle; // unused

    return BP_ENOTIMPL;
}

void bp_unibo_bp_copy_eid(al_types_endpoint_id* dst, al_types_endpoint_id* src) {
    (void) dst; // unused
    (void) src; // unused
}

al_bp_error_t bp_unibo_bp_parse_eid_string(al_types_endpoint_id* eid,
                                           const char* str) {
    (void) eid; // unused
    (void) str; // unused

    return BP_ENOTIMPL;
}

al_bp_error_t bp_unibo_bp_set_payload(al_types_bundle_payload* payload,
                                      al_types_bundle_payload_location location,
                                      char* val,
                                      int len) {
    (void) payload; // unused
    (void) location; // unused
    (void) val; // unused
    (void) len; // unused

    return BP_ENOTIMPL;
}

void bp_unibo_bp_free_payload(al_types_bundle_payload* payload) {
    (void) payload; // unused
}

void bp_unibo_bp_free_extensions(al_types_bundle_spec* spec) {
    (void) spec; // unused
}
#endif
