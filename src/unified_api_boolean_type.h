/*
 * unified_api_boolean_type
 *
 *  Created on: 4 nov 2022
 *      Author: root
 */

#ifndef UNIFIED_API_BOOLEAN_TYPE_H_
#define UNIFIED_API_BOOLEAN_TYPE_H_

//boolean

#ifndef NULL
#define NULL      ((void*)0)
#endif


#ifndef TRUE
#define TRUE        (1 == 1)
#endif
#ifndef true
#define true        TRUE
#endif
#ifndef FALSE
#define FALSE       (0 == 1)
#endif
#ifndef false
#define false       FALSE
#endif

typedef char boolean_t;

#endif /* UNIFIED_API_BOOLEAN_TYPE_H_ */
