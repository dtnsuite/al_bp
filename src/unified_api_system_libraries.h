/** \file unified_api_system_libraries.h
 *
 *  \brief  This file contains a list of all system library used by the al_bp.
 *
 *  \par Copyright
 *  	Copyright (c) 2013, Alma Mater Studiorum, University of Bologna. All rights reserved.
 *
 *  \par License
 *     This file is part of Unified-API.                                            	<br>
 *                                                                                	<br>
 *     Unified-API is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.                                        	<br>
 *     Unified-API is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.                               	<br>
 *                                                                                	<br>
 *     You should have received a copy of the GNU General Public License
 *     along with Unified-API.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  \authors Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *  \authors Anna d'Amico, anna.damico2@studio.unibo.it
 *
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 *	\par Revision History
 *
 *  DD/MM/YY | AUTHOR          |  DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  01/01/13 | M. Rodolfi	   |  Initial Implementation.
 *  01/01/13 | A. D'Amico	   |  Initial Implementation.
 *
 *
 */

#ifndef UNIFIED_API_SYSTEM_LIBRARIES_H_
#define UNIFIED_API_SYSTEM_LIBRARIES_H_

#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#endif /*UNIFIED_API_SYSTEM_LIBRARIES_H_*/
